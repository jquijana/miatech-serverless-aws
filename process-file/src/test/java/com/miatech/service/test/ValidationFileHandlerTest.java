package com.miatech.service.test;

import com.miatech.service.dto.expose.ServerlessRequest;
import com.miatech.service.exception.BaseException;
import com.miatech.service.handler.ValidationFileHandler;
import com.miatech.service.util.Constants;

public class ValidationFileHandlerTest {

	private static final String urlService= "/api/v1/validation";
	
	public static void main(String[] args) throws BaseException {
		ServerlessRequest serverlessRequest = new ServerlessRequest();
		serverlessRequest.setHttpMethod(Constants.Web.HTTP_POST);
		serverlessRequest.setResource(urlService);
		serverlessRequest.setBody("{'validationType':'CARGA_MASIVA', 'fields': [{'field':'REFERENCIA'},{'field':'CÓDIGO POSTAL'},{'field':'NO ETICKET /PNR'},{'field':'NOMBRE /RAZÓN SOCIAL'}]}");
		System.out.println(new ValidationFileHandler().handleRequest(serverlessRequest, null));
		
	}

}