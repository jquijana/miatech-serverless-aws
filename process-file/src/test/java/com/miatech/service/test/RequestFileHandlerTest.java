package com.miatech.service.test;

import java.time.Instant;
import java.util.Date;

import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.service.ProcessFileService;
import com.miatech.service.service.ProcessFileServiceImpl;

public class RequestFileHandlerTest {

//	private static final String urlbeatrackRoute = "/v1/beatrack/route";

	public static void main(String[] args) throws BaseException {
		ProcessFileService processFileService = new ProcessFileServiceImpl();
		RequestFileDto requestFileDto = new RequestFileDto();
		requestFileDto.setUser("AWS:AIDAIZ3PUD7R7ESUQE7UO");
		requestFileDto.setFilePathOriginal("miatech-input-file/cargaMasiva.xlsx");
		requestFileDto.setFilePathWorking("miatech-work-file/cargaMasiva.xlsx");
		requestFileDto.setRequestDateTime(Date.from(Instant.now()));
		requestFileDto.setCreatedBy("AWS:AIDAIZ3PUD7R7ESUQE7UO");
		requestFileDto.setUpdatedBy("AWS:AIDAIZ3PUD7R7ESUQE7UO");
		requestFileDto.setStatus(0);
		processFileService.saveRequestFile(requestFileDto);
	}

}