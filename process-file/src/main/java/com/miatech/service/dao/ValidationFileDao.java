package com.miatech.service.dao;

import java.util.Map;

import com.miatech.service.dto.ValidationFileDto;
import com.miatech.service.exception.BaseException;

public interface ValidationFileDao {

	Map<String, ValidationFileDto> getMapById(String fileId, boolean indById) throws BaseException;

}
