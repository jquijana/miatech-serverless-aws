package com.miatech.service.dao;

import java.util.List;

import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.exception.BaseException;

public interface ProcessMasiveFileDao {

	void saveMasiveFile(List<MasiveFileDto> processMasiveFileDtos) throws BaseException;
	
	void saveEspejoFile(List<EspejoFileDto> espejoFileDto) throws BaseException;
	
	List<MasiveFileDto> getByRequestFileId(Long requestFileId) throws BaseException;

}
