package com.miatech.service.dao;

import java.util.List;

import com.miatech.service.dto.IneDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.exception.BaseException;

public interface RequestFileDao {

	RequestFileDto getRequestFileById(Long id) throws BaseException;

	void updateRequestFile(RequestFileDto requestFileDto) throws BaseException;

	void saveIne(List<IneDto> ineDtos) throws BaseException;

	RequestFileDto save(RequestFileDto requestFileDto) throws BaseException;

	RequestFileDto getDataGenerateMasiveFile(String requestFileId) throws BaseException;

	RequestFileDto getDataGenerateEspejoFile(String requestFileId) throws BaseException;
	
	List<IneDto> getDataIne(String requestFileId) throws BaseException;

	RequestFileDto getDataSendMailMasive(String requestFileId) throws BaseException;

	RequestFileDto getDataSendMailEspejo(String requestFileId) throws BaseException;

}
