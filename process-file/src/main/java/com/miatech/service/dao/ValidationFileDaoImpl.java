package com.miatech.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.DBConection;
import com.miatech.service.dto.ValidationFileDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Constants.QueryAlias;

public class ValidationFileDaoImpl implements ValidationFileDao {

	private static final Logger LOGGER = LogManager.getLogger(ValidationFileDaoImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	@Override
	public Map<String, ValidationFileDto> getMapById(String fileId, boolean indById) throws BaseException {
		LOGGER.info("getMapById() -> :: " + GSON.toJson(fileId));
		Map<String, ValidationFileDto> response = new HashMap<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_VALIDATIONS_BY_ID);
			ps.setString(1, fileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ValidationFileDto validationFileDto = getValidationDto(rs);
				if (indById) {
					response.put(validationFileDto.getId().toString(), validationFileDto);
				}else {
					response.put(validationFileDto.getName(), validationFileDto);	
				}
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return response;
	}

	private ValidationFileDto getValidationDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.VALIDATION_ID);
		String codeFile = rs.getString(QueryAlias.CODE_FILE);
		String name = rs.getString(QueryAlias.NAME);
		String createdBy = rs.getString(QueryAlias.CREATED_BY);
		Date createdOn = rs.getTimestamp(QueryAlias.CREATED_ON);
		String updatedBy = rs.getString(QueryAlias.UPDATED_BY);
		Date updatedOn = rs.getTimestamp(QueryAlias.UPDATED_ON);

		ValidationFileDto validationFileDto = new ValidationFileDto();
		validationFileDto.setId(id);
		validationFileDto.setCodeFile(codeFile);
		validationFileDto.setName(name);
		validationFileDto.setCreatedBy(createdBy);
		validationFileDto.setCreatedOn(createdOn);
		validationFileDto.setUpdatedBy(updatedBy);
		validationFileDto.setUpdatedOn(updatedOn);
		
		return validationFileDto;
	}

}
