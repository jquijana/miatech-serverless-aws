package com.miatech.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.DBConection;
import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.dto.IneDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Constants.QueryAlias;
import com.miatech.service.util.Utils;

public class RequestFileDaoImpl implements RequestFileDao {

	private static final Logger LOGGER = LogManager.getLogger(RequestFileDaoImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	@Override
	public RequestFileDto getRequestFileById(Long id) throws BaseException {
		LOGGER.info("getRequestFileById() -> :: " + GSON.toJson(id));
		List<RequestFileDto> response = new ArrayList<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_PROCESS_MASIVE_FILE_BY_REQUEST_ID);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				response.add(getRequestFileDto(rs));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return (response != null && response.size() > 0) ? response.get(0) : null;
	}

	private RequestFileDto getRequestFileDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.AUTO_ID);
		String user = rs.getString(QueryAlias.USER);
		String fileType = rs.getString(QueryAlias.FILE_TYPE);
		String filePathOriginal = rs.getString(QueryAlias.FILE_PATH_ORIGINAL);
		String filePathWorking = rs.getString(QueryAlias.FILE_PATH_WORKING);
		String filePathResult = rs.getString(QueryAlias.FILE_PATH_RESULT);
		Date requestDateTime = rs.getTimestamp(QueryAlias.REQUEST_DATE);
		Integer origNumberTickets = rs.getInt(QueryAlias.ORIGIN_NUMBER_TICKETS);
		Integer calculatedNumberTickets = rs.getInt(QueryAlias.CALCULATED_NUMBER_TICKETS);
		Integer numberTicketsProcesed = rs.getInt(QueryAlias.NUMBER_TICKETS_PROCESED);
		Integer status = rs.getInt(QueryAlias.STATUS);
		Date lastUpdate = rs.getTimestamp(QueryAlias.LAST_UPDATE);
		Date lastVerified = rs.getTimestamp(QueryAlias.LAST_VERIFIED);

		String createdBy = rs.getString(QueryAlias.CREATED_BY);
		Date createdOn = rs.getTimestamp(QueryAlias.CREATED_ON);
		String updatedBy = rs.getString(QueryAlias.UPDATED_BY);
		Date updatedOn = rs.getTimestamp(QueryAlias.UPDATED_ON);

		RequestFileDto requestFileDto = new RequestFileDto();
		requestFileDto.setId(id);
		requestFileDto.setUser(user);
		requestFileDto.setFileType(fileType);
		requestFileDto.setFilePathOriginal(filePathOriginal);
		requestFileDto.setFilePathWorking(filePathWorking);
		requestFileDto.setFilePathResult(filePathResult);
		requestFileDto.setRequestDateTime(requestDateTime);
		requestFileDto.setOrigNumberTickets(origNumberTickets);
		requestFileDto.setCalculatedNumberTickets(calculatedNumberTickets);
		requestFileDto.setNumberTicketsProcesed(numberTicketsProcesed);
		requestFileDto.setStatus(status);
		requestFileDto.setLastUpdate(lastUpdate);
		requestFileDto.setLastVerified(lastVerified);
		requestFileDto.setCreatedBy(createdBy);
		requestFileDto.setCreatedOn(createdOn);
		requestFileDto.setUpdatedBy(updatedBy);
		requestFileDto.setUpdatedOn(updatedOn);
		return requestFileDto;
	}

	@Override
	public void updateRequestFile(RequestFileDto requestFileDto) throws BaseException {
		LOGGER.info("updateRequestFile() -> :: " + GSON.toJson(requestFileDto));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE REQUEST_FILES "
					+ "SET ORIGNUMBERTICKETS =  ? , FILEPATHWORKING =  ? , "
					+ "CALCULATEDNUMBERTICKETS =  ? , UPDATEDBY = 'SYSTEM', UPDATEDON = NOW(), STATUS =  ?, FILEPATHRESULT = ?, " 
					+ "LASTUPDATE = NOW(), LASTVERIFIED = NOW() "
					+ "WHERE AUTOID = ?  ");

			ps.setInt(1, Utils.toInt(requestFileDto.getOrigNumberTickets()));
			ps.setString(2, requestFileDto.getFilePathWorking());
			ps.setInt(3, Utils.toInt(requestFileDto.getCalculatedNumberTickets()));
			ps.setInt(4, requestFileDto.getStatus());
			ps.setString(5, requestFileDto.getFilePathResult());
			ps.setLong(6, requestFileDto.getId());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL ACTUALIZAR REQUEST FILE : {}", ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void saveIne(List<IneDto> ineDtos) throws BaseException {
		LOGGER.info("saveIne() -> :: " + GSON.toJson(ineDtos));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO REQUEST_FILE_INE "
					+ "(REQUESTFILEID, TICKETNUMBER,  DOCUMENTTYPE, PROCESSTYPE, COMITETYPE, AMBITOTYPE, "
					+ "ENTITY, CONTABILIDADID, OBSERVATION, CREATEDBY, CREATEDON, UPDATEDBY, UPDATEDON) "
					+ "VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , 'SYSTEM', NOW(), 'SYSTEM', NOW() ) ");

			for (IneDto data : ineDtos) {
				ps.setLong(1, data.getRequestFileId());
				ps.setString(2, data.getTicketNumber());
				ps.setString(3, data.getDocumentType());
				ps.setString(4, data.getProcessType());
				ps.setString(5, data.getComiteType());
				ps.setString(6, data.getAmbitoType());
				ps.setString(7, data.getEntity());
				ps.setString(8, data.getContabilidadId());
				ps.setString(9, data.getObservation());
				ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL GRABAR TABLA Request_File_Ine : {}", ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public RequestFileDto save(RequestFileDto requestFileDto) throws BaseException {
		LOGGER.info("save() -> :: " + GSON.toJson(requestFileDto));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO REQUEST_FILES "
					+ "(USER, FILETYPE, FILEPATHORIGINAL, FILEPATHWORKING, FILEPATHRESULT, REQUESTDATETIME, ORIGNUMBERTICKETS, CALCULATEDNUMBERTICKETS, "
					+ "NUMBERTICKETSPROCESED, STATUS, CREATEDBY, CREATEDON, UPDATEDBY, UPDATEDON) "
					+ "VALUES ( ? , ? , ? , ? , ? , NOW(), ? , ? , ? , ? , 'SYSTEM', NOW(), 'SYSTEM', NOW()) ",
					Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, requestFileDto.getUser());
			ps.setString(2, requestFileDto.getFileType());
			ps.setString(3, requestFileDto.getFilePathOriginal());
			ps.setString(4, requestFileDto.getFilePathWorking());
			ps.setString(5, requestFileDto.getFilePathResult());
			ps.setInt(6, Utils.getInt(requestFileDto.getOrigNumberTickets()));
			ps.setInt(7, Utils.getInt(requestFileDto.getCalculatedNumberTickets()));
			ps.setInt(8, Utils.getInt(requestFileDto.getNumberTicketsProcesed()));
			ps.setInt(9, requestFileDto.getStatus());

			ps.executeUpdate();
			try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					requestFileDto.setId(generatedKeys.getLong(1));
				} else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}

			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL GRABAR REQUEST FILE : {}", ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return requestFileDto;
	}

	@Override
	public RequestFileDto getDataGenerateMasiveFile(String requestFileId) throws BaseException {
		LOGGER.info("getDataGenerateFile() -> :: requestFileId:  " + requestFileId);
		HashMap<Long, RequestFileDto> requestFileMap = new HashMap<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_GENERATE_MASIVE_FILE);
			ps.setString(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				parseRequestMasiveFileDto(rs, requestFileMap);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		List<RequestFileDto> response = new ArrayList<RequestFileDto>(requestFileMap.values());
		return (response != null && response.size()>0)?response.get(0):null;
	}

	@Override
	public RequestFileDto getDataSendMailMasive(String requestFileId) throws BaseException {
		LOGGER.info("getDataSendMailFile() -> :: requestFileId:  " + requestFileId);
		HashMap<Long, RequestFileDto> requestFileMap = new HashMap<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_SEND_MAIL_MASIVO);
			ps.setString(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				parseRequestMasiveFileDto(rs, requestFileMap);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		List<RequestFileDto> response = new ArrayList<RequestFileDto>(requestFileMap.values());
		return (response != null && response.size()>0)?response.get(0):null;
	}
	
	private void parseRequestMasiveFileDto(ResultSet rs, HashMap<Long, RequestFileDto> requestFileMap) throws SQLException {
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);

		if (!requestFileMap.containsKey(requestFileId)) {
			String user = rs.getString(QueryAlias.USER);
			Date requestDateTime = rs.getTimestamp(QueryAlias.CREATED_ON);
			Integer numberTicketsProcesed = rs.getInt(QueryAlias.NUMBER_TICKETS_PROCESED);
			Integer calculatedNumberTickets = rs.getInt(QueryAlias.CALCULATED_NUMBER_TICKETS_PROCESED);
			Integer origNumberTickets = rs.getInt(QueryAlias.ORIGIN_NUMBER_TICKETS);
			Integer requestFileStatus = rs.getInt(QueryAlias.REQUEST_FILE_STATUS);
			String filePathWorking = rs.getString(QueryAlias.FILE_PATH_WORKING);

			RequestFileDto requestFileDto = new RequestFileDto();
			
			requestFileDto.setId(requestFileId);
			requestFileDto.setCalculatedNumberTickets(calculatedNumberTickets);
			requestFileDto.setOrigNumberTickets(origNumberTickets);
			requestFileDto.setNumberTicketsProcesed(numberTicketsProcesed);
			requestFileDto.setStatus(requestFileStatus);
			requestFileDto.setUser(user);
			requestFileDto.setRequestDateTime(requestDateTime);
			requestFileDto.setFilePathWorking(filePathWorking);
			requestFileDto.setLstMasiveFileDto(new ArrayList<>());
			requestFileDto.getLstMasiveFileDto().add(buildMasiveFileDto(rs));
			requestFileMap.put(requestFileId, requestFileDto);
		} else {
			RequestFileDto requestFileDto = requestFileMap.get(requestFileId);
			requestFileDto.getLstMasiveFileDto().add(buildMasiveFileDto(rs));
			requestFileMap.put(requestFileId, requestFileDto);
		}
	}
	
	private void parseRequestEspejoFileDto(ResultSet rs, HashMap<Long, RequestFileDto> requestFileMap) throws SQLException {
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);

		if (!requestFileMap.containsKey(requestFileId)) {
			String user = rs.getString(QueryAlias.USER);
			Date requestDateTime = rs.getTimestamp(QueryAlias.CREATED_ON);
			Integer numberTicketsProcesed = rs.getInt(QueryAlias.NUMBER_TICKETS_PROCESED);
			Integer calculatedNumberTickets = rs.getInt(QueryAlias.CALCULATED_NUMBER_TICKETS_PROCESED);
			Integer origNumberTickets = rs.getInt(QueryAlias.ORIGIN_NUMBER_TICKETS);
			Integer requestFileStatus = rs.getInt(QueryAlias.REQUEST_FILE_STATUS);
			String filePathWorking = rs.getString(QueryAlias.FILE_PATH_WORKING);

			RequestFileDto requestFileDto = new RequestFileDto();
			requestFileDto.setId(requestFileId);
			requestFileDto.setCalculatedNumberTickets(calculatedNumberTickets);
			requestFileDto.setOrigNumberTickets(origNumberTickets);
			requestFileDto.setNumberTicketsProcesed(numberTicketsProcesed);
			requestFileDto.setStatus(requestFileStatus);
			requestFileDto.setUser(user);
			requestFileDto.setRequestDateTime(requestDateTime);
			requestFileDto.setFilePathWorking(filePathWorking);
			requestFileDto.setLstEspejoFileDto(new ArrayList<>());
			requestFileDto.getLstEspejoFileDto().add(buildEspejoFileDto(rs));
			requestFileMap.put(requestFileId, requestFileDto);
		} else {
			RequestFileDto requestFileDto = requestFileMap.get(requestFileId);
			requestFileDto.getLstEspejoFileDto().add(buildEspejoFileDto(rs));
			requestFileMap.put(requestFileId, requestFileDto);
		}
	}

	private MasiveFileDto buildMasiveFileDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.AUTO_ID);
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);
		String ticketNumber = rs.getString(QueryAlias.TICKET_NUMBER);
		String ticketType = rs.getString(QueryAlias.TYPE_TICKET);
		String rfc = rs.getString(QueryAlias.RFC);
		String companyName = rs.getString(QueryAlias.COMPANY_NAME);
		String zipCode = rs.getString(QueryAlias.ZIP_CODE);
		String fileName = rs.getString(QueryAlias.FILE_NAME);
		String Reference = rs.getString(QueryAlias.REFERENCE);
		String Email = rs.getString(QueryAlias.EMAIL);
		String Status = rs.getString(QueryAlias.STATUS);
		String uuid = rs.getString(QueryAlias.UUID);
		String observation = rs.getString(QueryAlias.OBSERVATION);
		String pathpdf = rs.getString(QueryAlias.PATH_PDF);
		String pathxml = rs.getString(QueryAlias.PATH_XML);
		Date lastUpdate = rs.getTimestamp(QueryAlias.LAST_UPDATE);
		Date lastVerified = rs.getTimestamp(QueryAlias.LAST_VERIFIED);
		String createdBy = rs.getString(QueryAlias.CREATED_BY);
		Date createdOn = rs.getTimestamp(QueryAlias.CREATED_ON);
		String updatedBy = rs.getString(QueryAlias.UPDATED_BY);
		Date updatedOn = rs.getTimestamp(QueryAlias.UPDATED_ON);
		String serie = rs.getString(QueryAlias.SERIE);
		String transaction = rs.getString(QueryAlias.TRANSACTION);

		MasiveFileDto masiveFileDto = new MasiveFileDto();
		masiveFileDto.setId(id);
		masiveFileDto.setRequestFileId(requestFileId);
		masiveFileDto.setTicketNumber(ticketNumber);
		masiveFileDto.setTicketType(ticketType);
		masiveFileDto.setRfc(rfc);
		masiveFileDto.setCompanyName(companyName);
		masiveFileDto.setZipCode(zipCode);
		masiveFileDto.setFileName(fileName);
		masiveFileDto.setReference(Reference);
		masiveFileDto.setEmail(Email);
		masiveFileDto.setUuid(uuid);
		masiveFileDto.setObservation(observation);
		masiveFileDto.setPathpdf(pathpdf);
		masiveFileDto.setPathxml(pathxml);
		masiveFileDto.setStatus(Status);
		masiveFileDto.setLastUpdate(lastUpdate);
		masiveFileDto.setLastVerified(lastVerified);
		masiveFileDto.setCreatedBy(createdBy);
		masiveFileDto.setCreatedOn(createdOn);
		masiveFileDto.setUpdatedBy(updatedBy);
		masiveFileDto.setUpdatedOn(updatedOn);
		masiveFileDto.setSerie(serie);
		masiveFileDto.setTransaction(transaction);
		
		return masiveFileDto;
	}

	private EspejoFileDto buildEspejoFileDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.AUTO_ID);
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);
		String airlineCode = rs.getString(QueryAlias.AIR_LINE_CODE);
		String voucherNumber = rs.getString(QueryAlias.VOUCHER_NUMBER);
		String iata = rs.getString(QueryAlias.IATA);
		Double tarifa0 = rs.getDouble(QueryAlias.TARIFA_0);
		Double iva0 = rs.getDouble(QueryAlias.IVA_0);
		Double tarifa16 = rs.getDouble(QueryAlias.TARIFA_16);
		Double iva16 = rs.getDouble(QueryAlias.IVA_16);
		Double tua = rs.getDouble(QueryAlias.TUA);
		Double otrosImp = rs.getDouble(QueryAlias.OTROS_IMP);
		Double total = rs.getDouble(QueryAlias.TOTAL);
		String currencyType = rs.getString(QueryAlias.CURRENCY_TYPE);
		String paymentType = rs.getString(QueryAlias.PAYMENT_TYPE);
		String rfcDep = rs.getString(QueryAlias.RFC_DEP);
		String pax = rs.getString(QueryAlias.PAX);
		String ruta = rs.getString(QueryAlias.RUTA);
		String tipDocto = rs.getString(QueryAlias.TIP_DOCTO);
		String Email = rs.getString(QueryAlias.EMAIL);
		String Status = rs.getString(QueryAlias.STATUS);
		String uuid = rs.getString(QueryAlias.UUID);
		String observation = rs.getString(QueryAlias.OBSERVATION);
		String pathpdf = rs.getString(QueryAlias.PATH_PDF);
		String pathxml = rs.getString(QueryAlias.PATH_XML);
		Date lastUpdate = rs.getTimestamp(QueryAlias.LAST_UPDATE);
		Date lastVerified = rs.getTimestamp(QueryAlias.LAST_VERIFIED);
		String createdBy = rs.getString(QueryAlias.CREATED_BY);
		Date createdOn = rs.getTimestamp(QueryAlias.CREATED_ON);
		String updatedBy = rs.getString(QueryAlias.UPDATED_BY);
		Date updatedOn = rs.getTimestamp(QueryAlias.UPDATED_ON);

		EspejoFileDto espejoFileDto = new EspejoFileDto();
		espejoFileDto.setId(id);
		espejoFileDto.setRequestFileId(requestFileId);
		espejoFileDto.setAirlineCode(airlineCode);
		espejoFileDto.setVoucherNumber(voucherNumber);
		espejoFileDto.setIata(iata);
		espejoFileDto.setTarifa0(tarifa0);
		espejoFileDto.setIva0(iva0);
		espejoFileDto.setTarifa16(tarifa16);
		espejoFileDto.setIva16(iva16);
		espejoFileDto.setTua(tua);
		espejoFileDto.setOtrosImp(otrosImp);
		espejoFileDto.setTotal(total);
		espejoFileDto.setCurrencyType(currencyType);
		espejoFileDto.setPaymentType(paymentType);
		espejoFileDto.setRfcDep(rfcDep);
		espejoFileDto.setPax(pax);
		espejoFileDto.setRuta(ruta);
		espejoFileDto.setTipDocto(tipDocto);
		espejoFileDto.setEmail(Email);
		espejoFileDto.setUuid(uuid);
		espejoFileDto.setObservation(observation);
		espejoFileDto.setPathpdf(pathpdf);
		espejoFileDto.setPathxml(pathxml);
		espejoFileDto.setStatus(Status);
		espejoFileDto.setLastUpdate(lastUpdate);
		espejoFileDto.setLastVerified(lastVerified);
		espejoFileDto.setCreatedBy(createdBy);
		espejoFileDto.setCreatedOn(createdOn);
		espejoFileDto.setUpdatedBy(updatedBy);
		espejoFileDto.setUpdatedOn(updatedOn);
		return espejoFileDto;
	}
	
	@Override
	public List<IneDto> getDataIne(String requestFileId) throws BaseException {
		LOGGER.info("getDataIne() -> :: requestFileId : " + requestFileId);
		List<IneDto> response = new ArrayList<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_INE_BY_REQUEST_ID);
			ps.setString(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				response.add(getIneDto(rs));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

	private IneDto getIneDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.INE_ID);
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);
		String ambitoType = rs.getString(QueryAlias.AMBITO_TYPE);
		String comiteType = rs.getString(QueryAlias.COMITE_TYPE);
		String contabilidadId = rs.getString(QueryAlias.CONTABILIDAD_ID);
		String documentType = rs.getString(QueryAlias.DOCUMENT_TYPE);
		String entity = rs.getString(QueryAlias.ENTITY);
		String observation = rs.getString(QueryAlias.INE_OBSERVATION);
		String ticketNumber = rs.getString(QueryAlias.TICKET_NUMBER);
		String processType = rs.getString(QueryAlias.PROCESS_TYPE);

		IneDto ineDto = new IneDto();
		ineDto.setId(id);
		ineDto.setRequestFileId(requestFileId);
		ineDto.setAmbitoType(ambitoType);
		ineDto.setComiteType(comiteType);
		ineDto.setContabilidadId(contabilidadId);
		ineDto.setDocumentType(documentType);
		ineDto.setEntity(entity);
		ineDto.setObservation(observation);
		ineDto.setProcessType(processType);
		ineDto.setRequestFileId(requestFileId);
		ineDto.setTicketNumber(ticketNumber);
		return ineDto;
	}

	@Override
	public RequestFileDto getDataGenerateEspejoFile(String requestFileId) throws BaseException {
		LOGGER.info("getDataGenerateEspejoFile() -> :: requestFileId:  " + requestFileId);
		HashMap<Long, RequestFileDto> requestFileMap = new HashMap<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_GENERATE_ESPEJO_FILE);
			ps.setString(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				parseRequestEspejoFileDto(rs, requestFileMap);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		List<RequestFileDto> response = new ArrayList<RequestFileDto>(requestFileMap.values());
		return (response != null && response.size()>0)?response.get(0):null;
	}

	@Override
	public RequestFileDto getDataSendMailEspejo(String requestFileId) throws BaseException {
		LOGGER.info("getDataSendMailEspejo() -> :: requestFileId:  " + requestFileId);
		HashMap<Long, RequestFileDto> requestFileMap = new HashMap<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_SEND_MAIL_ESPEJO);
			ps.setString(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				parseRequestEspejoFileDto(rs, requestFileMap);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		List<RequestFileDto> response = new ArrayList<RequestFileDto>(requestFileMap.values());
		return (response != null && response.size()>0)?response.get(0):null;
	}
	
}