package com.miatech.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.DBConection;
import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Constants.QueryAlias;

public class ProcessMasiveFileDaoImpl implements ProcessMasiveFileDao {

	private static final Logger LOGGER = LogManager.getLogger(ProcessMasiveFileDaoImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	@Override
	public void saveMasiveFile(List<MasiveFileDto> masiveFileDtos) throws BaseException {
		LOGGER.info("saveMasiveFile() -> :: " + GSON.toJson(masiveFileDtos));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO PROCESS_MASIVE_FILE "
					+ "(TICKETNUMBER, REQUESTFILEID,  RFC, COMPANYNAME, ZIPCODE, FILENAME, REFERENCE, EMAIL, STATUS, "
					+ "CREATEDBY, CREATEDON, UPDATEDBY, UPDATEDON) " 
					+ "VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , "
					+ "'SYSTEM', NOW(), 'SYSTEM', NOW() ) ");

			for (MasiveFileDto data : masiveFileDtos) {
				ps.setString(1, data.getTicketNumber());
				ps.setLong(2, data.getRequestFileId());
				ps.setString(3, data.getRfc());
				ps.setString(4, data.getCompanyName());
				ps.setString(5, data.getZipCode());
				ps.setString(6, data.getFileName());
				ps.setString(7, data.getReference());
				ps.setString(8, data.getEmail());
				ps.setString(9, data.getStatus());
				ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL GRABAR DATA ROW FILE : {}", ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<MasiveFileDto> getByRequestFileId(Long requestFileId) throws BaseException {
		LOGGER.info("getByRequestFileId() -> :: ");
		List<MasiveFileDto> response = new ArrayList<>();

		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_PROCESS_MASIVE_FILE_BY_REQUEST_ID);
			ps.setLong(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				response.add(getProcessMasiveFileDto(rs));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return response;
	}

	private MasiveFileDto getProcessMasiveFileDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.AUTO_ID);
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);
		String ticketNumber = rs.getString(QueryAlias.TICKET_NUMBER);
		String rfc = rs.getString(QueryAlias.RFC);
		String companyName = rs.getString(QueryAlias.COMPANY_NAME);
		String zipCode = rs.getString(QueryAlias.ZIP_CODE);
		String fileName = rs.getString(QueryAlias.FILE_NAME);
		String Reference = rs.getString(QueryAlias.REFERENCE);
		String Email = rs.getString(QueryAlias.EMAIL);
		String uuid = rs.getString(QueryAlias.UUID);
		String observation = rs.getString(QueryAlias.OBSERVATION);
		String PathXml = rs.getString(QueryAlias.PATH_XML);
		String PathPdf = rs.getString(QueryAlias.PATH_PDF);
		String Status = rs.getString(QueryAlias.STATUS);
		Date lastUpdate = rs.getTimestamp(QueryAlias.LAST_UPDATE);
		Date lastVerified = rs.getTimestamp(QueryAlias.LAST_VERIFIED);
		String createdBy = rs.getString(QueryAlias.CREATED_BY);
		Date createdOn = rs.getTimestamp(QueryAlias.CREATED_ON);
		String updatedBy = rs.getString(QueryAlias.UPDATED_BY);
		Date updatedOn = rs.getTimestamp(QueryAlias.UPDATED_ON);

		MasiveFileDto processMasiveFileDto = new MasiveFileDto();
		processMasiveFileDto.setId(id);
		processMasiveFileDto.setRequestFileId(requestFileId);
		processMasiveFileDto.setTicketNumber(ticketNumber);
		processMasiveFileDto.setRfc(rfc);
		processMasiveFileDto.setCompanyName(companyName);
		processMasiveFileDto.setZipCode(zipCode);
		processMasiveFileDto.setFileName(fileName);
		processMasiveFileDto.setReference(Reference);
		processMasiveFileDto.setEmail(Email);
		processMasiveFileDto.setStatus(Status);
		processMasiveFileDto.setLastUpdate(lastUpdate);
		processMasiveFileDto.setLastVerified(lastVerified);
		processMasiveFileDto.setUuid(uuid);
		processMasiveFileDto.setObservation(observation);
		processMasiveFileDto.setPathxml(PathXml);
		processMasiveFileDto.setPathpdf(PathPdf);
		processMasiveFileDto.setCreatedBy(createdBy);
		processMasiveFileDto.setCreatedOn(createdOn);
		processMasiveFileDto.setUpdatedBy(updatedBy);
		processMasiveFileDto.setUpdatedOn(updatedOn);
		return processMasiveFileDto;
	}

	@Override
	public void saveEspejoFile(List<EspejoFileDto> espejoFileDto) throws BaseException {
		LOGGER.info("saveEspejoFile() -> :: " + GSON.toJson(espejoFileDto));
		
		try (Connection conn = new DBConection().getConnection();
				PreparedStatement stmt = conn
						.prepareStatement("INSERT INTO MIATECH.PROCESS_ESPEJO_FILE "
								+"( REQUESTFILEID, AIRLINECODE, VOUCHERNUMBER, IATA, TARIFA_0, IVA_0, TARIFA_16, IVA_16, TUA, OTROS_IMP, "
								+"TOTAL, CURRENCYTYPE, PAYMENTTYPE, RFCDEP, PAX, RUTA, TIPDOCTO, EMAIL, STATUS, OBSERVACIONES "
								+"CREATEDBY, CREATEDON, UPDATEDBY, UPDATEDON ) " 
								+"VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , "
								+ "		   ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? "
								+ "		   'SYSTEM', NOW(), 'SYSTEM', NOW() ) ");) {

			for (EspejoFileDto data : espejoFileDto) {
				stmt.setLong(1, data.getRequestFileId());
				stmt.setString(2, data.getAirlineCode());
				stmt.setString(3, data.getVoucherNumber());
				stmt.setString(4, data.getIata());
				stmt.setDouble(5, data.getTarifa0());
				stmt.setDouble(6, data.getIva0());
				stmt.setDouble(7, data.getTarifa16());
				stmt.setDouble(8, data.getIva16());
				stmt.setDouble(9, data.getTua());
				stmt.setDouble(10, data.getOtrosImp());
				stmt.setDouble(11, data.getTotal());
				stmt.setString(12, data.getCurrencyType());
				stmt.setString(13, data.getPaymentType());
				stmt.setString(14, data.getRfcDep());
				stmt.setString(15, data.getPax());
				stmt.setString(16, data.getRuta());
				stmt.setString(17, data.getTipDocto());
				stmt.setString(18, data.getEmail());
				stmt.setString(19, data.getStatus());
				stmt.setString(20, data.getObservaciones());
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR saveEspejoFile() {}", ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		}
	}

}
