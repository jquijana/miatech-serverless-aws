package com.miatech.service.service;

import com.miatech.service.exception.BaseException;

public interface SendMailService {

	void sendMailMasivo(String requestFileId) throws BaseException;
	
	void sendMailEspejo(String requestFileId) throws BaseException;
}
