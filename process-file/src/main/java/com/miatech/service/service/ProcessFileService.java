package com.miatech.service.service;

import java.io.IOException;
import java.io.InputStream;

import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;

public interface ProcessFileService {

	void saveMasiveFile(String fileName, InputStream inputStream) throws BusinessException, BaseException, IOException;

	void saveEspejoFile(String fileName, InputStream inputStream) throws BusinessException, BaseException, IOException;

	RequestFileDto saveRequestFile(RequestFileDto requestFileDto) throws BusinessException, BaseException;

	void updateRequestFile(RequestFileDto requestFileDto) throws BaseException;
}
