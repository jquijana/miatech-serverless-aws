package com.miatech.service.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.ValidationFileDao;
import com.miatech.service.dao.ValidationFileDaoImpl;
import com.miatech.service.dto.ValidationFileDto;
import com.miatech.service.dto.ValidationFileRq;
import com.miatech.service.dto.ValidationFileRs;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;

public class ValidationFileServiceImpl implements ValidationFileService {

	private ValidationFileDao validationFileDao;

	private static final Logger LOGGER = LogManager.getLogger(ValidationFileServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(Constants.DATEFORMAT7).create();
	
	public ValidationFileServiceImpl() throws BaseException {
		this.validationFileDao = new ValidationFileDaoImpl();
	}

	@Override
	public ValidationFileRs validationFile(ValidationFileRq validationFileRq) throws BaseException {
		LOGGER.info("validationFile() -->  request : {} ", GSON.toJson(validationFileRq));
		
		Map<String, ValidationFileDto> validationsMap = validationFileDao.getMapById(validationFileRq.getValidationType(), false);
		if (validationFileRq.getFields()!=null) {
			validationFileRq.getFields().forEach(data -> {
				if (validationsMap.containsKey(data.getField())) {
					validationsMap.remove(data.getField());
				}
			});
		}
		
		ValidationFileRs response = new ValidationFileRs();
		List<String> faltantes = new ArrayList<String>(validationsMap.keySet());
		if (faltantes!=null && !faltantes.isEmpty()) {
			response.setStatus(Boolean.FALSE);
			faltantes.forEach(data->{
				response.setMessage(response.getMessage()!=null? (response.getMessage()+ " | " + data) : "CAMPOS FALTANTES : " + data);
			});
		}else {
			response.setStatus(Boolean.TRUE);
			response.setMessage("Archivo Válido");
		}
		return response;
	}	
}
