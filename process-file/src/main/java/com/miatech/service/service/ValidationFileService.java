package com.miatech.service.service;

import com.miatech.service.dto.ValidationFileRq;
import com.miatech.service.dto.ValidationFileRs;
import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;

public interface ValidationFileService {

	ValidationFileRs validationFile(ValidationFileRq validationFileRq) throws BusinessException, BaseException;
	
}
