package com.miatech.service.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.ProcessMasiveFileDao;
import com.miatech.service.dao.ProcessMasiveFileDaoImpl;
import com.miatech.service.dao.RequestFileDao;
import com.miatech.service.dao.RequestFileDaoImpl;
import com.miatech.service.dao.ValidationFileDao;
import com.miatech.service.dao.ValidationFileDaoImpl;
import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.dto.IneDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.dto.ValidationFileDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;
import com.miatech.service.util.Utils;
import com.miatech.service.util.enums.ValidationEspejoFieldEnum;
import com.miatech.service.util.enums.ValidationMasiveFieldEnum;

public class ProcessFileServiceImpl implements ProcessFileService {

	private ProcessMasiveFileDao processMasiveFileDao;
	private RequestFileDao requestFileDao;
	private ValidationFileDao validationFileDao;
	
	private static final Logger LOGGER = LogManager.getLogger(ProcessFileServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	private static final String ESTADO_PENDIENTE = "PENDIENTE";
	private static final Integer ESTADO_REQUEST_FILE_ENVIO_PARCIAL = 2;
	private static final Integer ESTADO_REQUEST_FILE_COMPLETO = 9;
	private static final String CARGA_MASIVA = "CARGA_MASIVA";
	private static final String COMP_INE = "COMP_INE";
	private static final String FORMATO_ESPEJO = "FORMATO_ESPEJO";

	public ProcessFileServiceImpl() throws BaseException {
		this.processMasiveFileDao = new ProcessMasiveFileDaoImpl();
		this.requestFileDao = new RequestFileDaoImpl();
		this.validationFileDao = new ValidationFileDaoImpl();
	}

	@Override
	public void saveMasiveFile(String fileName, InputStream inputStream) throws BaseException, IOException {
		LOGGER.info("saveMasiveFile() --> " + GSON.toJson(fileName));

		Long requestFileId = Long.valueOf(fileName.substring(fileName.lastIndexOf("-") + 1, fileName.lastIndexOf(".")));
		RequestFileDto requestFileDto = requestFileDao.getRequestFileById(requestFileId);
		if (requestFileDto != null) {	
			requestFileDto.setFilePathWorking(fileName);
			Workbook workbook = new XSSFWorkbook(inputStream);
			saveFacturas(requestFileDto, workbook);
			saveIne(requestFileDto, workbook);
			workbook.close();
			inputStream.close();
		}
	}

	private void saveFacturas(RequestFileDto requestFileDto, Workbook workbook) throws BaseException {
		LOGGER.info("saveFacturas() --> " + GSON.toJson(requestFileDto));
		Map<String, ValidationFileDto> validationsMapMasive = validationFileDao.getMapById(CARGA_MASIVA, true);
		
		List<MasiveFileDto> processMasiveFileDtos = new ArrayList<>();
		MasiveFileDto processMasiveFileDto = null;
		Integer origNumberTickets = 0;

		Integer indexFactura = workbook.getSheetIndex("Facturas");
		Sheet firstSheet = workbook.getSheetAt(indexFactura);
		Iterator<Row> iterator = firstSheet.iterator();

		int indTicketNumber = -1, indRfc = -1, indCompanyName = -1, indFileName = -1, indZipCode = -1,
				indReference = -1, indEmail = -1;
		while (iterator.hasNext()) {
			Row row = iterator.next();
			if (row.getRowNum() == 0) {
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					try {
						Cell cell = cellIterator.next();
						String data = (String) Utils.getValueFromCell(cell);
//						LOGGER.info("TIPO :: --> "+ cell.getCellType() +", DATA  :: --> " + data + ", INDICE :: --> " + indice);

						if (data.equals(validationsMapMasive.get(ValidationMasiveFieldEnum.NO_ETICKET_PNR.getId()).getName())) {
							indTicketNumber = cell.getColumnIndex();
						}

						if (data.equals(validationsMapMasive.get(ValidationMasiveFieldEnum.RFC.getId()).getName())) {
							indRfc = cell.getColumnIndex();
						}

						if (data.equals(validationsMapMasive.get(ValidationMasiveFieldEnum.NOMBRE_RAZON_SOCIAL.getId()).getName())) {
							indCompanyName = cell.getColumnIndex();
						}

						if (data.equals(validationsMapMasive.get(ValidationMasiveFieldEnum.NOMBRE_ARCHIVO.getId()).getName())) {
							indFileName = cell.getColumnIndex();
						}

						if (data.equals(validationsMapMasive.get(ValidationMasiveFieldEnum.CORREO_ELECTRONICO.getId()).getName())) {
							indEmail = cell.getColumnIndex();
						}

						if (data.equals(validationsMapMasive.get(ValidationMasiveFieldEnum.CODIGO_POSTAL.getId()).getName())) {
							indZipCode = cell.getColumnIndex();
						}

						if (data.equals(validationsMapMasive.get(ValidationMasiveFieldEnum.REFERENCIA.getId()).getName())) {
							indReference = cell.getColumnIndex();
						}

					} catch (Exception e) {
						LOGGER.error(" ERROR OBTENER DATA CABECERA:: --> {} ", ExceptionUtils.getStackTrace(e));
					}
				}
			} else {
				processMasiveFileDto = new MasiveFileDto();
				processMasiveFileDto.setStatus(ESTADO_PENDIENTE);
				processMasiveFileDto.setRequestFileId(requestFileDto.getId());
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					try {
						Cell cell = cellIterator.next();
						String data = (String) Utils.getValueFromCell(cell);
//						LOGGER.info("TIPO :: --> "+ cell.getCellType() +", DATA  :: --> " + data + ", INDICE :: --> " + cell.getColumnIndex());

						if (cell.getColumnIndex() == indTicketNumber) {
							processMasiveFileDto.setTicketNumber(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indRfc) {
							processMasiveFileDto.setRfc(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indCompanyName) {
							processMasiveFileDto.setCompanyName(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indFileName) {
							processMasiveFileDto.setFileName(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indRfc) {
							processMasiveFileDto.setRfc(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indZipCode) {
							processMasiveFileDto.setZipCode(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indEmail) {
							processMasiveFileDto.setEmail(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indReference) {
							processMasiveFileDto.setReference(Utils.getValue(data));
						}
					} catch (Exception e) {
						LOGGER.error(" ERROR OBTENER DATA :: --> {}", ExceptionUtils.getStackTrace(e));
					}
				}

				if (processMasiveFileDto.getTicketNumber() != null && !processMasiveFileDto.getTicketNumber().isEmpty()) {
					processMasiveFileDtos.add(processMasiveFileDto);
					origNumberTickets++;
				}
			}
		}
		
		processMasiveFileDao.saveMasiveFile(processMasiveFileDtos);
		requestFileDto.setOrigNumberTickets(origNumberTickets);
		requestFileDto.setCalculatedNumberTickets(origNumberTickets);
		requestFileDao.updateRequestFile(requestFileDto);
	}

	private void saveIne(RequestFileDto requestFileDto, Workbook workbook) throws BaseException {
		LOGGER.info("saveIne() --> " + GSON.toJson(requestFileDto));
		Map<String, ValidationFileDto> validationsMap = validationFileDao.getMapById(COMP_INE, true);

		List<IneDto> ineDtos = new ArrayList<>();
		IneDto ineDto = null;

		Integer indexIne = workbook.getSheetIndex("INE");
		Sheet firstSheet = workbook.getSheetAt(indexIne);
		Iterator<Row> iterator = firstSheet.iterator();

		int indTicketNumber = -1, indDocumentType = -1, indProcessType = -1, indComiteType = -1, indAmbitoType = -1,
				indEntity = -1, indContabilidadId = -1, indObservation = -1;
		while (iterator.hasNext()) {
			Row row = iterator.next();

			// Leemos cabecera
			if (row.getRowNum() == 0) {
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					try {
						Cell cell = cellIterator.next();
						String data = (String) Utils.getValueFromCell(cell);
//						LOGGER.info("TIPO :: --> "+ cell.getCellType() +", DATA  :: --> " + data + ", INDICE :: --> " + indice);

						if (data.equals(validationsMap.get(ValidationMasiveFieldEnum.ETICKET.getId()).getName())) {
							indTicketNumber = cell.getColumnIndex();
						}

						if (data.equals(validationsMap.get(ValidationMasiveFieldEnum.TIPO_DOCTO.getId()).getName())) {
							indDocumentType = cell.getColumnIndex();
						}

						if (data.equals(validationsMap.get(ValidationMasiveFieldEnum.TIPO_PROCESO.getId()).getName())) {
							indProcessType = cell.getColumnIndex();
						}

						if (data.equals(validationsMap.get(ValidationMasiveFieldEnum.TIPO_COMITE.getId()).getName())) {
							indComiteType = cell.getColumnIndex();
						}

						if (data.equals(validationsMap.get(ValidationMasiveFieldEnum.TIPO_AMBITO.getId()).getName())) {
							indAmbitoType = cell.getColumnIndex();
						}

						if (data.equals(validationsMap.get(ValidationMasiveFieldEnum.CLAVE_ENTIDAD.getId()).getName())) {
							indEntity = cell.getColumnIndex();
						}

						if (data.equals(validationsMap.get(ValidationMasiveFieldEnum.ID_CONTABILIDAD.getId()).getName())) {
							indContabilidadId = cell.getColumnIndex();
						}

					} catch (Exception e) {
						LOGGER.error(" ERROR OBTENER DATA CABECERA:: --> {}", ExceptionUtils.getStackTrace(e));
					}
				}
			} else {
				ineDto = new IneDto();
				ineDto.setRequestFileId(requestFileDto.getId());
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					try {
						Cell cell = cellIterator.next();
						String data = (String) Utils.getValueFromCell(cell);
//						LOGGER.info("TIPO :: --> "+ cell.getCellType() +", DATA  :: --> " + data + ", INDICE :: --> " + cell.getColumnIndex());

						if (cell.getColumnIndex() == indTicketNumber) {
							ineDto.setTicketNumber(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indDocumentType) {
							ineDto.setDocumentType(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indProcessType) {
							ineDto.setProcessType(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indComiteType) {
							ineDto.setComiteType(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indAmbitoType) {
							ineDto.setAmbitoType(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indEntity) {
							ineDto.setEntity(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indContabilidadId) {
							ineDto.setContabilidadId(Utils.getValue(data));
						}

						if (cell.getColumnIndex() == indObservation) {
							ineDto.setObservation(Utils.getValue(data));
						}
					} catch (Exception e) {
						LOGGER.error(" ERROR OBTENER DATA :: --> {}", ExceptionUtils.getStackTrace(e));
					}
				}

				if (ineDto.getTicketNumber() != null && !ineDto.getTicketNumber().isEmpty()) {
					ineDtos.add(ineDto);
				}

			}
		}

		try {
			requestFileDao.saveIne(ineDtos);
		} catch (BaseException e) {
			LOGGER.error(" ERROR GRABANDO DATOS MASIVOS :: --> {}", ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public RequestFileDto saveRequestFile(RequestFileDto requestFileDto) throws BaseException {
		LOGGER.info("saveRequestFile() --> " + GSON.toJson(requestFileDto));
		return requestFileDao.save(requestFileDto);
	}
	
	@Override
	public void updateRequestFile(RequestFileDto requestFileDto) throws BaseException {
		LOGGER.info("updateRequestFile() --> " + GSON.toJson(requestFileDto));
		if ((requestFileDto.getNumberTicketsProcesed() < requestFileDto.getCalculatedNumberTickets())) {
			requestFileDto.setStatus(ESTADO_REQUEST_FILE_ENVIO_PARCIAL);
		} else if((requestFileDto.getNumberTicketsProcesed() == requestFileDto.getCalculatedNumberTickets())){
			requestFileDto.setStatus(ESTADO_REQUEST_FILE_COMPLETO);
		}
		requestFileDao.updateRequestFile(requestFileDto);
	}

	
	@Override
	public void saveEspejoFile(String fileName, InputStream inputStream)
			throws BusinessException, BaseException, IOException {
		LOGGER.info("saveEspejoFile() --> " + GSON.toJson(fileName));

		Long requestFileId = Long.valueOf(fileName.substring(fileName.lastIndexOf("-") + 1, fileName.lastIndexOf(".")));
		RequestFileDto requestFileDto = requestFileDao.getRequestFileById(requestFileId);
		if (requestFileDto != null) {	
			requestFileDto.setFilePathWorking(fileName);
			Workbook workbook = new XSSFWorkbook(inputStream);
			saveEspejo(requestFileDto, workbook);
			workbook.close();
			inputStream.close();
		}
	}
	
	
	private void saveEspejo(RequestFileDto requestFileDto, Workbook workbook) throws BaseException {
		LOGGER.info("saveEspejo() --> " + GSON.toJson(requestFileDto));
		Map<String, ValidationFileDto> validationsMap = validationFileDao.getMapById(FORMATO_ESPEJO, true);

		List<EspejoFileDto> espejoFileDtos = new ArrayList<>();
		EspejoFileDto espejoFileDto = null;
		Integer origNumberTickets = 0;
		Integer indexFactura = workbook.getSheetIndex("Espejo");
		Sheet firstSheet = workbook.getSheetAt(indexFactura);
		Iterator<Row> iterator = firstSheet.iterator();

		int airlineCode = -1, voucherNumber = -1, iata = -1, tarifa0 = -1, tarifa16 = -1,
				tua = -1, iva0 = -1, iva16 = -1, otrosImp = -1, total = -1, currencyType = -1, paymentType = -1,
						rfcDep = -1, pax = -1, ruta = -1, tipDocto = -1, observation = -1, email = -1;
		while (iterator.hasNext()) {
			Row row = iterator.next();
			if (row.getRowNum() == 0) {
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					try {
						Cell cell = cellIterator.next();
						String data = (String) Utils.getValueFromCell(cell);
//						LOGGER.info("TIPO :: --> "+ cell.getCellType() +", DATA  :: --> " + data + ", INDICE :: --> " + indice);

						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.CODIGO_AEROLINEA.getId()).getName())) 
							airlineCode = cell.getColumnIndex();

						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.NO_BOLETO.getId()).getName())) 
							voucherNumber = cell.getColumnIndex();

						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.IATA_AGENCIA.getId()).getName())) 
							iata = cell.getColumnIndex();

						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.TARIFA_0.getId()).getName())) 
							tarifa0 = cell.getColumnIndex();

						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.IVA_0.getId()).getName())) 
							iva0 = cell.getColumnIndex();

						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.TARIFA_16.getId()).getName())) 
							tarifa16 = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.IVA_16.getId()).getName())) 
							iva16 = cell.getColumnIndex();

						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.TUA.getId()).getName())) 
							tua = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.OTROS_IMP.getId()).getName())) 
							otrosImp = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.TOTAL.getId()).getName())) 
							total = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.MONEDA.getId()).getName()))
							currencyType = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.MPAGO.getId()).getName()))
							paymentType = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.RFC_DEP.getId()).getName()))
							rfcDep = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.PAX.getId()).getName()))
							pax = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.RUTA.getId()).getName())) 
							ruta = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.TIPODOCTO.getId()).getName())) 
							tipDocto = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.OBSERVACIONES.getId()).getName())) 
							observation = cell.getColumnIndex();
						
						if (data.equals(validationsMap.get(ValidationEspejoFieldEnum.CORREO_ELECTRONICO.getId()).getName())) 
							email = cell.getColumnIndex();

					} catch (Exception e) {
						LOGGER.error(" ERROR OBTENER DATA CABECERA:: --> {} ", ExceptionUtils.getStackTrace(e));
					}
				}
			} else {
				espejoFileDto = new EspejoFileDto();
				espejoFileDto.setStatus(ESTADO_PENDIENTE);
				espejoFileDto.setRequestFileId(requestFileDto.getId());
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					try {
						Cell cell = cellIterator.next();
						String data = (String) Utils.getValueFromCell(cell);
//						LOGGER.info("TIPO :: --> "+ cell.getCellType() +", DATA  :: --> " + data + ", INDICE :: --> " + cell.getColumnIndex());

						if (cell.getColumnIndex() == airlineCode) 
							espejoFileDto.setAirlineCode(Utils.getValue(data));

						if (cell.getColumnIndex() == voucherNumber) 
							espejoFileDto.setVoucherNumber(Utils.getValue(data));

						if (cell.getColumnIndex() == iata) 
							espejoFileDto.setIata(Utils.getValue(data));

						if (cell.getColumnIndex() == tarifa0) 
							espejoFileDto.setTarifa0(Utils.round(Utils.getValueDoubleCell(cell), 2));

						if (cell.getColumnIndex() == iva0) 
							espejoFileDto.setIva0(Utils.round(Utils.getValueDoubleCell(cell), 2));

						if (cell.getColumnIndex() == tarifa16) 
							espejoFileDto.setTarifa16(Utils.round(Utils.getValueDoubleCell(cell), 2));

						if (cell.getColumnIndex() == iva16) 
							espejoFileDto.setIva16(Utils.round(Utils.getValueDoubleCell(cell), 2));
						
						if (cell.getColumnIndex() == tua) 
							espejoFileDto.setTua(Utils.round(Utils.getValueDoubleCell(cell), 2));
						
						if (cell.getColumnIndex() == otrosImp) 
							espejoFileDto.setOtrosImp(Utils.round(Utils.getValueDoubleCell(cell), 2));
						
						if (cell.getColumnIndex() == total) 
							espejoFileDto.setTotal(Utils.round(Utils.getValueDoubleCell(cell), 2));
						
						if (cell.getColumnIndex() == currencyType) 
							espejoFileDto.setCurrencyType(Utils.getValue(data));
					
						if (cell.getColumnIndex() == paymentType) 
							espejoFileDto.setPaymentType(Utils.getValue(data));
	
						if (cell.getColumnIndex() == rfcDep) 
							espejoFileDto.setRfcDep(Utils.getValue(data));
						
						if (cell.getColumnIndex() == pax) 
							espejoFileDto.setPax(Utils.getValue(data));
						
						if (cell.getColumnIndex() == ruta) 
							espejoFileDto.setRuta(Utils.getValue(data));
						
						if (cell.getColumnIndex() == tipDocto) 
							espejoFileDto.setTipDocto(Utils.getValue(data));
						
						if (cell.getColumnIndex() == observation) 
							espejoFileDto.setObservaciones(Utils.getValue(data));
						
						if (cell.getColumnIndex() == email) 
							espejoFileDto.setEmail(Utils.getValue(data));
						
						
					} catch (Exception e) {
						LOGGER.error(" ERROR OBTENER DATA :: --> {}", ExceptionUtils.getStackTrace(e));
					}
				}

				if (espejoFileDto.getAirlineCode() != null && !espejoFileDto.getAirlineCode().isEmpty()) {
					espejoFileDtos.add(espejoFileDto);
					origNumberTickets++;
				}
			}
		}
		
		processMasiveFileDao.saveEspejoFile(espejoFileDtos);
		requestFileDto.setOrigNumberTickets(origNumberTickets);
		requestFileDto.setCalculatedNumberTickets(origNumberTickets);
		requestFileDao.updateRequestFile(requestFileDto);
	}

}
