package com.miatech.service.service;

import com.miatech.service.dto.FileRs;
import com.miatech.service.exception.BaseException;

public interface GenerateMasiveFileService {

	FileRs generateFile(String requestFileId) throws BaseException;

}
