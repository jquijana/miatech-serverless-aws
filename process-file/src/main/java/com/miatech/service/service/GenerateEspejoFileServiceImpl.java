package com.miatech.service.service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.RequestFileDaoImpl;
import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.dto.FileRs;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.exception.BaseException;

public class GenerateEspejoFileServiceImpl implements GenerateEspejoFileService {

	private RequestFileDaoImpl requestFileDao;
	private static final Logger LOGGER = LogManager.getLogger(GenerateEspejoFileServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	public GenerateEspejoFileServiceImpl() throws BaseException {
		this.requestFileDao = new RequestFileDaoImpl();
	}

	@Override
	public FileRs generateFile(String requestFileId) throws BaseException {
		LOGGER.info(" generateFile() --> requestFileId: {} ", requestFileId);
		FileRs fileRs = null;
		try {
			RequestFileDto requestFileDto = requestFileDao.getDataGenerateEspejoFile(requestFileId);
			List<EspejoFileDto> espejoFileDtos = requestFileDto.getLstEspejoFileDto();
			XSSFWorkbook wb = new XSSFWorkbook();
			generarArchivo(espejoFileDtos, wb);

			String fileName = requestFileDto.getFilePathWorking().substring(requestFileDto.getFilePathWorking().lastIndexOf("/") + 1);
			File file = File.createTempFile("tmp", fileName);
//				File file = new File("D:\\" + "EPEZIA@MIATECH.COM[20190118115200]cargamasiva-45.xlsx");
			FileOutputStream out = new FileOutputStream(file);
			wb.write(out);
			out.close();
			fileRs = new FileRs(fileName, file, requestFileDto);
		} catch (Exception e) {
			throw new BaseException(e);
		}

		return fileRs;
	}
	
	private void generarArchivo(List<EspejoFileDto> espejoFileDtos, XSSFWorkbook wb) {
		LOGGER.info(" generarArchivo() --> {} ", GSON.toJson(espejoFileDtos));

		XSSFSheet sheet = wb.createSheet("Espejo");

		String[] titulos = { "CODIGO_AEROLINEA", "NO_BOLETO", "IATA_AGENCIA", "TARIFA_0%", "IVA_0%", "TARIFA_16%", "IVA_16%","TUA", "OTROS_IMP"
				,"TOTAL", "MONEDA", "MPAGO", "RFC_DEP", "PAX", "RUTA", "TIPODOCTO", "OBSERVACIONES", "CORREO_ELECTRONICO" 
				,"ESTADO", "UUID", "VALIDACIONES", "PATHXML", "PATHPDF", "SERIE", "TRANSACTION" };
		Row fila = sheet.createRow(0);
		for (int i = 0; i < titulos.length; i++) {
			Cell celda = fila.createCell(i);
			celda.setCellValue(titulos[i]);
		}

		if (espejoFileDtos != null) {
			int n = 1;
			for (int i = 0; i < espejoFileDtos.size(); i++) {
				EspejoFileDto data = espejoFileDtos.get(i);
				Row filax = sheet.createRow(n);
				String[] datos = { data.getAirlineCode(), data.getVoucherNumber(), data.getIata(),
						String.valueOf(data.getTarifa0()), String.valueOf(data.getIva0()), String.valueOf(data.getTarifa16()), 
						String.valueOf(data.getIva16()), String.valueOf(data.getTua()), String.valueOf(data.getOtrosImp()), 
						String.valueOf(data.getTotal()), data.getCurrencyType(), 
						data.getPaymentType(), data.getRfcDep(), data.getPax(), data.getRuta(), data.getTipDocto(),
						data.getObservaciones(), data.getEmail(), data.getStatus(),
						data.getUuid(), data.getObservation(), data.getPathxml(), data.getPathpdf(), data.getSerie(), data.getTransaction()};
				for (int j = 0; j < datos.length; j++) {
					Cell celda = filax.createCell(j);
					celda.setCellValue((datos[j] == null) ? "" : datos[j]);
				}
				n++;
			}
		}
	}
 }