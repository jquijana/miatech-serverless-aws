package com.miatech.service.service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.RequestFileDaoImpl;
import com.miatech.service.dto.FileRs;
import com.miatech.service.dto.IneDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.exception.BaseException;

public class GenerateMasiveFileServiceImpl implements GenerateMasiveFileService {

	private RequestFileDaoImpl requestFileDao;
	private static final Logger LOGGER = LogManager.getLogger(GenerateMasiveFileServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	public GenerateMasiveFileServiceImpl() throws BaseException {
		this.requestFileDao = new RequestFileDaoImpl();
	}

	@Override
	public FileRs generateFile(String requestFileId) throws BaseException {
		LOGGER.info(" generateFile() --> requestFileId: {} ", requestFileId);
		FileRs fileRs = null;
		try {
			RequestFileDto requestFileDto = requestFileDao.getDataGenerateMasiveFile(requestFileId);
			List<IneDto> ineDtos = requestFileDao.getDataIne(requestFileId);

			List<MasiveFileDto> masiveFileDtos = requestFileDto.getLstMasiveFileDto();
			XSSFWorkbook wb = new XSSFWorkbook();
			generarFacturas(masiveFileDtos, wb);
			generarIne(ineDtos, wb);

			String fileName = requestFileDto.getFilePathWorking().substring(requestFileDto.getFilePathWorking().lastIndexOf("/") + 1);
			File file = File.createTempFile("tmp", fileName);
//				File file = new File("D:\\" + "EPEZIA@MIATECH.COM[20190118115200]cargamasiva-45.xlsx");
			FileOutputStream out = new FileOutputStream(file);
			wb.write(out);
			out.close();
			fileRs = new FileRs(fileName, file, requestFileDto);
		} catch (Exception e) {
			throw new BaseException(e);
		}

		return fileRs;
	}
	
	private void generarFacturas(List<MasiveFileDto> masiveFileDtos, XSSFWorkbook wb) {
		LOGGER.info(" generarFacturas() --> {} ", GSON.toJson(masiveFileDtos));

		XSSFSheet sheet = wb.createSheet("Facturas");

		String[] titulos = { "NO ETICKET /PNR", "RFC", "NOMBRE /RAZÓN SOCIAL", "CÓDIGO POSTAL", "REFERENCIA", "NOMBRE ARCHIVO", "CORREO ELECTRÓNICO", "ESTADO", "UUID", "OBSERVACIONES", "PATHXML", "PATHPDF", "SERIE", "TRANSACTION" };
		Row fila = sheet.createRow(0);
		for (int i = 0; i < titulos.length; i++) {
			Cell celda = fila.createCell(i);
			celda.setCellValue(titulos[i]);
		}

		if (masiveFileDtos != null) {
			int n = 1;
			for (int i = 0; i < masiveFileDtos.size(); i++) {
				MasiveFileDto data = masiveFileDtos.get(i);
				Row filax = sheet.createRow(n);
				String[] datos = { data.getTicketNumber(), data.getRfc(), data.getCompanyName(), data.getZipCode(),
						data.getReference(), data.getFileName(), data.getEmail(), data.getStatus(),
						data.getUuid(), data.getObservation(), data.getPathxml(), data.getPathpdf(), data.getSerie(), data.getTransaction()};
				for (int j = 0; j < datos.length; j++) {
					Cell celda = filax.createCell(j);
					celda.setCellValue((datos[j] == null) ? "" : datos[j]);
				}
				n++;
			}
		}
	}
	
	private void generarIne(List<IneDto> ineDtos, XSSFWorkbook wb) {
		LOGGER.info(" generarIne() -->  {} ", GSON.toJson(ineDtos));
		XSSFSheet sheet = wb.createSheet("INE");

		String[] titulos = { "ETICKET", "TIPO DOCTO", "TIPO PROCESO", "TIPO COMITÉ", "TIPO AMBITO", "CLAVE ENTIDAD", "ID CONTABILIDAD", "OBSERVACIONES"};
		Row fila = sheet.createRow(0);
		for (int i = 0; i < titulos.length; i++) {
			Cell celda = fila.createCell(i);
			celda.setCellValue(titulos[i]);
		}

		if (ineDtos != null) {
			int n = 1;
			for (int i = 0; i < ineDtos.size(); i++) {
				IneDto data = ineDtos.get(i);
				Row filax = sheet.createRow(n);
				String[] datos = { data.getTicketNumber(), data.getDocumentType(), data.getProcessType(), data.getComiteType(),
						data.getAmbitoType(), data.getEntity(), data.getContabilidadId(), data.getObservation()};
				for (int j = 0; j < datos.length; j++) {
					Cell celda = filax.createCell(j);
					celda.setCellValue((datos[j] == null) ? "" : datos[j]);
				}
				n++;
			}
		}
	}
}