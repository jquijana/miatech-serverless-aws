package com.miatech.service.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.RequestFileDao;
import com.miatech.service.dao.RequestFileDaoImpl;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.dto.client.MailFileRq;
import com.miatech.service.dto.client.ZipFileRq;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Utils;

public class SendMailServiceImpl implements SendMailService {

	private RequestFileDao requestFileDao;
	private RestTemplate restTemplate;

	private static final Logger LOGGER = LogManager.getLogger(SendMailServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	public SendMailServiceImpl(RestTemplate restTemplate) throws BaseException {
		this.requestFileDao = new RequestFileDaoImpl();
		this.restTemplate = restTemplate;
	}

	@Override
	public void sendMailMasivo(String requestFileId) throws BaseException {
		LOGGER.info("sendMailMasivo() --> requestFileId {}", requestFileId);
		Map<String, ZipFileRq> requestZipMap = new HashedMap<>();
		RequestFileDto requestFileDto = requestFileDao.getDataSendMailMasive(requestFileId);
		if (requestFileDto != null && requestFileDto.getLstMasiveFileDto() != null) {
			requestFileDto.getLstMasiveFileDto().forEach(data -> {
				generateZipMap(data.getEmail(), data.getPathxml(), data.getPathpdf(), requestZipMap);
			});
		}
		sendMails(requestZipMap);
	}

	@Override
	public void sendMailEspejo(String requestFileId) throws BaseException {
		LOGGER.info("sendMailEspejo() --> requestFileId {}", requestFileId);
		Map<String, ZipFileRq> requestZipMap = new HashedMap<>();
		RequestFileDto requestFileDto = requestFileDao.getDataSendMailEspejo(requestFileId);
		if (requestFileDto != null && requestFileDto.getLstEspejoFileDto() != null) {
			requestFileDto.getLstEspejoFileDto().forEach(data -> {
				generateZipMap(data.getEmail(), data.getPathxml(), data.getPathpdf(), requestZipMap);
			});
		}
		sendMails(requestZipMap);
	}
	
	private void generateZipMap(String email, String pathXml, String pathPdf, Map<String, ZipFileRq> requestZipMap){
		if (email != null && !email.trim().isEmpty()) {
			email = email.trim();
			if (!requestZipMap.containsKey(email)) {
				String requestJson = joinXmlPdf(pathXml, pathPdf);
				ZipFileRq oZipFileRq = new ZipFileRq(email, requestJson);
				requestZipMap.put(email, oZipFileRq);
			} else {
				ZipFileRq oZipFileRq = requestZipMap.get(email);
				oZipFileRq.setJsonZip(oZipFileRq.getJsonZip() + "," + joinXmlPdf(pathXml, pathPdf));
				requestZipMap.put(email, oZipFileRq);
			}
		}
	}
	
	private void sendMails(Map<String, ZipFileRq> requestZipMap) {
		List<ZipFileRq> lstZipFileRq = new ArrayList<ZipFileRq>(requestZipMap.values());
		if (lstZipFileRq != null) {
			lstZipFileRq.forEach(data -> {
				String requestZipMail = "[" + data.getJsonZip() + "]";
				try {
					String pathZip = callGenerateZipFile(GSON.toJson(requestZipMail));
					String pathZip2 = pathZip.substring(1, pathZip.length() - 1);
					callSendMail(data.getMail(), pathZip2);
				} catch (BaseException e) {
					LOGGER.error("ERROR PROCESO ENVIO", ExceptionUtils.getStackTrace(e));
				}
			});
		}
	}
	
	private String joinXmlPdf(String pathxml, String pathPdf) {
		return "\"" + pathxml + "\"" + "," + "\"" + pathPdf + "\"";
	}

	private HttpHeaders generateHeaderRequest() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);
		return headers;
	}

	private String callGenerateZipFile(String requestJson) throws BaseException {
		LOGGER.info("Call Zip File :: -> request  : {} ", requestJson);
		try {
			HttpEntity<String> request = new HttpEntity<String>(requestJson, generateHeaderRequest());
			String responseEntity = restTemplate.postForObject(Constants.Enviroments.URL_ZIP_FILE, request, String.class);
			LOGGER.info("Call Zip File :: -> response  : " + GSON.toJson(responseEntity));

			return responseEntity;
		} catch (Exception e) {
			LOGGER.error("ERROR callGenerateFile  :: ");
			throw new BaseException(e);
		}
	}
	
	private String callSendMail(String mail, String pathZip) throws BaseException {
		LOGGER.info("Call Send Mail :: -> request  :: mail : {}, pathZip : {} ", mail, pathZip);
		try {
			MailFileRq mailFileRq = new MailFileRq();
			mailFileRq.setMail(mail);
			mailFileRq.setPathS3Adjunto(pathZip);
			mailFileRq.setCuerpo("");
			mailFileRq.setAsunto("PROCESO MASIVO");
			
			String requestSendMail = Utils.parseObjectToJsonString(mailFileRq);
			HttpEntity<String> request = new HttpEntity<String>(requestSendMail, generateHeaderRequest());
			String responseEntity = restTemplate.postForObject(Constants.Enviroments.URL_SEND_MAIL, request, String.class);
			LOGGER.info("Call Send Mail :: -> response  : " + GSON.toJson(responseEntity));
			return responseEntity;
		} catch (Exception e) {
			LOGGER.error("ERROR callGenerateFile  :: ");
			throw new BaseException(e);
		}
	}
}
