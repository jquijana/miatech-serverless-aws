package com.miatech.service.handler;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.GsonBuilder;
import com.miatech.service.dto.ValidationFileRq;
import com.miatech.service.dto.expose.ResultMessageRs;
import com.miatech.service.dto.expose.ServerlessRequest;
import com.miatech.service.dto.expose.ServerlessResponse;
import com.miatech.service.exception.BusinessException;
import com.miatech.service.exception.ExceptionUtil;
import com.miatech.service.service.ValidationFileService;
import com.miatech.service.service.ValidationFileServiceImpl;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Utils;
import com.miatech.service.util.enums.MessageEnum;

public class ValidationFileHandler implements RequestHandler<ServerlessRequest, ServerlessResponse> {

	private static final String urlService = "/api/v1/validation";
	private int invocations = 0;

	private static final Logger LOGGER = LogManager.getLogger(ValidationFileHandler.class);

	@Override
	public ServerlessResponse handleRequest(ServerlessRequest serverlessRequest, Context context) {
		invocations++;
		LOGGER.info("InputRequest : " + invocations + " - " + serverlessRequest.toString());
		ResultMessageRs resultMessage = new ResultMessageRs(MessageEnum.MESSAGE_GENERAL_SUCCESS);

		try {
			ValidationFileService studentService = new ValidationFileServiceImpl();

			switch (serverlessRequest.getResource()) {
			case urlService:
				switch (serverlessRequest.getHttpMethod()) {
				case Constants.Web.HTTP_POST:
					ValidationFileRq validationFileRq = new GsonBuilder().create()
							.fromJson(serverlessRequest.getBody(), ValidationFileRq.class);
					resultMessage.setData(studentService.validationFile(validationFileRq));
					break;
				}

			default:
				break;
			}
		} catch (BusinessException businessException) {
			resultMessage = ExceptionUtil.handleBusinessException(businessException);
		} catch (Exception exception) {
			resultMessage = ExceptionUtil.handleException(exception);
		}

		ServerlessResponse resp = Utils.parseServerlessResponse(resultMessage);
		Map<String, String> pathParameters = new HashMap<>();
		pathParameters.put("Access-Control-Allow-Origin", "*");
		pathParameters.put("Access-Control-Allow-Headers", "Content-Type");
		serverlessRequest.setPathParameters(pathParameters);
		resp.setHeaders(pathParameters);
		return resp;
	}

}
