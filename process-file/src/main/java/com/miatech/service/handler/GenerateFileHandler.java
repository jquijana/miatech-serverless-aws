package com.miatech.service.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.RestemplateConfig;
import com.miatech.service.dto.FileRs;
import com.miatech.service.dto.client.GenerateFileRq;
import com.miatech.service.dto.expose.ResultMessageRs;
import com.miatech.service.dto.expose.ServerlessRequest;
import com.miatech.service.dto.expose.ServerlessResponse;
import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;
import com.miatech.service.exception.ExceptionUtil;
import com.miatech.service.service.GenerateEspejoFileService;
import com.miatech.service.service.GenerateEspejoFileServiceImpl;
import com.miatech.service.service.GenerateMasiveFileService;
import com.miatech.service.service.GenerateMasiveFileServiceImpl;
import com.miatech.service.service.ProcessFileService;
import com.miatech.service.service.ProcessFileServiceImpl;
import com.miatech.service.service.SendMailService;
import com.miatech.service.service.SendMailServiceImpl;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Constants.Enviroments;
import com.miatech.service.util.Utils;
import com.miatech.service.util.enums.MessageEnum;

public class GenerateFileHandler implements RequestHandler<ServerlessRequest, ServerlessResponse> {

	private RestTemplate restTemplate;
	private static final String URL_GENERATE_FILE = "/v1/generate/file";
	private static final Logger LOGGER = LogManager.getLogger(GenerateFileHandler.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	public GenerateFileHandler() throws BaseException {
		restTemplate = RestemplateConfig.getRestTemplate();
	}

	@Override
	public ServerlessResponse handleRequest(ServerlessRequest serverlessRequest, Context context) {
		LOGGER.info("ServerlessRequest : " + GSON.toJson(serverlessRequest));
		ResultMessageRs resultMessage = new ResultMessageRs(MessageEnum.MESSAGE_GENERAL_SUCCESS);

		try {
			switch (serverlessRequest.getResource()) {
			case URL_GENERATE_FILE:
				
				ProcessFileService processFileService = new ProcessFileServiceImpl();
				SendMailService sendMailServiceImpl = new SendMailServiceImpl(this.restTemplate);
				
				switch (serverlessRequest.getHttpMethod()) {
				case Constants.Web.HTTP_POST:
					String data = serverlessRequest.getBody();
					GenerateFileRq request = GSON.fromJson(data, GenerateFileRq.class);

					if (request.getFileType().equals(Constants.FILE_TYPE_MASIVO)) {
						GenerateMasiveFileService generateMasiveFileService = new GenerateMasiveFileServiceImpl();
						FileRs fileRs = generateMasiveFileService.generateFile(request.getRequestFileId());
						if (fileRs != null && fileRs.getRequestFileDto() != null) {
							AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
							LOGGER.info("WRITE FILE TO : " + fileRs.getFileName());
							s3Client.putObject(Enviroments.BUCKET_RESULT_MASIVO, fileRs.getFileName(), fileRs.getFile());
							fileRs.getRequestFileDto().setFilePathResult(Enviroments.BUCKET_RESULT_MASIVO+"/"+fileRs.getFileName());
							processFileService.updateRequestFile(fileRs.getRequestFileDto());
							sendMailServiceImpl.sendMailMasivo(fileRs.getRequestFileDto().getId().toString());
						}
					}else if(request.getFileType().equals(Constants.FILE_TYPE_ESPEJO) ) {
						GenerateEspejoFileService generateEspejoFileService = new GenerateEspejoFileServiceImpl();
						FileRs fileRs = generateEspejoFileService.generateFile(request.getRequestFileId());
						if (fileRs != null && fileRs.getRequestFileDto() != null) {
							AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
							LOGGER.info("WRITE FILE TO : " + fileRs.getFileName());
							s3Client.putObject(Enviroments.BUCKET_RESULT_ESPEJO, fileRs.getFileName(), fileRs.getFile());
							fileRs.getRequestFileDto().setFilePathResult(Enviroments.BUCKET_RESULT_ESPEJO+"/"+fileRs.getFileName());
							processFileService.updateRequestFile(fileRs.getRequestFileDto());
							sendMailServiceImpl.sendMailEspejo(fileRs.getRequestFileDto().getId().toString());
						}
					}

					break;
				}
				break;
			}

		} catch (BusinessException businessException) {
			resultMessage = ExceptionUtil.handleBusinessException(businessException);
		} catch (Exception exception) {
			resultMessage = ExceptionUtil.handleException(exception);
		}

		ServerlessResponse response = Utils.parseServerlessResponse(resultMessage);
		return response;
	}

}
