package com.miatech.service.handler;

import java.io.InputStream;
import java.net.URLDecoder;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.service.ProcessFileService;
import com.miatech.service.service.ProcessFileServiceImpl;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Utils;

public class RequestFileHandler implements RequestHandler<S3Event, String> {

	private static final Logger LOGGER = LogManager.getLogger(RequestFileHandler.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();
	private static final String RESPONSE_OK = "OK";
	private static final String RESPONSE_ERROR = "ERROR";
	private static final Integer STATUS_RECIBIDO = 0;
	private static final String FILE_TYPE_M1 = "M1";
	private static final String FILE_TYPE_E1 = "E1";

	@Override
	public String handleRequest(S3Event s3event, Context context) {
		LOGGER.info("S3 EVENT : " + s3event.toJson());
		LOGGER.info("CONTEXT : " + GSON.toJson(context));

		try {
			ProcessFileService processFileService = new ProcessFileServiceImpl();
			S3EventNotificationRecord record = s3event.getRecords().get(0);
			String srcOriginBucketName = record.getS3().getBucket().getName();

			String fileName = URLDecoder.decode(record.getS3().getObject().getKey().replace('+', ' '), "UTF-8");
			LOGGER.info("INPUT FILE :: " + fileName);

			// Registrar File in DB
			if (record.getS3().getObject().getSizeAsLong() > 0) {
				String usuario = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.indexOf("["));
				RequestFileDto requestFileDto = new RequestFileDto();
				requestFileDto.setUser(usuario);
				requestFileDto.setFileType(Constants.Enviroments.FILE_TYPE);
				requestFileDto.setFilePathOriginal(srcOriginBucketName + "/" + fileName);
				requestFileDto.setCreatedBy(usuario);
				requestFileDto.setUpdatedBy(usuario);
				requestFileDto.setStatus(STATUS_RECIBIDO);
				RequestFileDto requestFileDtoResponse = processFileService.saveRequestFile(requestFileDto);

				if (requestFileDtoResponse.getId() != null) {
					// Grabar en Buket Destino
					AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
					S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcOriginBucketName, fileName));
					InputStream inputStreamFile = s3Object.getObjectContent();
					InputStream inputStreamCloned = Utils.clone(inputStreamFile);

					String fileNameDestino = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.lastIndexOf(".")) + "-"
							+ requestFileDtoResponse.getId() + fileName.substring(fileName.lastIndexOf("."));

					LOGGER.info("----  SAVE DATA FILE ---- ");
					if (requestFileDto.getFileType().equals(FILE_TYPE_M1)) {
						String bucketFileNameDestino = Constants.Enviroments.BUCKET_DESTINATION_MASIVO + "/" + fileNameDestino;
						LOGGER.info("WRITE INPUT FILE TO : " + bucketFileNameDestino);
						ObjectMetadata meta = new ObjectMetadata();
						meta.setContentType(s3Object.getObjectMetadata().getContentType());
						s3Client.putObject(Constants.Enviroments.BUCKET_DESTINATION_MASIVO, fileNameDestino, inputStreamFile, null);
						processFileService.saveMasiveFile(bucketFileNameDestino, inputStreamCloned);
					}else if(requestFileDto.getFileType().equals(FILE_TYPE_E1)){
						String bucketFileNameDestino = Constants.Enviroments.BUCKET_DESTINATION_ESPEJO + "/" + fileNameDestino;
						LOGGER.info("WRITE INPUT FILE TO : " + bucketFileNameDestino);
						ObjectMetadata meta = new ObjectMetadata();
						meta.setContentType(s3Object.getObjectMetadata().getContentType());
						s3Client.putObject(Constants.Enviroments.BUCKET_DESTINATION_ESPEJO, fileNameDestino, inputStreamFile, null);
						processFileService.saveEspejoFile(bucketFileNameDestino, inputStreamCloned);
					}
					
					return RESPONSE_OK;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERROR AL PROCESAR FILE : {}", ExceptionUtils.getStackTrace(e));
		}

		return RESPONSE_ERROR;
	}

}
