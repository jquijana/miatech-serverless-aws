package com.miatech.service.dto.expose;

public class ErrorGenericRs {

	private Integer responseCode;
	private String responseMessage;

	public ErrorGenericRs() {
		super();
	}

	public ErrorGenericRs(Integer responseCode, String responseMessage) {
		super();
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

}
