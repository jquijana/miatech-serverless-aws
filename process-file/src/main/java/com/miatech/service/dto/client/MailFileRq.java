package com.miatech.service.dto.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MailFileRq {

	private String mail;
	private String asunto;
	private String cuerpo;
	private String pathS3Adjunto;
}
