package com.miatech.service.dto;

import java.io.File;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileRs {

	private String fileName;
	private File file;
	private RequestFileDto requestFileDto;
}
