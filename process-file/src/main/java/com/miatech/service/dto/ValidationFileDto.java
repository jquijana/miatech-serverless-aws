package com.miatech.service.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidationFileDto {

	private String codeFile;
	private Long id;
	private String name;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy; 
		
}
