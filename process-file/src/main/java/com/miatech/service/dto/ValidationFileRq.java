package com.miatech.service.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidationFileRq {

	private String validationType;
	private List<ValidationRq> fields;

		
}
