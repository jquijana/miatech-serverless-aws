package com.miatech.service.dto;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestFileDto {

	private Long id;
	private String user;
	private String fileType;
	private String filePathOriginal;
	private String filePathWorking;
	private String filePathResult;
	private Date requestDateTime;
	private Integer origNumberTickets;
	private Integer calculatedNumberTickets;
	private Integer numberTicketsProcesed;
	private Integer status;
	private Date lastUpdate;
	private Date lastVerified;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;

	private List<MasiveFileDto> lstMasiveFileDto;
	private List<EspejoFileDto> lstEspejoFileDto;

}
