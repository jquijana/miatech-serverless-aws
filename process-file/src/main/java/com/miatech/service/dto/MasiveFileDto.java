package com.miatech.service.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MasiveFileDto {

	private Long id;
	private Long requestFileId;
	private String ticketNumber;
	private String ticketType;
	private String rfc;
	private String companyName;
	private String zipCode;
	private String fileName;
	private String Reference;
	private String Email;
	private String Status;
	private Date lastUpdate;
	private Date lastVerified;
	private String observation;
	private String uuid;
	private String pathxml;
	private String pathpdf;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String serie;
	private String transaction;
}
