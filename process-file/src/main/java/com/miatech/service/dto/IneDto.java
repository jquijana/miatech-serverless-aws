package com.miatech.service.dto;

import java.time.Instant;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IneDto {

	private Long id;
	private Long requestFileId;
	private String ticketNumber;
	private String documentType;
	private String processType;
	private String comiteType;
	private String ambitoType;
	private String entity;
	private String contabilidadId;
	private String observation;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;

	public IneDto() {
		this.createdOn = Date.from(Instant.now());
		this.updatedOn = Date.from(Instant.now());
	}

}
