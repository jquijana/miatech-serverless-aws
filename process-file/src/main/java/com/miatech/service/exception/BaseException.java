package com.miatech.service.exception;

import com.miatech.service.util.enums.MessageEnum;

public class BaseException extends Exception {

	private static final long serialVersionUID = 1L;

	private Integer errorCode;
	private String errorMessage;
	private MessageEnum messageEnum;

	public BaseException() {
	}

	public BaseException(Exception exception) {
		this.errorCode = 0;
		this.errorMessage = exception.getMessage();
	}

	public BaseException(Integer code, String message) {
		super();
		this.errorCode = code;
		this.errorMessage = message;
	}

	public BaseException(MessageEnum messageEnum) {
		super();
		this.errorCode = messageEnum.getIdMessage();
		this.errorMessage = messageEnum.getMessage();
		this.messageEnum = messageEnum;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public MessageEnum getMessageEnum() {
		return messageEnum;
	}

	public void setMessageEnum(MessageEnum messageEnum) {
		this.messageEnum = messageEnum;
	}

}
