package com.miatech.service.config;

import java.nio.charset.Charset;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class RestemplateConfig {

	private RestemplateConfig() {
	}

	private static final Integer CONNECTION_TIMEOUT = 30000;
	private static final Integer READ_TIMEOUT = 30000;

	public static RestTemplate getRestTemplate() {
		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		httpRequestFactory.setConnectTimeout(Integer.valueOf(CONNECTION_TIMEOUT));
		httpRequestFactory.setReadTimeout(Integer.valueOf(READ_TIMEOUT));
		RestTemplate restTemplate = new RestTemplate(httpRequestFactory);
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		return restTemplate;
	}

	public static RestTemplate getRestTemplate(Integer connectionTimeout, Integer readTimeout) {
		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		httpRequestFactory.setConnectTimeout(connectionTimeout);
		httpRequestFactory.setReadTimeout(readTimeout);
		RestTemplate restTemplate = new RestTemplate(httpRequestFactory);
		return restTemplate;
	}

}
