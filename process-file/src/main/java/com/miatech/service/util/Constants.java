package com.miatech.service.util;

public class Constants {

	public static final String STR_AT = "@";
	public static final String STR_TAB = "\t";
	public static final String STR_BREAK = "\n";
	public static final String STR_EMPTY = "";
	public static final String STR_ASTERISK = "*";
	public static final String STR_LIST_CONTAINS_NO_DATA = "List contains no data";
	public static final String STR_COLON = ":";
	public static final String STR_COMMA = ",";
	public static final String STR_WHITE_SPACE = " ";
	public static final String STR_TRUE = "true";
	public static final String STR_FALSE = "false";
	public static final String DATEFORMAT = "dd/MM/yyyy";
	public static final String DATEFORMAT2 = "dd.MM.yyyy";
	public static final String DATEFORMAT3 = "yyyy-MM-dd";
	public static final String DATEFORMAT4 = "ddMMyyyy";
	public static final String DATEFORMAT5 = "dd/MM/yyyy hh:mm:ss";
	public static final String DATEFORMAT7 = "yyyy-MM-dd hh:mm:ss";
	public static final String DATEFORMAT6 = "yyyy-MM-dd";
	public static final String DATEFORMAT8 = "dd-MM-yyyy";
	public static final String DOUBLEFORMAT = "#,###.##";
	public static final String STR_PERIOD_PREFIX = "MMMYYYY";
	public static final String STR_REGEX_ONLYDIGITS = new String("[0-9]+");
	public static final String STR_GUION = "-";
	public static final String STR_UNDERLINE = "_";
	public static final String STR_EXCLAMATION = "!";
	public static final String STR_COMMILLAS = "\"";
	public static final String STR_SLASH = "/";
	public static final String STR_POINT = ".";
	public static final String STR_HEADER = "H";
	public static final String STR_DOT_DELIMATOR = "@";
	public static final String STR_HEADER_XML_FILE = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
	public static final Integer INT_NUMBER_ZERO = 0;
	public static final Integer INT_NUMBER_ONE = 1;
	public static final Integer INT_NUMBER_TWO = 2;
	public static final String FILE_TYPE_MASIVO = "M1";
	public static final String FILE_TYPE_ESPEJO = "E1";

	public static class Parameter {

		private Parameter() {
			super();
		}

		public static final String CODE_FILE = "codeFile";
		public static final String REQUEST_FILE_ID = "requestFileId";
	}

	public static class Query {

		private Query() {
			super();
		}

	 
		public static final String GET_PROCESS_MASIVE_FILE_BY_REQUEST_ID = new StringBuilder()
				.append("  SELECT  	AUTOID AS AUTO_ID, USER AS USER, FILETYPE AS FILE_TYPE, FILEPATHORIGINAL AS FILE_PATH_ORIGINAL, 				 ")
				.append("  		 	FILEPATHWORKING AS FILE_PATH_WORKING, FILEPATHRESULT AS FILE_PATH_RESULT, REQUESTDATETIME AS REQUEST_DATE, 		 ")
				.append("  		 	ORIGNUMBERTICKETS AS ORIGIN_NUMBER_TICKETS, CALCULATEDNUMBERTICKETS AS CALCULATED_NUMBER_TICKETS, NUMBERTICKETSPROCESED AS NUMBER_TICKETS_PROCESED,   ")
				.append("  		 	STATUS AS STATUS, LASTUPDATE AS LAST_UPDATE, LASTVERIFIED AS LAST_VERIFIED,  ")
				.append("  		 	CREATEDBY AS CREATED_BY, CREATEDON AS CREATED_ON, UPDATEDBY AS UPDATED_BY, UPDATEDON AS UPDATED_ON ")
				.append("  FROM REQUEST_FILES ")
				.append("  WHERE AUTOID = ? ")
				.toString();

		public static final String GET_DATA_INE_BY_REQUEST_ID = new StringBuilder()
				.append("  SELECT 	ID AS INE_ID, REQUESTFILEID AS REQUEST_FILE_ID, TICKETNUMBER AS TICKET_NUMBER, DOCUMENTTYPE AS DOCUMENT_TYPE, ")
				.append("  		PROCESSTYPE AS PROCESS_TYPE, COMITETYPE AS COMITE_TYPE, AMBITOTYPE AS AMBITO_TYPE, ENTITY AS ENTITY,              ")
				.append("  		CONTABILIDADID AS CONTABILIDAD_ID, OBSERVATION AS INE_OBSERVATION                                                 ")
				.append("  FROM REQUEST_FILE_INE                                                               						  			  ")
				.append("  WHERE REQUESTFILEID = ?																					  ")
				.toString();

		public static final String GET_DATA_GENERATE_MASIVE_FILE = new StringBuilder()
				.append("   SELECT 	PMF.AUTOID AS AUTO_ID, PMF.REQUESTFILEID AS REQUEST_FILE_ID, PMF.TICKETNUMBER AS TICKET_NUMBER, PMF.RFC AS RFC, PMF.COMPANYNAME AS COMPANY_NAME, PMF.ZIPCODE AS ZIP_CODE, PMF.FILENAME AS FILE_NAME,  ")
				.append("			PMF.TYPETICKET AS TYPE_TICKET, PMF.REFERENCE AS REFERENCE, PMF.EMAIL AS EMAIL, PMF.STATUS AS STATUS, PMF.LASTUPDATE AS LAST_UPDATE, PMF.LASTVERIFIED AS LAST_VERIFIED, ")
				.append("		    PMF.UUID AS UUID, PMF.OBSERVATION AS OBSERVATION, PMF.PATHXML AS PATH_XML, PMF.PATHPDF AS PATH_PDF, ")
				.append("		    PMF.CREATEDBY AS CREATED_BY, PMF.CREATEDON AS CREATED_ON, PMF.UPDATEDBY AS UPDATED_BY, PMF.UPDATEDON AS UPDATED_ON, ")
				.append("  			RF.USER AS USER, RF.REQUESTDATETIME AS REQUEST_DATE, RF.NUMBERTICKETSPROCESED AS NUMBER_TICKETS_PROCESED, ")
				.append("  			RF.CALCULATEDNUMBERTICKETS AS CALCULATED_NUMBER_TICKETS_PROCESED, RF.STATUS AS REQUEST_FILE_STATUS, ")
				.append("  			RF.ORIGNUMBERTICKETS AS ORIGIN_NUMBER_TICKETS, RF.FILEPATHWORKING AS FILE_PATH_WORKING , PMF.SERIE , PMF.TRANSACTION ")
				.append("  	FROM 	PROCESS_MASIVE_FILE PMF ")
				.append("  	INNER JOIN REQUEST_FILES RF ON RF.AUTOID = PMF.REQUESTFILEID ")
				.append("  	WHERE 	RF.AUTOID = ? ")
				.toString();
		
		public static final String GET_DATA_GENERATE_ESPEJO_FILE = new StringBuilder()
				.append("  SELECT  PEF.AUTOID AS AUTO_ID, PEF.REQUESTFILEID AS REQUEST_FILE_ID, 																					")
				.append("  		  PEF.AIRLINECODE AS AIR_LINE_CODE, PEF.VOUCHERNUMBER AS VOUCHER_NUMBER, PEF.IATA AS IATA,                                                          ")
				.append("  		  PEF.TARIFA_0 AS TARIFA_0, PEF.IVA_0 AS IVA_0, PEF.TARIFA_16 AS TARIFA_16, PEF.IVA_16 AS IVA_16,                                                   ")
				.append("  		  PEF.TUA AS TUA, PEF.OTROS_IMP AS OTROS_IMP, PEF.TOTAL AS TOTAL,                                                                                   ")
				.append("  		  PEF.CURRENCYTYPE AS CURRENCY_TYPE, PEF.PAYMENTTYPE AS PAYMENT_TYPE, PEF.RFCDEP AS RFC_DEP,                                                        ")
				.append("  		  PEF.PAX AS PAX, PEF.RUTA AS RUTA, PEF.TIPDOCTO AS TIP_DOCTO,                                                                                      ")
				.append("		  PEF.EMAIL AS EMAIL, PEF.STATUS AS STATUS, PEF.LASTUPDATE AS LAST_UPDATE, PEF.LASTVERIFIED AS LAST_VERIFIED,                                       ")
				.append("		  PEF.UUID AS UUID, PEF.OBSERVATION AS OBSERVATION, PEF.PATHXML AS PATH_XML, PEF.PATHPDF AS PATH_PDF,                                               ")                                                        
				.append("		  PEF.CREATEDBY AS CREATED_BY, PEF.CREATEDON AS CREATED_ON, PEF.UPDATEDBY AS UPDATED_BY, PEF.UPDATEDON AS UPDATED_ON,                               ")                                                        		  
				.append("		  RF.USER AS USER, RF.REQUESTDATETIME AS REQUEST_DATE, RF.NUMBERTICKETSPROCESED AS NUMBER_TICKETS_PROCESED,                                         ")                                                        
				.append("		  RF.CALCULATEDNUMBERTICKETS AS CALCULATED_NUMBER_TICKETS_PROCESED, RF.STATUS AS REQUEST_FILE_STATUS,  FILEPATHRESULT AS FILE_PATH_RESULT,          ")                                                                                           
				.append("		  RF.ORIGNUMBERTICKETS AS ORIGIN_NUMBER_TICKETS,	RF.FILETYPE AS FILE_TYPE,  RF.FILEPATHORIGINAL AS FILE_PATH_ORIGINAL,			  			    ")                                                                                                                              
				.append("		  RF.CALCULATEDNUMBERTICKETS AS CALCULATED_NUMBER_TICKETS, RF.FILEPATHWORKING AS FILE_PATH_WORKING                                                  ")                                                                               
				.append("  FROM 	PROCESS_ESPEJO_FILE PEF                                                                                                                         ")                                                      
				.append("  INNER JOIN REQUEST_FILES RF ON RF.AUTOID = PEF.REQUESTFILEID                                                                                             ")
				.append("  WHERE   PEF.REQUESTFILEID = ? ")
				.toString();
		
		public static final String GET_DATA_SEND_MAIL_MASIVO = new StringBuilder()
				.append("   SELECT 	PMF.AUTOID AS AUTO_ID, PMF.REQUESTFILEID AS REQUEST_FILE_ID, PMF.TICKETNUMBER AS TICKET_NUMBER, PMF.RFC AS RFC, PMF.COMPANYNAME AS COMPANY_NAME, PMF.ZIPCODE AS ZIP_CODE, PMF.FILENAME AS FILE_NAME,  ")
				.append("			PMF.TYPETICKET AS TYPE_TICKET, PMF.REFERENCE AS REFERENCE, PMF.EMAIL AS EMAIL, PMF.STATUS AS STATUS, PMF.LASTUPDATE AS LAST_UPDATE, PMF.LASTVERIFIED AS LAST_VERIFIED, ")
				.append("		    PMF.UUID AS UUID, PMF.OBSERVATION AS OBSERVATION, PMF.PATHXML AS PATH_XML, PMF.PATHPDF AS PATH_PDF, ")
				.append("		    PMF.CREATEDBY AS CREATED_BY, PMF.CREATEDON AS CREATED_ON, PMF.UPDATEDBY AS UPDATED_BY, PMF.UPDATEDON AS UPDATED_ON, ")
				.append("  			RF.USER AS USER, RF.REQUESTDATETIME AS REQUEST_DATE, RF.NUMBERTICKETSPROCESED AS NUMBER_TICKETS_PROCESED, ")
				.append("  			RF.CALCULATEDNUMBERTICKETS AS CALCULATED_NUMBER_TICKETS_PROCESED, RF.STATUS AS REQUEST_FILE_STATUS, ")
				.append("  			RF.ORIGNUMBERTICKETS AS ORIGIN_NUMBER_TICKETS, RF.FILEPATHWORKING AS FILE_PATH_WORKING , PMF.SERIE , PMF.TRANSACTION  ")
				.append("  	FROM 	PROCESS_MASIVE_FILE PMF ")
				.append("  	INNER JOIN REQUEST_FILES RF ON RF.AUTOID = PMF.REQUESTFILEID ")
				.append("  	WHERE 	RF.AUTOID = ? AND PMF.STATUS = 'TIMBRADO' ")
				.toString();
		
		public static final String GET_DATA_SEND_MAIL_ESPEJO = new StringBuilder()
				.append("  SELECT  PEF.AUTOID AS AUTO_ID, PEF.REQUESTFILEID AS REQUEST_FILE_ID, 																					")
				.append("  		  PEF.AIRLINECODE AS AIR_LINE_CODE, PEF.VOUCHERNUMBER AS VOUCHER_NUMBER, PEF.IATA AS IATA,                                                          ")
				.append("  		  PEF.TARIFA_0 AS TARIFA_0, PEF.IVA_0 AS IVA_0, PEF.TARIFA_16 AS TARIFA_16, PEF.IVA_16 AS IVA_16,                                                   ")
				.append("  		  PEF.TUA AS TUA, PEF.OTROS_IMP AS OTROS_IMP, PEF.TOTAL AS TOTAL,                                                                                   ")
				.append("  		  PEF.CURRENCYTYPE AS CURRENCY_TYPE, PEF.PAYMENTTYPE AS PAYMENT_TYPE, PEF.RFCDEP AS RFC_DEP,                                                        ")
				.append("  		  PEF.PAX AS PAX, PEF.RUTA AS RUTA, PEF.TIPDOCTO AS TIP_DOCTO,                                                                                      ")
				.append("		  PEF.EMAIL AS EMAIL, PEF.STATUS AS STATUS, PEF.LASTUPDATE AS LAST_UPDATE, PEF.LASTVERIFIED AS LAST_VERIFIED,                                       ")
				.append("		  PEF.UUID AS UUID, PEF.OBSERVATION AS OBSERVATION, PEF.PATHXML AS PATH_XML, PEF.PATHPDF AS PATH_PDF,                                               ")                                                        
				.append("		  PEF.CREATEDBY AS CREATED_BY, PEF.CREATEDON AS CREATED_ON, PEF.UPDATEDBY AS UPDATED_BY, PEF.UPDATEDON AS UPDATED_ON,                               ")                                                        		  
				.append("		  RF.USER AS USER, RF.REQUESTDATETIME AS REQUEST_DATE, RF.NUMBERTICKETSPROCESED AS NUMBER_TICKETS_PROCESED,                                         ")                                                        
				.append("		  RF.CALCULATEDNUMBERTICKETS AS CALCULATED_NUMBER_TICKETS_PROCESED, RF.STATUS AS REQUEST_FILE_STATUS,  FILEPATHRESULT AS FILE_PATH_RESULT,          ")                                                                                           
				.append("		  RF.ORIGNUMBERTICKETS AS ORIGIN_NUMBER_TICKETS,	RF.FILETYPE AS FILE_TYPE,  RF.FILEPATHORIGINAL AS FILE_PATH_ORIGINAL,			  			    ")                                                                                                                              
				.append("		  RF.CALCULATEDNUMBERTICKETS AS CALCULATED_NUMBER_TICKETS, RF.FILEPATHWORKING AS FILE_PATH_WORKING                                                  ")                                                                               
				.append("  FROM 	PROCESS_ESPEJO_FILE PEF                                                                                                                         ")                                                      
				.append("  INNER JOIN REQUEST_FILES RF ON RF.AUTOID = PEF.REQUESTFILEID                                                                                             ")
				.append("  WHERE   RF.AUTOID = ? AND PEF.STATUS = 'TIMBRADO'  ")
				.toString();
		
		public static final String GET_VALIDATIONS_BY_ID = new StringBuilder()
				.append("   SELECT 	CODEFILE AS CODE_FILE, ID AS VALIDATION_ID, NAME AS NAME, ")
				.append("  			CREATEDBY AS CREATED_BY, CREATEDON AS CREATED_ON, UPDATEDBY AS UPDATED_BY, UPDATEDON AS UPDATED_ON ")
				.append("  	FROM 	VALIDATION_FILE  WHERE CODEFILE = ? ")
				.toString();	
	
	}

	public static class QueryAlias {

		private QueryAlias() {
			super();
		}

		public static final String AUTO_ID = "AUTO_ID";
		public static final String REQUEST_FILE_ID = "REQUEST_FILE_ID";
		public static final String USER = "USER";
		public static final String FILE_TYPE = "FILE_TYPE";
		public static final String FILE_PATH_ORIGINAL = "FILE_PATH_ORIGINAL";
		public static final String FILE_PATH_WORKING = "FILE_PATH_WORKING";
		public static final String SERIE = "SERIE";
		public static final String TRANSACTION = "TRANSACTION";
		public static final String FILE_PATH_RESULT = "FILE_PATH_RESULT";
		public static final String REQUEST_DATE = "REQUEST_DATE";
		public static final String ORIGIN_NUMBER_TICKETS = "ORIGIN_NUMBER_TICKETS";
		public static final String CALCULATED_NUMBER_TICKETS = "CALCULATED_NUMBER_TICKETS";
		public static final String REQUEST_FILE_STATUS = "REQUEST_FILE_STATUS";
		public static final String NUMBER_TICKETS_PROCESED = "NUMBER_TICKETS_PROCESED";
		public static final String STATUS = "STATUS";
		public static final String LAST_UPDATE = "LAST_UPDATE";
		public static final String LAST_VERIFIED = "LAST_VERIFIED";
		public static final String TYPE_TICKET = "TYPE_TICKET";
		public static final String TICKET_NUMBER = "TICKET_NUMBER";
		public static final String RFC = "RFC";
		public static final String COMPANY_NAME = "COMPANY_NAME";
		public static final String ZIP_CODE = "ZIP_CODE";
		public static final String FILE_NAME = "FILE_NAME";
		public static final String REFERENCE = "REFERENCE";
		public static final String EMAIL = "EMAIL";
		public static final String UUID = "UUID";
		public static final String OBSERVATION = "OBSERVATION";
		public static final String PATH_XML = "PATH_XML";
		public static final String PATH_PDF = "PATH_PDF";

		public static final String CREATED_BY = "CREATED_BY";
		public static final String CREATED_ON = "CREATED_ON";
		public static final String UPDATED_BY = "UPDATED_BY";
		public static final String UPDATED_ON = "UPDATED_ON";

		public static final String INE_ID = "INE_ID";
		public static final String DOCUMENT_TYPE = "DOCUMENT_TYPE";
		public static final String PROCESS_TYPE = "PROCESS_TYPE";
		public static final String COMITE_TYPE = "COMITE_TYPE";
		public static final String AMBITO_TYPE = "AMBITO_TYPE";
		public static final String ENTITY = "ENTITY";
		public static final String CONTABILIDAD_ID = "CONTABILIDAD_ID";
		public static final String INE_OBSERVATION = "INE_OBSERVATION";
		public static final String FILE_NAME_XLS = "FILE_NAME_XLS";
		public static final String CALCULATED_NUMBER_TICKETS_PROCESED = "CALCULATED_NUMBER_TICKETS_PROCESED";
		public static final String VALIDATION_ID = "VALIDATION_ID";
		public static final String CODE_FILE = "CODE_FILE";
		public static final String NAME = "NAME";
		
		public static final String AIR_LINE_CODE = "AIR_LINE_CODE";
		public static final String VOUCHER_NUMBER = "VOUCHER_NUMBER";
		public static final String IATA = "IATA";
		public static final String TARIFA_0 = "TARIFA_0";
		
		public static final String IVA_0 = "IVA_0";
		public static final String TARIFA_16 = "TARIFA_16";
		public static final String IVA_16 = "IVA_16";
		public static final String TUA = "TUA";
		public static final String OTROS_IMP = "OTROS_IMP";
		public static final String TOTAL = "TOTAL";
		
		public static final String CURRENCY_TYPE = "CURRENCY_TYPE";
		public static final String PAYMENT_TYPE = "PAYMENT_TYPE";
		public static final String RFC_DEP = "RFC_DEP";
		public static final String PAX = "PAX";
		public static final String RUTA = "RUTA";
		public static final String TIP_DOCTO = "TIP_DOCTO";
	}

	public static class Web {
		private Web() {
			super();
		}

		public static final String HTTP_GET = "GET";
		public static final String HTTP_POST = "POST";
		public static final String HTTP_DELETE = "DELETE";

	}

	public static class Enviroments {
		private Enviroments() {
			super();
		}

		public static final String DB_IP = System.getenv("DB_IP");
		public static final String DB_PORT = System.getenv("DB_PORT");
		public static final String DB_NAME = System.getenv("DB_NAME");
		public static final String DB_USSER = System.getenv("DB_USSER");
		public static final String DB_PASSWORD = System.getenv("DB_PASSWORD");
		public static final String BUCKET_DESTINATION_MASIVO = System.getenv("BUCKET_DESTINATION_MASIVO");
		public static final String BUCKET_DESTINATION_ESPEJO = System.getenv("BUCKET_DESTINATION_ESPEJO");
		public static final String BUCKET_RESULT_MASIVO = System.getenv("BUCKET_RESULT_MASIVO");
		public static final String BUCKET_RESULT_ESPEJO = System.getenv("BUCKET_RESULT_ESPEJO");
		public static final String URL_ZIP_FILE= System.getenv("URL_ZIP_FILE");
		public static final String URL_SEND_MAIL= System.getenv("URL_SEND_MAIL");
		public static final String FILE_TYPE= System.getenv("FILE_TYPE");

//		public static final String DB_IP = "miatechprod.cyl003eugmf2.us-east-1.rds.amazonaws.com";
//		public static final String DB_PORT = "3306";
//		public static final String DB_NAME = "MIATECH";
//		public static final String DB_USSER = "root";
//		public static final String DB_PASSWORD = "Peru2018";
//		public static final String BUCKET_DESTINATION = "miatech-work-file";
//		public static final String URL_ZIP_FILE= "https://5xdr3w9m7c.execute-api.us-east-1.amazonaws.com/mi/la-fnc-s3-zipfilepath";
//		public static final String URL_SEND_MAIL= "https://5xdr3w9m7c.execute-api.us-east-1.amazonaws.com/mi/la-fnc-ses-sendmail";
//		public static final String FILE_TYPE = "M1";

	}
}
