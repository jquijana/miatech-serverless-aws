package com.miatech.service.util.enums;

public enum ValidationEspejoFieldEnum {
	CODIGO_AEROLINEA("1"),
	NO_BOLETO("2"),
	IATA_AGENCIA("3"),
	TARIFA_0("4"),
	IVA_0("5"),
	TARIFA_16("6"),
	IVA_16("7"),
	TUA("8"),
	OTROS_IMP("9"),
	TOTAL("10"),
	MONEDA("11"),
	MPAGO("12"),
	RFC_DEP("13"),
	PAX("14"),
	RUTA("15"),
	TIPODOCTO("16"),
	OBSERVACIONES("17"),
	CORREO_ELECTRONICO("18");
	
	private String id;

	private ValidationEspejoFieldEnum(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
