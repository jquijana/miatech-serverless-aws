package com.miatech.service.util.enums;

public enum ValidationMasiveFieldEnum {

	// FACTURAS
	NO_ETICKET_PNR("1"),
	RFC("2"),
	NOMBRE_RAZON_SOCIAL("3"),
	CODIGO_POSTAL("4"),
	REFERENCIA("5"),
	NOMBRE_ARCHIVO("6"),
	CORREO_ELECTRONICO("7"),
	
	//COMPLEMENTO INE
	ETICKET("1"),
	TIPO_DOCTO("2"),
	TIPO_PROCESO("3"),
	CLAVE_ENTIDAD("4"),
	ID_CONTABILIDAD("5"),
	TIPO_COMITE("6"),
	TIPO_AMBITO("7");

	private String id;

	private ValidationMasiveFieldEnum(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
