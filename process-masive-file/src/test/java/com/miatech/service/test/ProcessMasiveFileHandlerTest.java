package com.miatech.service.test;

import java.io.IOException;

import com.miatech.service.dto.expose.ServerlessRequest;
import com.miatech.service.exception.BaseException;
import com.miatech.service.handler.ProcessMasiveFileHandler;

public class ProcessMasiveFileHandlerTest {

	public static void main(String[] args) throws BaseException, IOException {
		ServerlessRequest serverlessRequest = new ServerlessRequest();
		System.out.println(new ProcessMasiveFileHandler().handleRequest(serverlessRequest, null));
	}
}