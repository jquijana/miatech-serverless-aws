package com.miatech.service.test;

import java.io.IOException;

import com.miatech.service.dto.expose.ServerlessRequest;
import com.miatech.service.exception.BaseException;
import com.miatech.service.handler.ProcessValidationFileHandler;

public class ProcessValidationFileHandlerTest {
	private static final String URL_REQUEST_VALIDATION = "/v1/request/validation/file";

	public static void main(String[] args) throws BaseException, IOException {

		ServerlessRequest serverlessRequest = new ServerlessRequest();
		serverlessRequest.setHttpMethod("POST");
		serverlessRequest.setResource(URL_REQUEST_VALIDATION);
		serverlessRequest.setBody("{'requestFileId':'15', 'fileType': 'M1' }");
		System.out.println(new ProcessValidationFileHandler().handleRequest(serverlessRequest, null));
	}
}