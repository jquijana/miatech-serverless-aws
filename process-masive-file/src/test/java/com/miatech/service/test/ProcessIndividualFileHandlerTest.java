package com.miatech.service.test;

import java.io.IOException;

import com.miatech.service.dto.expose.ServerlessRequest;
import com.miatech.service.exception.BaseException;
import com.miatech.service.handler.ProcessIndividualFileHandler;

public class ProcessIndividualFileHandlerTest {

	private static final String URL_TIMBRADO_INDIVIDUAL = "/v1/timbrado/individual";

	public static void main(String[] args) throws BaseException, IOException {

		ServerlessRequest serverlessRequest = new ServerlessRequest();
		serverlessRequest.setResource(URL_TIMBRADO_INDIVIDUAL);
		serverlessRequest.setHttpMethod("POST");
		serverlessRequest.setBody("{'dataFileId':'1', 'fileType': 'M1' }");
		System.out.println(new ProcessIndividualFileHandler().handleRequest(serverlessRequest, null));

	}
}