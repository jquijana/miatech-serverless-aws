package com.miatech.service.config;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;

public class DBConection {

	private static final Logger LOGGER = LogManager.getLogger(DBConection.class);

	public Connection getConnection() throws BaseException {
		String url = "jdbc:mysql://" + Constants.Enviroments.DB_IP + ":" 
									 + Constants.Enviroments.DB_PORT + "/"
									 + Constants.Enviroments.DB_NAME + "?useSSL=false";
		try {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException ex) {
				LOGGER.error(" Error al registrar el driver de MySQL  --> {} ", ExceptionUtils.getStackTrace(ex));
			}
			return DriverManager.getConnection(url, Constants.Enviroments.DB_USSER, Constants.Enviroments.DB_PASSWORD);
		} catch (Exception e) {
			LOGGER.error(" ERROR getDataSource() --> {} ",  ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		}
	}

}
