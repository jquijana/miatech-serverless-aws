package com.miatech.service.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.RestemplateConfig;
import com.miatech.service.dto.client.ProcessIndividualRq;
import com.miatech.service.dto.expose.ResultMessageRs;
import com.miatech.service.dto.expose.ServerlessRequest;
import com.miatech.service.dto.expose.ServerlessResponse;
import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;
import com.miatech.service.exception.ExceptionUtil;
import com.miatech.service.service.TimbradoEspejoFileService;
import com.miatech.service.service.TimbradoEspejoFileServiceImpl;
import com.miatech.service.service.TimbradoMasivoFileService;
import com.miatech.service.service.TimbradoMasivoFileServiceImpl;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Utils;
import com.miatech.service.util.enums.MessageEnum;

public class ProcessIndividualFileHandler implements RequestHandler<ServerlessRequest, ServerlessResponse> {

	private RestTemplate restTemplate;

	private static final String URL_TIMBRADO_INDIVIDUAL = "/v1/timbrado/individual";
	private static final Logger LOGGER = LogManager.getLogger(ProcessIndividualFileHandler.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	public ProcessIndividualFileHandler() throws BaseException {
		restTemplate = RestemplateConfig.getRestTemplate();
	}

	@Override
	public ServerlessResponse handleRequest(ServerlessRequest serverlessRequest, Context context) {
		LOGGER.info("ServerlessRequest : " + GSON.toJson(serverlessRequest));
		ResultMessageRs resultMessage = new ResultMessageRs(MessageEnum.MESSAGE_GENERAL_SUCCESS);

		try {
			switch (serverlessRequest.getResource()) {
			case URL_TIMBRADO_INDIVIDUAL:
				

				switch (serverlessRequest.getHttpMethod()) {
				case Constants.Web.HTTP_POST:
					String data = serverlessRequest.getBody();
					ProcessIndividualRq request = GSON.fromJson(data, ProcessIndividualRq.class);
					if (request.getFileType().equals(Constants.FILE_TYPE_MASIVO)) {
						TimbradoMasivoFileService processFileService = new TimbradoMasivoFileServiceImpl(this.restTemplate);
						processFileService.timbrado(request.getDataFileId());
					}else if(request.getFileType().equals(Constants.FILE_TYPE_ESPEJO)) {
						TimbradoEspejoFileService processFileService = new TimbradoEspejoFileServiceImpl(this.restTemplate);
						processFileService.timbrado(request.getDataFileId());
					}
					break;
				}
				break;
			}
		} catch (BusinessException businessException) {
			resultMessage = ExceptionUtil.handleBusinessException(businessException);
		} catch (Exception exception) {
			resultMessage = ExceptionUtil.handleException(exception);
		}

		ServerlessResponse response = Utils.parseServerlessResponse(resultMessage);
		return response;
	}

}
