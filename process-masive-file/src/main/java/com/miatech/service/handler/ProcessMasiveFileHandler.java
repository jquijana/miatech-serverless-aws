package com.miatech.service.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.RestemplateConfig;
import com.miatech.service.dto.expose.ResultMessageRs;
import com.miatech.service.dto.expose.ServerlessRequest;
import com.miatech.service.dto.expose.ServerlessResponse;
import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;
import com.miatech.service.exception.ExceptionUtil;
import com.miatech.service.service.ProcessFileService;
import com.miatech.service.service.ProcessFileServiceImpl;
import com.miatech.service.util.Utils;
import com.miatech.service.util.enums.MessageEnum;

public class ProcessMasiveFileHandler implements RequestHandler<ServerlessRequest, ServerlessResponse> {

	private RestTemplate restTemplate;

	private static final Logger LOGGER = LogManager.getLogger(ProcessMasiveFileHandler.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	public ProcessMasiveFileHandler() throws BaseException {
		restTemplate = RestemplateConfig.getRestTemplate();
	}

	@Override
	public ServerlessResponse handleRequest(ServerlessRequest serverlessRequest, Context context) {
		LOGGER.info("ServerlessRequest : " + GSON.toJson(serverlessRequest));
		ResultMessageRs resultMessage = new ResultMessageRs(MessageEnum.MESSAGE_GENERAL_SUCCESS);

		try {
			ProcessFileService processFileService = new ProcessFileServiceImpl(this.restTemplate);
			processFileService.processMasive();
		} catch (BusinessException businessException) {
			resultMessage = ExceptionUtil.handleBusinessException(businessException);
		} catch (Exception exception) {
			resultMessage = ExceptionUtil.handleException(exception);
		}

		ServerlessResponse response = Utils.parseServerlessResponse(resultMessage);
		return response;
	}

}
