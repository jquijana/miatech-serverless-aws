package com.miatech.service.util.enums;

public enum MessageEnum {

	MESSAGE_GENERAL_SUCCESS(1, "message.general.success"),
	MESSAGE_ERROR_GENERIC(-1, "Error Generic"),
	ERROR_BUCKET_NOT_EXIST(2, "Error Bucket No Existe"),
	MESSAGE_DATA_EMPTY_TO_ORDER_DETAIL(2, "No se encontró detalle del pedido ingresado"),
	MESSAGE_DATA_EMPTY_TO_SERVICE(2, "El pedido no tiene servicios asociados");

	private Integer idMessage;
	private String message;

	private MessageEnum(Integer idMessage, String message) {
		this.idMessage = idMessage;
		this.message = message;
	}

	public Integer getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(Integer idMessage) {
		this.idMessage = idMessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
