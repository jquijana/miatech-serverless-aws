package com.miatech.service.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.miatech.service.dto.client.TimbradoEspejoRs;
import com.miatech.service.dto.client.TimbradoMasivoRs;
import com.miatech.service.dto.expose.ResultMessageRs;
import com.miatech.service.dto.expose.ServerlessResponse;
import com.miatech.service.util.enums.HttpStatusEnum;

public class Utils {

	/**
	 * @return
	 * @throws Exception
	 */
	/**
	 * @return
	 */
	private static final Logger LOGGER = LogManager.getLogger(Utils.class);

	public static <T> List<T> getPage(List<T> sourceList, int page, int pageSize) {
		if (pageSize <= 0 || page <= 0) {
			throw new IllegalArgumentException("invalid page size: " + pageSize);
		}

		int fromIndex = (page - 1) * pageSize;
		if (sourceList == null || sourceList.size() < fromIndex) {
			return Collections.emptyList();
		}

		// toIndex exclusive
		return sourceList.subList(fromIndex, Math.min(fromIndex + pageSize, sourceList.size()));
	}

	public static Date getCurrentDate() {
		return new Date();
	}

	/**
	 * @param year
	 * @param month
	 * @return
	 */
	public static Date getFirstDayOfMonth(Integer year, Integer month) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, cal.getActualMinimum(Calendar.DAY_OF_MONTH), cal.getMinimum(Calendar.HOUR_OF_DAY),
				cal.getMinimum(Calendar.MINUTE), cal.getMinimum(Calendar.SECOND));
		return cal.getTime();
	}

	/**
	 * @param year
	 * @param month
	 * @return
	 */
	public static Date getLastDayOfMonth(Integer year, Integer month) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, cal.getActualMaximum(Calendar.DAY_OF_MONTH), cal.getMaximum(Calendar.HOUR_OF_DAY),
				cal.getMaximum(Calendar.MINUTE), cal.getMaximum(Calendar.SECOND));
		return cal.getTime();
	}

	public static Date addHoursToDate(Date date, int hours) {
		if (date != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.HOUR, hours);
			return calendar.getTime();
		}
		return date;
	}

	public static Date convertStringToDate(String dateSinFormat) {
		LOGGER.info("dateSinFormat : " + dateSinFormat);
		if (dateSinFormat != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATEFORMAT7);
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date date = null;
			try {
				date = formatter.parse(dateSinFormat);
				LOGGER.info("dateConfFormat : " + date.toString());
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return date;
		}
		return null;
	}

	public static Date getCurrentWeekMonday() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

		return cal.getTime();
	}

	public static String toString(Object objData) {
		if (objData == null) {
			return Constants.STR_EMPTY;
		} else {
			return Constants.STR_EMPTY.concat(objData.toString()).trim();
		}
	}

	public static Double toDouble(Object objData) {
		if (objData == null) {
			return null;
		} else {
			try {
				return Double.parseDouble(objData.toString());
			} catch (Exception e) {
				return null;
			}
		}
	}

	public static Long toLong(Object objData) {
		if (objData == null) {
			return null;
		} else {
			try {
				return Long.parseLong(objData.toString());
			} catch (Exception e) {
				return null;
			}
		}
	}

	public static String subString(String strData, int idxStart, int idxEnd) {
		if (strData != null && strData.length() > 0) {
			if (idxEnd < strData.length() && idxStart < idxEnd) {
				return strData.substring(idxStart, idxEnd);
			} else if (idxStart < idxEnd) {
				return strData.substring(idxStart, strData.length());
			}
		}
		return Constants.STR_EMPTY;
	}

	public static Date getCurrentDateWithoutTime() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date getDate(String strDateAsString, String strFormat) {

		Date objDate = null;

		try {

			SimpleDateFormat objFormatter = new SimpleDateFormat(strFormat);
			objDate = objFormatter.parse(strDateAsString);

		} catch (ParseException e) {
			objDate = null;
		}

		return objDate;

	}

	/**
	 * Returns the value after removing commas
	 *
	 * @param strValue <code>String</code>
	 * @return <code>String</code>
	 */
	public static String removeCommas(String strValue) {
		String strResult = Constants.STR_EMPTY;
		if (strValue != null && strValue.trim().length() > 0) {
			strResult = strValue.replaceAll(Constants.STR_COMMA, Constants.STR_EMPTY);
		}
		return strResult;
	}

	public static boolean isNumber(String strNumberAsString, int inIntegers, int inDecimals) {
		if (inDecimals == 0) {
			return isInteger(strNumberAsString, inIntegers);
		} else {
			return isDouble(strNumberAsString, inIntegers, inDecimals);
		}
	}

	public static boolean isDouble(String strNumberAsString, int inIntegers, int inDecimals) {

		try {
			double dblNumber = Double.parseDouble(strNumberAsString);

			if (dblNumber == 0) {
				return true;
			}

			double inDigits = Math.floor(Math.log10(Math.abs(dblNumber)) + 1);
			if ((long) Math.abs(dblNumber) == 0) {
				inDigits = 1;
			}
			String text = Double.toString(Math.abs(dblNumber));
			int inFloats = (int) (text.length() - inDigits - 1);

			if (inDecimals == 0) {
				// No interesa la cantidad de decimales
				if (inIntegers == 0) {
					// No interesa la cantidad de enteros
					return true;

				} else {
					// Si interesa la cantidad de enteros
					if (inDigits <= inIntegers) {
						return true;
					} else {
						return false;
					}
				}
			} else {
				// Si intersa la cantidad de decimales
				if (inIntegers == 0) {
					// No interesa la cantidad de enteros
					if (inFloats <= inDecimals) {
						return true;
					} else {
						return false;
					}
				} else {
					// Si interesa la cantidad de enteros
					if ((inDigits <= inIntegers) && (inFloats <= inDecimals)) {
						return true;
					} else {
						return false;
					}
				}
			}
		} catch (NumberFormatException objException) {
			return false;
		} catch (Exception objException) {
			return false;
		}
	}

	public static boolean isInteger(String strNumberAsString, int inIntegers) {

		boolean isInteger = false;

		try {

			double dblNumber = Math.abs(Double.parseDouble(strNumberAsString));
			double dblDiference = dblNumber - (long) dblNumber;
			if (dblDiference > 0) {
				return false;
			} else {
				strNumberAsString = String.valueOf((long) dblNumber);
			}

			if (!strNumberAsString.matches(Constants.STR_REGEX_ONLYDIGITS)) {
				isInteger = false;

			} else {
				if (inIntegers > 0) {
					if (strNumberAsString.length() <= inIntegers) {
						isInteger = true;
					} else {
						isInteger = false;
					}

				} else {
					isInteger = true;
				}
			}

		} catch (NumberFormatException objException) {
			isInteger = false;
		} catch (Exception objException) {
			isInteger = false;
		}

		return isInteger;
	}

	public static Date addToDate(Date dtOrigin, int inField, int inToAdd) {
		if (dtOrigin != null) {
			GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();

			gc.setTime(dtOrigin);
			gc.add(inField, inToAdd);

			return gc.getTime();
		}
		return null;
	}

	/**
	 * convertDatetoString Converts the given date to String format is "DD/MM/YYYY"
	 *
	 * @param dtDateVal <code> Date </code>
	 * @return strTempDate <code> String</code>
	 */
	public static String convertDatetoString(Date dtDateVal) {
		String strTempDate = Constants.STR_EMPTY;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATEFORMAT);
			if (dtDateVal != null)
				strTempDate = sdf.format(dtDateVal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strTempDate;
	}

	/**
	 * overloaded method which takes the specific format and converts the Date in
	 * String
	 *
	 * @param dtDate    <code>Date</code>
	 * @param strFormat <code>String</code>
	 * @return strNewDate <code>String</code>
	 */
	public static String convertDatetoString(Date dtDate, String strFormat) {
		String strNewDate = Constants.STR_EMPTY;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
			if (dtDate != null)
				strNewDate = sdf.format(dtDate);
		} catch (Exception e) {
		}
		return strNewDate;
	}

	public static int toInt(Object obj) {
		int returnValue = 0;
		if (obj != null) {
			try {
				returnValue = Integer.parseInt(obj.toString());
			} catch (Exception e) {
				return returnValue;
			}
		}
		return returnValue;
	}

	public static String getStringIntegerValue(Integer lngLongVal) {
		if (lngLongVal != null)
			return lngLongVal.toString();
		return Constants.STR_EMPTY;
	}

	public static long getIntegerValue(Integer lngVal) {
		if (lngVal != null)
			return lngVal.longValue();
		return 0;
	}

	public static Integer getPeriodId(Integer inYear, Integer inMonth) {
		return Integer.parseInt(inYear.toString() + inMonth.toString());
	}

	public static boolean isDate(String strDateAsString, String strFormat) {

		boolean isDate = false;

		try {

			SimpleDateFormat objFormatter = new SimpleDateFormat(strFormat);
			objFormatter.parse(strDateAsString);
			isDate = true;

		} catch (ParseException e) {
			isDate = false;
		}

		return isDate;

	}

	public static List<Long> getLongListFromString(String strLine) {
		List<String> strList = Arrays.asList(strLine.split(","));

		List<Long> lngList = new ArrayList<Long>();

		for (String myString : strList) {
			lngList.add(Long.parseLong(myString));
		}

		return lngList;
	}

	public static String toUpperCase(String strText) {
		if (strText == null) {
			strText = Constants.STR_EMPTY;
		}
		return Constants.STR_EMPTY.concat(strText).trim().toUpperCase();
	}

	public static String toLowerCase(String strText) {
		if (strText == null) {
			strText = Constants.STR_EMPTY;
		}
		return Constants.STR_EMPTY.concat(strText).trim().toLowerCase();
	}

	public static Date stringToDate(String dataAsString, String format) throws Exception {

		try {

			SimpleDateFormat formatter = new SimpleDateFormat(format);
			return formatter.parse(dataAsString);

		} catch (ParseException e) {
			throw e;
		}
	}

	public static String getBusinessExceptionMessage(java.util.List<String> listaMensajes) {
		String mensajes = "";
		if (listaMensajes != null && listaMensajes.size() > 0) {
			for (String mensaje : listaMensajes) {
				mensajes += ". " + mensaje;
			}
		}
		return mensajes;
	}

	public static String showTrace(StackTraceElement[] traze) {
		StringBuffer st = new StringBuffer();
		for (StackTraceElement item : traze) {
			st.append("\n").append(item.toString());
		}
		return st.toString();
	}

	public static ServerlessResponse parseServerlessResponse(ResultMessageRs resultMessage) {
		ServerlessResponse response = new ServerlessResponse();
		response.setStatusCode(HttpStatusEnum.OK.value());
		try {
			String body = new ObjectMapper().writeValueAsString(resultMessage);
			response.setBody(body);
		} catch (Exception e) {
			LOGGER.error(" converObjectToJson() --> {} ", e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public static TimbradoMasivoRs parseJsonStringToObject(String responseEntity) {
		String response = responseEntity.replace("\\", "");
		String responseJson = response.substring(1, response.length() - 1);
		TimbradoMasivoRs resultado = new Gson().fromJson(responseJson, TimbradoMasivoRs.class);
		return resultado;
	}
	
	public static TimbradoEspejoRs parseJsonStringToObject2(String responseEntity) {
		String response = responseEntity.replace("\\", "");
		String responseJson = response.substring(1, response.length() - 1);
		TimbradoEspejoRs resultado = new Gson().fromJson(responseJson, TimbradoEspejoRs.class);
		return resultado;
	}

	public static String parseObjectToJsonString(Object object) {
		return "\"" + (String) new Gson().toJson(object).toString().replaceAll("\"", "\\\\\"") + "\"";
	}

	public static String parseObjectToJsonString2(Object object) {
		return (String) new Gson().toJson(object).toString().replaceAll("\"", "\\\\\\\"");
	}

	public static Integer getInt(Integer data) {
		return data == null ? 0 : data;
	}

	public static Long getLong(Long data) {
		return data == null ? 0 : data;
	}
	
	public static java.sql.Date convertDateToDateSql(Date date) {
		return date != null ? new java.sql.Date(date.getTime()) : null;
	}
	
	public static String getExceptionMessageChain(Throwable throwable) {
	    List<String> result = new ArrayList<String>();
	    while (throwable != null) {
	        result.add(throwable.getMessage());
	        throwable = throwable.getCause();
	    }
	    return result.toString(); 
	}
	
	public static  HttpHeaders generateHeaderRequest() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}
	
	public static double sumDouble(double[] values, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	    BigDecimal rs = new BigDecimal(0);
	    for (int i = 0; i < values.length; i++) {
	    	BigDecimal bd = new BigDecimal(Double.toString(values[i]));
	    	rs = rs.add(bd.setScale(places, RoundingMode.HALF_UP));
		}
	    return rs.doubleValue();
	}
	
}
