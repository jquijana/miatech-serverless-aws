package com.miatech.service.exception;

import com.miatech.service.util.enums.MessageEnum;

public class BusinessException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BusinessException(Integer errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	public BusinessException(MessageEnum messageEnum) {
		super(messageEnum);
	}

}
