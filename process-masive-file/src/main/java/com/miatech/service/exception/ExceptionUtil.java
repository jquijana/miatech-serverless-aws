package com.miatech.service.exception;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.miatech.service.dto.expose.ResultMessageRs;
import com.miatech.service.util.Utils;
import com.miatech.service.util.enums.MessageEnum;

public class ExceptionUtil {

	private ExceptionUtil() {
	}

	private static final Logger LOGGER = LogManager.getLogger(ExceptionUtil.class);

	public static ResultMessageRs handleBusinessException(BusinessException businessException) {
		LOGGER.info(" handleBusinessException :  {} ", ExceptionUtils.getStackTrace(businessException));
		ResultMessageRs response = new ResultMessageRs(businessException.getMessageEnum());
		return response;
	}

	public static ResultMessageRs handleException(Exception exception) {
		LOGGER.info(" handleException :  {} ", ExceptionUtils.getStackTrace(exception));
		ResultMessageRs response = new ResultMessageRs(MessageEnum.MESSAGE_ERROR_GENERIC.getIdMessage(),
				Utils.showTrace(exception.getStackTrace()));
		return response;
	}
}
