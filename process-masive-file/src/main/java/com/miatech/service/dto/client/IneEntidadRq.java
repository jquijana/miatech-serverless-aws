package com.miatech.service.dto.client;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IneEntidadRq {

	@SerializedName("ClaveEntidad")
	private String claveEntidad;

	@SerializedName("Ambito")
	private String ambito;

	@SerializedName("Contabilidad")
	private IneContabilidadRq contabilidad;

}
