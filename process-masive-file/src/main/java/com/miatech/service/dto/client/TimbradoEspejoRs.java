package com.miatech.service.dto.client;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TimbradoEspejoRs {

	@JsonProperty("status")
	private String status;
	
	@JsonProperty("data")
	private List<TicketRs> data = null;

}
