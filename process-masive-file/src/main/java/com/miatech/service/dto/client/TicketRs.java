package com.miatech.service.dto.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TicketRs {

	@JsonProperty("tkt")
	private String tkt;
	
	@JsonProperty("serie")
	private String serie;

	@JsonProperty("trans")
	private String trans;
	
	@JsonProperty("pnr")
	private String pnr;

	@JsonProperty("estado")
	private String estado;

	@JsonProperty("uuid")
	private String uuid;

	@JsonProperty("urlxml")
	private String urlxml;

	@JsonProperty("urlpdf")
	private String urlpdf;

	@JsonProperty("pathxml")
	private String pathxml;

	@JsonProperty("pathpdf")
	private String pathpdf;
}
