package com.miatech.service.dto.client;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IneContabilidadRq {

	@SerializedName("idContabilidad")
	private String idContabilidad;

}
