package com.miatech.service.dto.expose;

import com.miatech.service.util.enums.MessageEnum;

public class ResultMessageRs {

	private Integer code;
	private String message;
	private Object data;

	public ResultMessageRs(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public ResultMessageRs(MessageEnum messageEnum) {
		this.code = messageEnum.getIdMessage();
		this.message = messageEnum.getMessage();
	}

	public ResultMessageRs() {
		super();
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
