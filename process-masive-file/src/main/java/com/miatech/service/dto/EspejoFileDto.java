package com.miatech.service.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EspejoFileDto {

	private Long id;
	private Long requestFileId;
	private String airlineCode;
	private String voucherNumber;
	private String iata;
	private Double tarifa0;
	private Double iva0;
	private Double tarifa16;
	private Double iva16;
	private Double tua;
	private Double otrosImp;
	private Double total;
	private String currencyType;
	private String paymentType;
	private String rfcDep;
	private String pax;
	private String ruta;
	private String tipDocto;
	private String email;
	private String observaciones;
	private String serie;
	private String transaction;
	private String pnrRef;
	private String Status;
	private Date lastUpdate;
	private Date lastVerified;
	private String observation;
	private String uuid;
	private String pathxml;
	private String pathpdf;
	
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	
	
}
