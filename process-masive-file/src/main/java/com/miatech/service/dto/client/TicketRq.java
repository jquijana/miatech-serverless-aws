package com.miatech.service.dto.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "TKTPNR", "RFC", "RFCName" , "REFERENCE"})
public class TicketRq {

	@JsonProperty("TKTPNR")
	private String tktPnr;

	@JsonProperty("RFC")
	private String rfc;

	@JsonProperty("RFCName")
	private String rfcName;

	@JsonProperty("REFERENCE")
	private String reference;

	@JsonProperty("TKTPNR")
	public String getTktPnr() {
		return tktPnr;
	}

	@JsonProperty("TKTPNR")
	public void setTktPnr(String tktPnr) {
		this.tktPnr = tktPnr;
	}

	@JsonProperty("RFC")
	public String getRfc() {
		return rfc;
	}

	@JsonProperty("RFC")
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@JsonProperty("RFCName")
	public String getRfcName() {
		return rfcName;
	}

	@JsonProperty("RFCName")
	public void setRfcName(String rfcName) {
		this.rfcName = rfcName;
	}

	@JsonProperty("REFERENCE")
	public String getReference() {
		return rfcName;
	}

	@JsonProperty("REFERENCE")
	public void setReference(String reference) {
		this.reference = reference;
	}

}
