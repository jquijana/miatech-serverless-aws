package com.miatech.service.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NoAutomaticDto {

	private Long requestFileId;
	private String ticketNumber;
	private String iata;
	private String rfcRecipient;
	private String rfcName;
	private String reference;
	private String fileName;
	private String recipientEmail;
	private Date invoicedDateTime;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String fileNameXls;
	private String statusDsc;

}
