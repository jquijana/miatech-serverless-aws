package com.miatech.service.dto.client;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IneRq {

	@SerializedName("Version")
	private String version;

	@SerializedName("TipoProceso")
	private String tipoProceso;

	@SerializedName("Entidad")
	private List<IneEntidadRq> entidad;

}
