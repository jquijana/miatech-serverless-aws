package com.miatech.service.dto.client;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimbradoEspejoRq {

	@SerializedName("Tkt")
	private String tkt;

	@SerializedName("DocType")
	private String docType;

	@SerializedName("ReceiverRFC")
	private String receiverRFC;

	@SerializedName("ReceiverName")
	private String receiverName;

	@SerializedName("IATA")
	private String iata;

	@SerializedName("Currency")
	private String currency;

	@SerializedName("FOP")
	private String fop;

	@SerializedName("OALTNU0")
	private String oaltNuo;

	@SerializedName("OALTNU2")
	private String oaltNu2;

	@SerializedName("OALTNU8")
	private String oaltNu8;

	@SerializedName("OALTNU16")
	private String oaltNu16;
	
	@SerializedName("TUA")
	private String tua;
	
	@SerializedName("XT")
	private String xt;
	
	@SerializedName("Comentario")
	private String comentario;
	
	@SerializedName("User")
	private String user;
	
	@SerializedName("ReceiverEmail")
	private String ReceiverEmail;
}
