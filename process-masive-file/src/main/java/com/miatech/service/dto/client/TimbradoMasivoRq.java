package com.miatech.service.dto.client;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimbradoMasivoRq {

	@SerializedName("TktPNR")
	private String[] tktPnr;

	@SerializedName("ReceiverRFC")
	private String rfc;

	@SerializedName("ReceiverName")
	private String rfcName;

	@SerializedName("ReceiverEmail")
	private String receiverEmail;

	@SerializedName("RFCFrecuente")
	private String rfcFrecuent;

	@SerializedName("Source")
	private String source;

	@SerializedName("User")
	private String user;

	@SerializedName("RequestDateTime")
	private String requestDateTime;

	@SerializedName("Comentario")
	private String comentario;

	@SerializedName("INE")
	private String jsonIne;
}
