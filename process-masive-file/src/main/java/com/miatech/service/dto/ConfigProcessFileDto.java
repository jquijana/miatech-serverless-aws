package com.miatech.service.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigProcessFileDto {

	private Integer timeSendMail;
	private Date dateNow;
	private Integer timeSendMailLimit;
	
}
