package com.miatech.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.miatech.service.config.DBConection;
import com.miatech.service.dto.ConfigProcessFileDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Constants.QueryAlias;

public class ConfigProcessFileDaoImpl implements ConfigProcessFileDao {

	private static final Logger LOGGER = LogManager.getLogger(ConfigProcessFileDaoImpl.class);

	@Override
	public ConfigProcessFileDto getDataConfig(String codeTable) throws BaseException {
		LOGGER.info("getDataConfig() -> :: codeTable: {} ", codeTable);
		List<ConfigProcessFileDto> response = new ArrayList<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_CONFIG);
			ps.setString(1, codeTable);
			ps.setString(2, "TIME_SEND_MAIL");
			ps.setString(3, "TIME_SEND_MAIL_LIMIT");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Integer value = rs.getInt(QueryAlias.VALOR_PARAMETER);
				Date dateNow = rs.getTimestamp(QueryAlias.DATE_NOW);
				String nameParamter = rs.getString(QueryAlias.NAME_PARAMETER);
				
				if (nameParamter.equals("TIME_SEND_MAIL")) {
					ConfigProcessFileDto configProcessFile = new ConfigProcessFileDto();
					configProcessFile.setTimeSendMail(value);
					configProcessFile.setDateNow(dateNow);					
					response.add(configProcessFile);
				}else if(nameParamter.equals("TIME_SEND_MAIL_LIMIT")) {
					response.get(0).setTimeSendMailLimit(value);
				}
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return (response != null && response.size() > 0) ? response.get(0) : null;
	}
}
