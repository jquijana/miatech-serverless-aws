package com.miatech.service.dao;

import java.util.List;

import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.exception.BaseException;

public interface ProcessEspejoFileDao {

	void updateEspejoFileData(EspejoFileDto data) throws BaseException;

	void updateEspejoFileData(List<EspejoFileDto> lstData) throws BaseException;

	void save(List<EspejoFileDto> lstData) throws BaseException;

}
