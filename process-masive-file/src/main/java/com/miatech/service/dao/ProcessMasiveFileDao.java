package com.miatech.service.dao;

import java.util.List;

import com.miatech.service.dto.IneDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.exception.BaseException;

public interface ProcessMasiveFileDao {

	void updateMasiveFileData(MasiveFileDto masiveFileDto) throws BaseException;

	void updateMasiveFileData(List<MasiveFileDto> masiveFileDtos) throws BaseException;

	void save(List<MasiveFileDto> masiveFileDtos) throws BaseException;

	List<IneDto> getDataIneByRequestFileId(String requestFileId) throws BaseException;
	
	Boolean existeComplementoIne(Long requestFileId) throws BaseException;

	List<IneDto> getDataIneByTicketNumber(Long requestFileId, String ticketNumber) throws BaseException;

}
