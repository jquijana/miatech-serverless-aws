package com.miatech.service.dao;

import com.miatech.service.dto.ConfigProcessFileDto;
import com.miatech.service.exception.BaseException;

public interface ConfigProcessFileDao {

	ConfigProcessFileDto getDataConfig(String codeTable) throws BaseException;
}
