package com.miatech.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.DBConection;
import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.exception.BaseException;

public class ProcessEspejoFileDaoImpl implements ProcessEspejoFileDao {

	private static final Logger LOGGER = LogManager.getLogger(ProcessEspejoFileDaoImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	@Override
	public void updateEspejoFileData(EspejoFileDto data) throws BaseException {
		LOGGER.info("updateEspejoFileData() -> :: " + GSON.toJson(data));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE PROCESS_ESPEJO_FILE "
					+ "SET STATUS = ? , OBSERVATION = ? , LASTUPDATE = NOW(), LASTVERIFIED = NOW(), "
					+ "UUID = ? , PATHXML = ? , PATHPDF = ? ,   " 
					+ "UPDATEDBY = 'SYSTEM', UPDATEDON = NOW(), SERIE = ?, TRANSACTION = ?, PNRREF = ? "
					+ "WHERE AUTOID = ?  ");

			ps.setString(1, data.getStatus());
			ps.setString(2, data.getObservation());
			ps.setString(3, data.getUuid());
			ps.setString(4, data.getPathxml());
			ps.setString(5, data.getPathpdf());
			ps.setString(6, data.getSerie());
			ps.setString(7, data.getTransaction());
			ps.setString(8, data.getPnrRef());
			ps.setLong(9, data.getId());
			ps.addBatch();
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL ACTUALIZAR TABLA PROCESS_ESPEJO_FILE  : {}", ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void updateEspejoFileData(List<EspejoFileDto> lstData) throws BaseException {
		LOGGER.info("updateEspejoFileData() -> :: " + GSON.toJson(lstData));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE PROCESS_ESPEJO_FILE "
					+ "SET STATUS = ? , OBSERVATION = ? , LASTUPDATE = NOW(), LASTVERIFIED = NOW(), "
					+ "UUID = ? , PATHXML = ? , PATHPDF = ? ,   " + "UPDATEDBY = 'SYSTEM', UPDATEDON = NOW(), SERIE = ?, TRANSACTION = ?, PNRREF = ?   "
					+ "WHERE AUTOID = ?  ");

			for (EspejoFileDto data : lstData) {
				ps.setString(1, data.getStatus());
				ps.setString(2, data.getObservation());
				ps.setString(3, data.getUuid());
				ps.setString(4, data.getPathxml());
				ps.setString(5, data.getPathpdf());
				ps.setString(6, data.getSerie());
				ps.setString(7, data.getTransaction());
				ps.setString(8, data.getPnrRef());
				ps.setLong(9, data.getId());
				ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL ACTUALIZAR TABLA PROCESS_ESPEJO_FILE  : {}",  ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void save(List<EspejoFileDto> lstData) throws BaseException {
		LOGGER.info("save() -> :: " + GSON.toJson(lstData));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO PROCESS_ESPEJO_FILE "
					+ "(REQUESTFILEID, AIRLINECODE, VOUCHERNUMBER, IATA, TARIFA_0, IVA_0, TARIFA_16, IVA_16, "
					+ "TUA, OTROS_IMP, TOTAL, CURRENCYTYPE, PAYMENTTYPE, RFCDEP, PAX, RUTA, TIPDOCTO, EMAIL, "
					+ "STATUS, LASTUPDATE, LASTVERIFIED, UUID, OBSERVATION, PATHXML, PATHPDF, CREATEDBY, CREATEDON, UPDATEDBY, UPDATEDON ) "
					+ "VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , "
					+ " ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , "
					+ " ? , NOW(), NOW(), ? , ? , ? , ?, 'SYSTEM', NOW(), 'SYSTEM', NOW() )  ");

			for (EspejoFileDto data : lstData) {
				ps.setLong(1, data.getRequestFileId());
				ps.setString(2, data.getAirlineCode());
				ps.setString(3, data.getVoucherNumber());
				ps.setString(4, data.getIata());
				ps.setDouble(5, data.getTarifa0());
				ps.setDouble(6, data.getIva0());
				ps.setDouble(7, data.getTarifa16());
				ps.setDouble(8, data.getIva16());
				ps.setDouble(9, data.getTua());
				ps.setDouble(10, data.getOtrosImp());
				ps.setDouble(11, data.getTotal());
				ps.setString(12, data.getCurrencyType());
				ps.setString(13, data.getPaymentType());
				ps.setString(14, data.getRfcDep());
				ps.setString(15, data.getPax());
				ps.setString(16, data.getRuta());
				ps.setString(17, data.getTipDocto());
				ps.setString(18, data.getEmail());
				ps.setString(19, data.getStatus());
				ps.setString(20, data.getUuid());
				ps.setString(21, data.getObservation());
				ps.setString(22, data.getPathxml());
				ps.setString(23, data.getPathpdf());
				ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL GRABAR PROCESS_ESPEJO_FILE : {} ",  ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
