package com.miatech.service.dao;

import java.util.List;

import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.exception.BaseException;

public interface RequestFileDao {

	void updateRequestFile(RequestFileDto requestFileDto) throws BaseException;

	void updateTicketsProscedRequestFile(Long requestFileId, Integer procesados, Integer calculados) throws BaseException;

	List<RequestFileDto> getDataRequestFilePendiente() throws BaseException;

	RequestFileDto getDataMasiveFileDetail(String requestFileId) throws BaseException;
	
	RequestFileDto getDataEspejoFileDetail(String requestFileId) throws BaseException;
	
	RequestFileDto getRequestFileById(String requestFileId) throws BaseException;

	RequestFileDto getDataMasiveById(String masiveFileId) throws BaseException;

	RequestFileDto getDataEspejoById(String espejoFileId) throws BaseException;

}
