package com.miatech.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.DBConection;
import com.miatech.service.dto.IneDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Constants.QueryAlias;

public class ProcessMasiveFileDaoImpl implements ProcessMasiveFileDao {

	private static final Logger LOGGER = LogManager.getLogger(ProcessMasiveFileDaoImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	@Override
	public void updateMasiveFileData(MasiveFileDto masiveFileDto) throws BaseException {
		LOGGER.info("updateMasiveFileData() -> :: " + GSON.toJson(masiveFileDto));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE PROCESS_MASIVE_FILE "
					+ "SET STATUS = ? , OBSERVATION = ? , TYPETICKET = ? , LASTUPDATE = NOW(), LASTVERIFIED = NOW(), "
					+ "UUID = ? , PATHXML = ? , PATHPDF = ? ,   " + "UPDATEDBY = 'SYSTEM', UPDATEDON = NOW(), SERIE = ?, TRANSACTION = ?, PNRREF = ? "
					+ "WHERE AUTOID = ?  ");

			ps.setString(1, masiveFileDto.getStatus());
			ps.setString(2, masiveFileDto.getObservation());
			ps.setString(3, masiveFileDto.getTicketType());
			ps.setString(4, masiveFileDto.getUuid());
			ps.setString(5, masiveFileDto.getPathxml());
			ps.setString(6, masiveFileDto.getPathpdf());
			ps.setString(7, masiveFileDto.getSerie());
			ps.setString(8, masiveFileDto.getTransaction());
			ps.setString(9, masiveFileDto.getPnrRef());
			ps.setLong(10, masiveFileDto.getId());
			ps.addBatch();
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL ACTUALIZAR TABLA Process_Masive_File  : {}", ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void updateMasiveFileData(List<MasiveFileDto> masiveFileDtos) throws BaseException {
		LOGGER.info("updateMasiveFileData() -> :: " + GSON.toJson(masiveFileDtos));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE PROCESS_MASIVE_FILE "
					+ "SET STATUS = ? , OBSERVATION = ? , TYPETICKET = ? , LASTUPDATE = NOW(), LASTVERIFIED = NOW(), "
					+ "UUID = ? , PATHXML = ? , PATHPDF = ? ,   " + "UPDATEDBY = 'SYSTEM', UPDATEDON = NOW(), SERIE = ?, TRANSACTION = ?  "
					+ "WHERE AUTOID = ?  ");

			for (MasiveFileDto data : masiveFileDtos) {
				ps.setString(1, data.getStatus());
				ps.setString(2, data.getObservation());
				ps.setString(3, data.getTicketType());
				ps.setString(4, data.getUuid());
				ps.setString(5, data.getPathxml());
				ps.setString(6, data.getPathpdf());
				ps.setString(7, data.getSerie());
				ps.setString(8, data.getTransaction());
				ps.setLong(9, data.getId());
				ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL ACTUALIZAR TABLA Process_Masive_File  : {}",  ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void save(List<MasiveFileDto> masiveFileDtos) throws BaseException {
		LOGGER.info("save() -> :: " + GSON.toJson(masiveFileDtos));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO PROCESS_MASIVE_FILE "
					+ "(TICKETNUMBER, REQUESTFILEID, PNRREF, RFC, COMPANYNAME, ZIPCODE, FILENAME, REFERENCE, EMAIL, STATUS, LASTUPDATE, LASTVERIFIED, CREATEDBY, CREATEDON, UPDATEDBY, UPDATEDON, "
					+ " TYPETICKET, UUID, PATHXML, PATHPDF, SERIE, TRANSACTION) "
					+ "VALUES ( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  now() ,  now() , 'SYSTEM', NOW(), 'SYSTEM', NOW() , ?, ?, ?, ?, ?, ? ) ");

			for (MasiveFileDto data : masiveFileDtos) {
				ps.setString(1, data.getTicketNumber());
				ps.setLong(2, data.getRequestFileId());
				ps.setString(3, data.getPnrRef());
				ps.setString(4, data.getRfc());
				ps.setString(5, data.getCompanyName());
				ps.setString(6, data.getZipCode());
				ps.setString(7, data.getFileName());
				ps.setString(8, data.getReference());
				ps.setString(9, data.getEmail());
				ps.setString(10, data.getStatus());
				ps.setString(11, data.getTicketType());
				ps.setString(12, data.getUuid());
				ps.setString(13, data.getPathxml());
				ps.setString(14, data.getPathpdf());
				ps.setString(15, data.getSerie());
				ps.setString(16, data.getTransaction());
				ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL GRABAR Process_Masive_File : {} ",  ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Boolean existeComplementoIne(Long requestFileId) throws BaseException {
		LOGGER.info("existeComplementoIne() -> :: requestFileId : " + requestFileId);
		List<Integer> response = new ArrayList<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.VALIDA_EXISTE_COMPLEMENTO_INE);
			ps.setLong(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Integer contador = rs.getInt(QueryAlias.EXISTE_INE);
				response.add(contador);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return response.get(0) == 0 ? true :false;
	}
	
	@Override
	public List<IneDto> getDataIneByRequestFileId(String requestFileId) throws BaseException {
		LOGGER.info("getDataIne() -> :: requestFileId : " + requestFileId);
		List<IneDto> response = new ArrayList<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_INE_BY_REQUEST_ID);
			ps.setString(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				response.add(getIneDto(rs));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

	private IneDto getIneDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.INE_ID);
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);
		String ambitoType = rs.getString(QueryAlias.AMBITO_TYPE);
		String comiteType = rs.getString(QueryAlias.COMITE_TYPE);
		String contabilidadId = rs.getString(QueryAlias.CONTABILIDAD_ID);
		String documentType = rs.getString(QueryAlias.DOCUMENT_TYPE);
		String entity = rs.getString(QueryAlias.ENTITY);
		String observation = rs.getString(QueryAlias.INE_OBSERVATION);
		String ticketNumber = rs.getString(QueryAlias.TICKET_NUMBER);
		String processType = rs.getString(QueryAlias.PROCESS_TYPE);

		IneDto ineDto = new IneDto();
		ineDto.setId(id);
		ineDto.setRequestFileId(requestFileId);
		ineDto.setAmbitoType(ambitoType);
		ineDto.setComiteType(comiteType);
		ineDto.setContabilidadId(contabilidadId);
		ineDto.setDocumentType(documentType);
		ineDto.setEntity(entity);
		ineDto.setObservation(observation);
		ineDto.setProcessType(processType);
		ineDto.setRequestFileId(requestFileId);
		ineDto.setTicketNumber(ticketNumber);
		return ineDto;
	}

	@Override
	public List<IneDto> getDataIneByTicketNumber(Long requestFileId, String ticketNumber) throws BaseException {
		LOGGER.info("getDataIneByTicketNumber() -> :: requestFileId : {},  ticketNumber : {}", requestFileId,
				ticketNumber);
		List<IneDto> response = new ArrayList<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_INE_BY_REQUEST_ID_AND_TICKET);
			ps.setLong(1, requestFileId);
			ps.setString(2, ticketNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				response.add(getIneDto(rs));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

}
