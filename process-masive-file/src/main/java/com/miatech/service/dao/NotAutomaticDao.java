package com.miatech.service.dao;

import com.miatech.service.dto.NoAutomaticDto;
import com.miatech.service.exception.BaseException;

public interface NotAutomaticDao {

	void save(NoAutomaticDto noAutomaticDtos) throws BaseException;
}
