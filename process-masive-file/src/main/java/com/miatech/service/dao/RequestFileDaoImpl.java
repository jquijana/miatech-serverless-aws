package com.miatech.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.DBConection;
import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Constants.QueryAlias;

public class RequestFileDaoImpl implements RequestFileDao {

	private static final Logger LOGGER = LogManager.getLogger(RequestFileDaoImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	@Override
	public void updateRequestFile(RequestFileDto requestFileDto) throws BaseException {
		LOGGER.info("updateRequestFile() -> :: " + GSON.toJson(requestFileDto));
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE REQUEST_FILES "
					+ "SET NUMBERTICKETSPROCESED = ? , STATUS =  ? , CALCULATEDNUMBERTICKETS =  ? , LASTUPDATE = NOW(), LASTVERIFIED = NOW(), UPDATEDBY = 'SYSTEM', UPDATEDON = NOW() "
					+ "WHERE AUTOID = ? ");

			ps.setInt(1, requestFileDto.getNumberTicketsProcesed());
			ps.setLong(2, requestFileDto.getStatus());
			ps.setInt(3, requestFileDto.getCalculatedNumberTickets());
			ps.setLong(4, requestFileDto.getId());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL ACTUALIZAR REQUEST FILE : ", e.getMessage());
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<RequestFileDto> getDataRequestFilePendiente() throws BaseException {
		LOGGER.info("getDataRequestFilePendiente() -> :: ");

		List<RequestFileDto> response = new ArrayList<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_REQUEST_FILE_PENDIENT);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				response.add(getRequestFileDto(rs));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

	private RequestFileDto getRequestFileDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.AUTO_ID);
		String user = rs.getString(QueryAlias.USER);
		String fileType = rs.getString(QueryAlias.FILE_TYPE);
		String filePathOriginal = rs.getString(QueryAlias.FILE_PATH_ORIGINAL);
		String filePathWorking = rs.getString(QueryAlias.FILE_PATH_WORKING);
		String filePathResult = rs.getString(QueryAlias.FILE_PATH_RESULT);
		Date requestDateTime = rs.getTimestamp(QueryAlias.REQUEST_DATE);
		Integer origNumberTickets = rs.getInt(QueryAlias.ORIGIN_NUMBER_TICKETS);
		Integer calculatedNumberTickets = rs.getInt(QueryAlias.CALCULATED_NUMBER_TICKETS);
		Integer numberTicketsProcesed = rs.getInt(QueryAlias.NUMBER_TICKETS_PROCESED);
		Integer status = rs.getInt(QueryAlias.REQUEST_FILE_STATUS);
		Date lastUpdate = rs.getTimestamp(QueryAlias.LAST_UPDATE);
		Date lastVerified = rs.getTimestamp(QueryAlias.LAST_VERIFIED);

		String createdBy = rs.getString(QueryAlias.CREATED_BY);
		Date createdOn = rs.getTimestamp(QueryAlias.CREATED_ON);
		String updatedBy = rs.getString(QueryAlias.UPDATED_BY);
		Date updatedOn = rs.getTimestamp(QueryAlias.UPDATED_ON);

		RequestFileDto requestFileDto = new RequestFileDto();
		requestFileDto.setId(id);
		requestFileDto.setUser(user);
		requestFileDto.setFileType(fileType);
		requestFileDto.setFilePathOriginal(filePathOriginal);
		requestFileDto.setFilePathWorking(filePathWorking);
		requestFileDto.setFilePathResult(filePathResult);
		requestFileDto.setRequestDateTime(requestDateTime);
		requestFileDto.setOrigNumberTickets(origNumberTickets);
		requestFileDto.setCalculatedNumberTickets(calculatedNumberTickets);
		requestFileDto.setNumberTicketsProcesed(numberTicketsProcesed);
		requestFileDto.setStatus(status);
		requestFileDto.setLastUpdate(lastUpdate);
		requestFileDto.setLastVerified(lastVerified);
		requestFileDto.setCreatedBy(createdBy);
		requestFileDto.setCreatedOn(createdOn);
		requestFileDto.setUpdatedBy(updatedBy);
		requestFileDto.setUpdatedOn(updatedOn);
		return requestFileDto;
	}

	@Override
	public RequestFileDto getDataMasiveFileDetail(String requestFileId) throws BaseException {
		LOGGER.info("getDataMasiveFileDetail() -> :: "+ requestFileId);

		HashMap<Long, RequestFileDto> requestFileMap = new HashMap<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_MASIVE_FILE_DETAIL);
			ps.setString(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				parseMasiveFileDto(rs, requestFileMap);
				
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		List<RequestFileDto> response = new ArrayList<RequestFileDto>(requestFileMap.values());
		return (response != null && response.size() > 0) ? response.get(0) : null;
	}
	
	@Override
	public RequestFileDto getRequestFileById(String requestFileId) throws BaseException {
		LOGGER.info("getRequestFileById() -> :: "+ requestFileId);
		List<RequestFileDto> response = new ArrayList<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_REQUEST_FILE_BY_ID);
			ps.setString(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				response.add(getRequestFileDto(rs));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return (response != null && response.size() > 0) ? response.get(0) : null;
	}

	private MasiveFileDto buildMasiveFileDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.AUTO_ID);
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);
		String ticketNumber = rs.getString(QueryAlias.TICKET_NUMBER);
		String ticketType = rs.getString(QueryAlias.TYPE_TICKET);
		String rfc = rs.getString(QueryAlias.RFC);
		String companyName = rs.getString(QueryAlias.COMPANY_NAME);
		String zipCode = rs.getString(QueryAlias.ZIP_CODE);
		String fileName = rs.getString(QueryAlias.FILE_NAME);
		String Reference = rs.getString(QueryAlias.REFERENCE);
		String Email = rs.getString(QueryAlias.EMAIL);
		String Status = rs.getString(QueryAlias.STATUS);
		String uuid = rs.getString(QueryAlias.UUID);
		String observation = rs.getString(QueryAlias.OBSERVATION);
		String pathpdf = rs.getString(QueryAlias.PATH_PDF);
		String pathxml = rs.getString(QueryAlias.PATH_XML);
		Date lastUpdate = rs.getTimestamp(QueryAlias.LAST_UPDATE);
		Date lastVerified = rs.getTimestamp(QueryAlias.LAST_VERIFIED);
		String createdBy = rs.getString(QueryAlias.CREATED_BY);
		Date createdOn = rs.getTimestamp(QueryAlias.CREATED_ON);
		String updatedBy = rs.getString(QueryAlias.UPDATED_BY);
		Date updatedOn = rs.getTimestamp(QueryAlias.UPDATED_ON);

		MasiveFileDto masiveFileDto = new MasiveFileDto();
		masiveFileDto.setId(id);
		masiveFileDto.setRequestFileId(requestFileId);
		masiveFileDto.setTicketNumber(ticketNumber);
		masiveFileDto.setTicketType(ticketType);
		masiveFileDto.setRfc(rfc);
		masiveFileDto.setCompanyName(companyName);
		masiveFileDto.setZipCode(zipCode);
		masiveFileDto.setFileName(fileName);
		masiveFileDto.setReference(Reference);
		masiveFileDto.setEmail(Email);
		masiveFileDto.setUuid(uuid);
		masiveFileDto.setObservation(observation);
		masiveFileDto.setPathpdf(pathpdf);
		masiveFileDto.setPathxml(pathxml);
		masiveFileDto.setStatus(Status);
		masiveFileDto.setLastUpdate(lastUpdate);
		masiveFileDto.setLastVerified(lastVerified);
		masiveFileDto.setCreatedBy(createdBy);
		masiveFileDto.setCreatedOn(createdOn);
		masiveFileDto.setUpdatedBy(updatedBy);
		masiveFileDto.setUpdatedOn(updatedOn);
		return masiveFileDto;
	}


	private EspejoFileDto buildEspejoFileDto(ResultSet rs) throws SQLException {
		Long id = rs.getLong(QueryAlias.AUTO_ID);
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);
		String airlineCode = rs.getString(QueryAlias.AIR_LINE_CODE);
		String voucherNumber = rs.getString(QueryAlias.VOUCHER_NUMBER);
		String iata = rs.getString(QueryAlias.IATA);
		Double tarifa0 = rs.getDouble(QueryAlias.TARIFA_0);
		Double iva0 = rs.getDouble(QueryAlias.IVA_0);
		Double tarifa16 = rs.getDouble(QueryAlias.TARIFA_16);
		Double iva16 = rs.getDouble(QueryAlias.IVA_16);
		Double tua = rs.getDouble(QueryAlias.TUA);
		Double otrosImp = rs.getDouble(QueryAlias.OTROS_IMP);
		Double total = rs.getDouble(QueryAlias.TOTAL);
		String currencyType = rs.getString(QueryAlias.CURRENCY_TYPE);
		String paymentType = rs.getString(QueryAlias.PAYMENT_TYPE);
		String rfcDep = rs.getString(QueryAlias.RFC_DEP);
		String pax = rs.getString(QueryAlias.PAX);
		String ruta = rs.getString(QueryAlias.RUTA);
		String tipDocto = rs.getString(QueryAlias.TIP_DOCTO);
		String Email = rs.getString(QueryAlias.EMAIL);
		String Status = rs.getString(QueryAlias.STATUS);
		String uuid = rs.getString(QueryAlias.UUID);
		String observation = rs.getString(QueryAlias.OBSERVATION);
		String pathpdf = rs.getString(QueryAlias.PATH_PDF);
		String pathxml = rs.getString(QueryAlias.PATH_XML);
		Date lastUpdate = rs.getTimestamp(QueryAlias.LAST_UPDATE);
		Date lastVerified = rs.getTimestamp(QueryAlias.LAST_VERIFIED);
		String createdBy = rs.getString(QueryAlias.CREATED_BY);
		Date createdOn = rs.getTimestamp(QueryAlias.CREATED_ON);
		String updatedBy = rs.getString(QueryAlias.UPDATED_BY);
		Date updatedOn = rs.getTimestamp(QueryAlias.UPDATED_ON);

		EspejoFileDto espejoFileDto = new EspejoFileDto();
		espejoFileDto.setId(id);
		espejoFileDto.setRequestFileId(requestFileId);
		espejoFileDto.setAirlineCode(airlineCode);
		espejoFileDto.setVoucherNumber(voucherNumber);
		espejoFileDto.setIata(iata);
		espejoFileDto.setTarifa0(tarifa0);
		espejoFileDto.setIva0(iva0);
		espejoFileDto.setTarifa16(tarifa16);
		espejoFileDto.setIva16(iva16);
		espejoFileDto.setTua(tua);
		espejoFileDto.setOtrosImp(otrosImp);
		espejoFileDto.setTotal(total);
		espejoFileDto.setCurrencyType(currencyType);
		espejoFileDto.setPaymentType(paymentType);
		espejoFileDto.setRfcDep(rfcDep);
		espejoFileDto.setPax(pax);
		espejoFileDto.setRuta(ruta);
		espejoFileDto.setTipDocto(tipDocto);
		espejoFileDto.setEmail(Email);
		espejoFileDto.setUuid(uuid);
		espejoFileDto.setObservation(observation);
		espejoFileDto.setPathpdf(pathpdf);
		espejoFileDto.setPathxml(pathxml);
		espejoFileDto.setStatus(Status);
		espejoFileDto.setLastUpdate(lastUpdate);
		espejoFileDto.setLastVerified(lastVerified);
		espejoFileDto.setCreatedBy(createdBy);
		espejoFileDto.setCreatedOn(createdOn);
		espejoFileDto.setUpdatedBy(updatedBy);
		espejoFileDto.setUpdatedOn(updatedOn);
		return espejoFileDto;
	}

	
	@Override
	public RequestFileDto getDataMasiveById(String masiveFileId) throws BaseException {
		LOGGER.info("getDataMasiveById() -> :: masiveFileId : {}", masiveFileId);
		HashMap<Long, RequestFileDto> requestFileMap = new HashMap<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_MASIVE_FILE_BY_ID);
			ps.setString(1, masiveFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				parseMasiveFileDto(rs, requestFileMap);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		List<RequestFileDto> response = new ArrayList<RequestFileDto>(requestFileMap.values());
		return (response != null && response.size() > 0) ? response.get(0) : null;
	}
	
	@Override
	public RequestFileDto getDataEspejoById(String espejoFileId) throws BaseException {
		LOGGER.info("getDataEspejoById() -> :: espejoFileId : {}", espejoFileId);
		HashMap<Long, RequestFileDto> requestFileMap = new HashMap<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_ESPEJO_FILE_BY_ID);
			ps.setString(1, espejoFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				parseEspejoFileDto(rs, requestFileMap);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		List<RequestFileDto> response = new ArrayList<RequestFileDto>(requestFileMap.values());
		return (response != null && response.size() > 0) ? response.get(0) : null;
	}

	private void parseMasiveFileDto(ResultSet rs, HashMap<Long, RequestFileDto> requestFileMap) throws SQLException {
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);

		if (!requestFileMap.containsKey(requestFileId)) {
			String user = rs.getString(QueryAlias.USER);
			Date requestDateTime = rs.getTimestamp(QueryAlias.CREATED_ON);
			Integer numberTicketsProcesed = rs.getInt(QueryAlias.NUMBER_TICKETS_PROCESED);
			Integer calculatedNumberTickets = rs.getInt(QueryAlias.CALCULATED_NUMBER_TICKETS_PROCESED);
			Integer origNumberTickets = rs.getInt(QueryAlias.ORIGIN_NUMBER_TICKETS);
			Integer requestFileStatus = rs.getInt(QueryAlias.REQUEST_FILE_STATUS);
			String filePathWorking = rs.getString(QueryAlias.FILE_PATH_WORKING);
			String fileType = rs.getString(QueryAlias.FILE_TYPE);

			RequestFileDto requestFileDto = new RequestFileDto();
			requestFileDto.setId(requestFileId);
			requestFileDto.setCalculatedNumberTickets(calculatedNumberTickets);
			requestFileDto.setOrigNumberTickets(origNumberTickets);
			requestFileDto.setNumberTicketsProcesed(numberTicketsProcesed);
			requestFileDto.setStatus(requestFileStatus);
			requestFileDto.setUser(user);
			requestFileDto.setRequestDateTime(requestDateTime);
			requestFileDto.setFilePathWorking(filePathWorking);
			requestFileDto.setFileType(fileType);
			requestFileDto.setLstMasiveFileDto(new ArrayList<>());
			requestFileDto.getLstMasiveFileDto().add(buildMasiveFileDto(rs));
			requestFileMap.put(requestFileId, requestFileDto);
		} else {
			RequestFileDto requestFileDto = requestFileMap.get(requestFileId);
			requestFileDto.getLstMasiveFileDto().add(buildMasiveFileDto(rs));
			requestFileMap.put(requestFileId, requestFileDto);
		}
	}
	
	private void parseEspejoFileDto(ResultSet rs, HashMap<Long, RequestFileDto> requestFileMap) throws SQLException {
		Long requestFileId = rs.getLong(QueryAlias.REQUEST_FILE_ID);

		if (!requestFileMap.containsKey(requestFileId)) {
			String user = rs.getString(QueryAlias.USER);
			Date requestDateTime = rs.getTimestamp(QueryAlias.CREATED_ON);
			Integer numberTicketsProcesed = rs.getInt(QueryAlias.NUMBER_TICKETS_PROCESED);
			Integer calculatedNumberTickets = rs.getInt(QueryAlias.CALCULATED_NUMBER_TICKETS_PROCESED);
			Integer origNumberTickets = rs.getInt(QueryAlias.ORIGIN_NUMBER_TICKETS);
			Integer requestFileStatus = rs.getInt(QueryAlias.REQUEST_FILE_STATUS);
			String filePathWorking = rs.getString(QueryAlias.FILE_PATH_WORKING);
			String fileType = rs.getString(QueryAlias.FILE_TYPE);
			
			RequestFileDto requestFileDto = new RequestFileDto();
			requestFileDto.setId(requestFileId);
			requestFileDto.setCalculatedNumberTickets(calculatedNumberTickets);
			requestFileDto.setOrigNumberTickets(origNumberTickets);
			requestFileDto.setNumberTicketsProcesed(numberTicketsProcesed);
			requestFileDto.setStatus(requestFileStatus);
			requestFileDto.setUser(user);
			requestFileDto.setFileType(fileType);
			requestFileDto.setRequestDateTime(requestDateTime);
			requestFileDto.setFilePathWorking(filePathWorking);
			requestFileDto.setLstEspejoFileDto(new ArrayList<>());
			requestFileDto.getLstEspejoFileDto().add(buildEspejoFileDto(rs));
			requestFileMap.put(requestFileId, requestFileDto);
		} else {
			RequestFileDto requestFileDto = requestFileMap.get(requestFileId);
			requestFileDto.getLstEspejoFileDto().add(buildEspejoFileDto(rs));
			requestFileMap.put(requestFileId, requestFileDto);
		}
	}

	@Override
	public void updateTicketsProscedRequestFile(Long requestFileId, Integer procesados, Integer calculados) throws BaseException {
		LOGGER.info("updateTicketsProscedRequestFile() -> requestFileId : {}, procesados: {}, calculados : {} ", requestFileId, procesados, calculados);

		try (Connection conn = new DBConection().getConnection();
				PreparedStatement ps = conn
						.prepareStatement("UPDATE REQUEST_FILES "
								+ "SET 	NUMBERTICKETSPROCESED = (NUMBERTICKETSPROCESED + ? ),  CALCULATEDNUMBERTICKETS = (COALESCE(CALCULATEDNUMBERTICKETS, NUMBERTICKETSPROCESED) + ? ), "
								+ "		LASTUPDATE = NOW(), LASTVERIFIED = NOW(), UPDATEDBY = 'SYSTEM', UPDATEDON = NOW() "
								+ "WHERE AUTOID = ? ");) {

			ps.setInt(1, procesados);
			ps.setInt(2, calculados);
			ps.setLong(3, requestFileId);
			ps.executeUpdate();
		} catch (Exception e) {
			LOGGER.error(" ERROR updateTicketsProscedRequestFile() {}", ExceptionUtils.getStackTrace(e));
			throw new BaseException(e);
		}
	}

	@Override
	public RequestFileDto getDataEspejoFileDetail(String requestFileId) throws BaseException {
		LOGGER.info("getDataEspejoFileDetail() -> :: "+ requestFileId);

		HashMap<Long, RequestFileDto> requestFileMap = new HashMap<>();
		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement(Constants.Query.GET_DATA_ESPEJO_FILE_DETAIL);
			ps.setString(1, requestFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				parseEspejoFileDto(rs, requestFileMap);
				
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		List<RequestFileDto> response = new ArrayList<RequestFileDto>(requestFileMap.values());
		return (response != null && response.size() > 0) ? response.get(0) : null;
	}

}
