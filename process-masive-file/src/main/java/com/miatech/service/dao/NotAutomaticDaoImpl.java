package com.miatech.service.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.config.DBConection;
import com.miatech.service.dto.NoAutomaticDto;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Utils;

public class NotAutomaticDaoImpl implements NotAutomaticDao {

	private static final Logger LOGGER = LogManager.getLogger(NotAutomaticDaoImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().create();

	@Override
	public void save(NoAutomaticDto noAutomaticDto) throws BaseException {
		LOGGER.info("save() -> :: {}",  GSON.toJson(noAutomaticDto));

		Connection conn = null;
		try {
			conn = new DBConection().getConnection();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO NOT_AUTOMATIC_TICKET "
					+ "(TICKETNUMBER, IATA, FILEID, RFCRECIPIENT, RFCNAME, REFERENCE, FILENAME, RECIPIENTEMAIL, INVOICEDDATETIME, CREATEDBY, CREATEDON, UPDATEDBY, UPDATEDON) "
					+ "VALUES ( ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  'SYSTEM' , NOW(),  'SYSTEM' , NOW() ) ");

				ps.setString(1, noAutomaticDto.getTicketNumber());
				ps.setString(2, noAutomaticDto.getIata());
				ps.setLong(3, noAutomaticDto.getRequestFileId());
				ps.setString(4, noAutomaticDto.getRfcRecipient());
				ps.setString(5, noAutomaticDto.getRfcName());
				ps.setString(6, noAutomaticDto.getReference());
				ps.setString(7, noAutomaticDto.getFileName());
				ps.setString(8, noAutomaticDto.getRecipientEmail());
				ps.setDate(9, Utils.convertDateToDateSql(noAutomaticDto.getInvoicedDateTime()));
				ps.addBatch();
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			LOGGER.error(" ERROR AL GRABAR Not_Automatic_Ticket : {}",  ExceptionUtils.getStackTrace(e));
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
