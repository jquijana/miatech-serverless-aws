package com.miatech.service.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.NotAutomaticDao;
import com.miatech.service.dao.NotAutomaticDaoImpl;
import com.miatech.service.dao.ProcessMasiveFileDao;
import com.miatech.service.dao.ProcessMasiveFileDaoImpl;
import com.miatech.service.dao.RequestFileDao;
import com.miatech.service.dao.RequestFileDaoImpl;
import com.miatech.service.dto.IneDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.dto.NoAutomaticDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.dto.client.ProcessIndividualRq;
import com.miatech.service.dto.client.ValidationFileRq;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;

public class ValidationMasiveFileServiceImpl implements ValidationMasiveFileService {

	private ProcessMasiveFileDao processMasiveFileDao;
	private RequestFileDao requestFileDao;
	private NotAutomaticDao notAutomaticDao;
	private RestTemplate restTemplate;

	private static final Logger LOGGER = LogManager.getLogger(ValidationMasiveFileServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(Constants.DATEFORMAT7).create();

	private static final String PROCESO_MASIVO_ESTADO_ERROR = "ERROR DE VALIDACION";
	private static final String PROCESO_MASIVO_ESTADO_EXCEPCION_REGISTRADA = "EXCEPCION REGISTRADA";
	private static final String TYPE_TICKET_TKT = "TKT";
	private static final String TYPE_TICKET_PNR = "PNR";
	private static final Integer REQUEST_FILE_ESTADO_COMPLETADO = 9;

	public ValidationMasiveFileServiceImpl(RestTemplate restTemplate) throws BaseException {
		this.processMasiveFileDao = new ProcessMasiveFileDaoImpl();
		this.requestFileDao = new RequestFileDaoImpl();
		this.notAutomaticDao = new NotAutomaticDaoImpl();
		this.restTemplate = restTemplate;
	}

	@Override
	public void validationMasive(ValidationFileRq request) throws BaseException {
		LOGGER.info("validationMasive() --> request : {}", GSON.toJson(request));
		Map<String, IneDto> ineNoTimbrarMap = new HashMap<>();
		RequestFileDto requestFileDto = requestFileDao.getDataMasiveFileDetail(request.getRequestFileId());
		List<IneDto> ineDtos = processMasiveFileDao.getDataIneByRequestFileId(request.getRequestFileId());

		Boolean terminarProcesoIne = Boolean.FALSE;
		if (ineDtos != null) {
			for (IneDto ine : ineDtos) {
				if (existeComplementoIne(requestFileDto)) {
					if (!isValidComplementoIne(ine)) {
						ineNoTimbrarMap.put(ine.getTicketNumber(), ine);
					}
				} else {
					terminarProcesoIne = Boolean.TRUE;
					break;
				}
			}
		}

		if (!terminarProcesoIne) {
			if (requestFileDto!=null && requestFileDto.getLstMasiveFileDto()!=null) {
				for (MasiveFileDto masiveFileDto : requestFileDto.getLstMasiveFileDto()) {
				NoAutomaticDto noAutomaticDto = null;
				try {
					if (validaNoTimbrarIne(ineNoTimbrarMap, masiveFileDto)
							&& (validaTicketNumberOrPnr(masiveFileDto) 
									&& validaRfc(masiveFileDto)
									&& validaTicketNumberAndRfc(masiveFileDto) 
									&& validaEmail(masiveFileDto)
									&& validaExcepcionReferencia(requestFileDto, noAutomaticDto, masiveFileDto))) {
						processMasiveFileDao.updateMasiveFileData(masiveFileDto);
						callTimbradoIndividual(requestFileDto.getFileType(), masiveFileDto);
					}else {
						updateNotValidation(requestFileDto.getId(), masiveFileDto);
					}
				} catch (Exception e) {
					LOGGER.error("ERROR () --> Validacion : {} , DETALLE : {} ", GSON.toJson(masiveFileDto), ExceptionUtils.getStackTrace(e));
				}
			}
			}
		} else {
			requestFileDto.setNumberTicketsProcesed(requestFileDto.getLstMasiveFileDto().size());
			requestFileDto.setStatus(REQUEST_FILE_ESTADO_COMPLETADO);
			requestFileDao.updateRequestFile(requestFileDto);
			requestFileDto.getLstMasiveFileDto().forEach(data -> { 
				joinObservacion(data, "COMPLEMENTO INE NO ENCONTRADO EN LA LISTA DE TICKETS");
			});
			processMasiveFileDao.updateMasiveFileData(requestFileDto.getLstMasiveFileDto());
		}
	}

	private void updateNotValidation(Long requestFileId,  MasiveFileDto  masiveFileDto) throws BaseException {
		if (masiveFileDto!=null) {
			processMasiveFileDao.updateMasiveFileData(masiveFileDto);
			requestFileDao.updateTicketsProscedRequestFile(requestFileId, 1, 0);
		}
	}
	
	private Boolean validaTicketNumberOrPnr(final MasiveFileDto masiveFileDto) {
		Boolean esValido = Boolean.TRUE;
		if (masiveFileDto.getTicketNumber() != null && !masiveFileDto.getTicketNumber().isEmpty()) {
			if (masiveFileDto.getTicketNumber().length() == 13
					&& masiveFileDto.getTicketNumber().substring(0, 3).equals("139")) {
				masiveFileDto.setTicketType(TYPE_TICKET_TKT);
			} else if (masiveFileDto.getTicketNumber().length() == 6) {
				masiveFileDto.setTicketType(TYPE_TICKET_PNR);
			} else {
				joinObservacion(masiveFileDto, "NUMERO DE TICKET/PNR NO PASA VALIDACION");
				esValido = Boolean.FALSE;
			}
		} else {
			joinObservacion(masiveFileDto, "CAMPOS OBLIGATORIAS FALTAN EN #TKT/PNR");
			esValido = Boolean.FALSE;
		}

		return esValido;
	}

	private Boolean existeComplementoIne(final RequestFileDto requestFileDto) throws BaseException {
		return  processMasiveFileDao.existeComplementoIne(requestFileDto.getId());
	}

	private Boolean isValidComplementoIne(IneDto ineDto) {
		Boolean esValido = Boolean.TRUE;

		// Valida ContabilidadId
		if (ineDto.getContabilidadId() == null || ineDto.getContabilidadId().trim().length() > 6) {
			Pattern patternMoral = Pattern.compile("([0-9])");
			Matcher mather = patternMoral.matcher(ineDto.getContabilidadId());
			if (mather.find() == false) {
				return Boolean.FALSE;
			}
		}

		// Valida TicketNumber
		if (ineDto.getTicketNumber() != null && !ineDto.getTicketNumber().isEmpty()) {
			if (ineDto.getTicketNumber().length() != 13 || !ineDto.getTicketNumber().substring(0, 3).equals("139")) {
				joinObservacionIne(ineDto, "NUMERO DE TICKET NO PASA VALIDACION");
				return esValido = Boolean.FALSE;
			}
		} else {
			joinObservacionIne(ineDto, "CAMPOS OBLIGATORIAS FALTAN EN #TKT");
			return esValido = Boolean.FALSE;
		}

		// Valida TipoProceso
		if (ineDto.getProcessType() != null && !ineDto.getProcessType().isEmpty()) {
			if (!(ineDto.getProcessType().toUpperCase().equals("ORDINARIO")
					|| ineDto.getProcessType().toUpperCase().equals("PRECAMPAÑA")
					|| ineDto.getProcessType().toUpperCase().equals("CAMPAÑA"))) {
				joinObservacionIne(ineDto, "TIPO PROCESO NO PASA VALIDACION");
				return esValido = Boolean.FALSE;
			}
		} else {
			joinObservacionIne(ineDto, "CAMPOS OBLIGATORIAS FALTAN EN TIPO PROCESO");
			return esValido = Boolean.FALSE;
		}

		// Valida Comite
		if (ineDto.getProcessType().toUpperCase().equals("ORDINARIO")) {
			if (ineDto.getComiteType() != null && !ineDto.getComiteType().isEmpty()) {
				if (!(ineDto.getComiteType().toUpperCase().equals("EJECUTIVO NACIONAL")
						|| ineDto.getComiteType().toUpperCase().equals("EJECUTIVO ESTATAL"))) {
					joinObservacionIne(ineDto, "TIPO COMITÉ DEBE SER EJECUTIVO NACIONAL O EJECUTIVO ESTATAL");
					return esValido = Boolean.FALSE;
				}
			} else {
				joinObservacionIne(ineDto, "TIPO DE PROCESO MANDATORIO SI PROCESO ES ORDINARIO");
				return esValido = Boolean.FALSE;
			}
		}

		// Valida TipoAmbito
		if (ineDto.getProcessType().toUpperCase().equals("PRECAMPAÑA")
				|| ineDto.getProcessType().toUpperCase().equals("CAMPAÑA")) {
			if (ineDto.getAmbitoType() != null && !ineDto.getAmbitoType().isEmpty()) {
				if (!(ineDto.getAmbitoType().toUpperCase().equals("LOCAL")
						|| ineDto.getAmbitoType().toUpperCase().equals("FEDERAL"))) {
					joinObservacionIne(ineDto, "TIPO DE AMBITO DEBE SER LOCAL O FEDERAL");
					return esValido = Boolean.FALSE;
				}
			} else {
				joinObservacionIne(ineDto, "TIPO DE AMBITO MANDATORIO SI PROCESO ES CAMPAÑA O PRECAMPAÑA");
				return esValido = Boolean.FALSE;
			}
		}

		return esValido;
	}

	private Boolean validaRfc(final MasiveFileDto masiveFileDto) {
		Boolean esValido = Boolean.TRUE;
		if (masiveFileDto.getRfc() == null || masiveFileDto.getRfc().isEmpty()) {
			joinObservacion(masiveFileDto, "CAMPOS OBLIGATORIAS FALTAN EN #TKT/PNR");
			esValido = Boolean.FALSE;
		} else {
			if (masiveFileDto.getRfc().length() == 12 || masiveFileDto.getRfc().length() == 13) {
				Pattern patternMoral = Pattern.compile(
						"([A-ZÑ&]{3,4}) ?(?:- ?)?(\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\d|3[01])) ?(?:- ?)?([A-Z\\d]{2})([A\\d])");
				Matcher mather = patternMoral.matcher(masiveFileDto.getRfc());
				if (mather.find() == false) {
					if (masiveFileDto.getRfc().length() == 12) {
						joinObservacion(masiveFileDto, "RFC MORAL NO PASA VALIDACION");
						esValido = Boolean.FALSE;
					} else {
						joinObservacion(masiveFileDto, "RFC FISICA NO PASA VALIDACION");
						esValido = Boolean.FALSE;
					}
				}
			} else {
				joinObservacion(masiveFileDto,
						"RFC NO PASA VALIDACION.  RFC TIENE QUE TENER 12 CARACTERES PARA RFC MORAL Y 13 CARACTERES PARA RFC FISICA");
				esValido = Boolean.FALSE;
			}
		}

		return esValido;
	}

	private Boolean validaTicketNumberAndRfc(final MasiveFileDto masiveFileDto) {
		Boolean esValido = Boolean.TRUE;
		if (masiveFileDto.getTicketNumber() != null
				&& (masiveFileDto.getRfc() == null || masiveFileDto.getRfc().isEmpty())) {
			joinObservacion(masiveFileDto, "CAMPOS OBLIGATORIAS FALTAN EN #TKT Y/O RFC");
			esValido = Boolean.FALSE;
		}

		return esValido;
	}

	private Boolean validaEmail(final MasiveFileDto masiveFileDto) {
		Boolean esValido = Boolean.TRUE;
		if (masiveFileDto.getEmail() != null && !masiveFileDto.getEmail().trim().isEmpty()) {
			Pattern pattern = Pattern.compile(
					"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			Matcher mather = pattern.matcher(masiveFileDto.getEmail());
			if (mather.find() == false) {
				joinObservacion(masiveFileDto, "CORREO ELECTRONICO NO PASA VALIDACION");
				esValido = Boolean.FALSE;
			}
		}

		return esValido;
	}

	private Boolean validaExcepcionReferencia(RequestFileDto requestFileDto, NoAutomaticDto noAutomaticDto, MasiveFileDto masiveFileDto) throws BaseException {
		Boolean esValido = Boolean.TRUE;
		if (masiveFileDto.getReference() != null && !masiveFileDto.getReference().trim().isEmpty()
				&& masiveFileDto.getReference().length() > 3) {
			String inicial = masiveFileDto.getReference().substring(0, 3);
			if ("#EE".equals(inicial)) {
				String iata = masiveFileDto.getReference().substring(3);
				Pattern patternMoral = Pattern.compile("[0-9]");
				Matcher mather = patternMoral.matcher(iata);
				if (mather.find() == false) {
					joinObservacion(masiveFileDto, "NUMERO DE IATA DE EXCEPCION ES INCORRECTA");
					esValido = Boolean.FALSE;
				} else {
					masiveFileDto.setStatus(PROCESO_MASIVO_ESTADO_EXCEPCION_REGISTRADA);
					esValido = Boolean.FALSE;
					
					noAutomaticDto = new NoAutomaticDto();
					noAutomaticDto.setTicketNumber(masiveFileDto.getTicketNumber());
					noAutomaticDto.setIata(iata);
					noAutomaticDto.setRequestFileId(masiveFileDto.getRequestFileId());
					noAutomaticDto.setRfcRecipient(masiveFileDto.getRfc());
					noAutomaticDto.setRecipientEmail(masiveFileDto.getEmail());
					noAutomaticDto.setRfcName(masiveFileDto.getCompanyName());
					noAutomaticDto.setCreatedBy(requestFileDto.getUser());
					notAutomaticDao.save(noAutomaticDto);
				}
			}
		}

		return esValido;
	}

	private void joinObservacion(MasiveFileDto masiveFileDto, String message) {
		masiveFileDto.setStatus(PROCESO_MASIVO_ESTADO_ERROR);
		if (masiveFileDto.getObservation() != null) {
			masiveFileDto.setObservation(masiveFileDto.getObservation() + "," + message);
		} else {
			masiveFileDto.setObservation(message);
		}
	}

	private void joinObservacionIne(IneDto ineDto, String message) {
		if (ineDto.getObservation() != null) {
			ineDto.setObservation(ineDto.getObservation() + "," + message);
		} else {
			ineDto.setObservation(message);
		}
	}

	private Boolean validaNoTimbrarIne(Map<String, IneDto> ineNoTimbrarMap, MasiveFileDto masiveFileDto) {
		Boolean esValido = Boolean.TRUE;
		if (ineNoTimbrarMap.containsKey(masiveFileDto.getTicketNumber())) {
			IneDto ineDto = ineNoTimbrarMap.get(masiveFileDto.getTicketNumber());
			joinObservacion(masiveFileDto, ineDto.getObservation());
			esValido = Boolean.FALSE;
		}
		return esValido;
	}

	private HttpHeaders generateHeaderRequest() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}

	private void callTimbradoIndividual(String fileType, MasiveFileDto masiveFileDto) {
		LOGGER.info("Call Envio Procces Individual File :: -> request  : {}",  GSON.toJson(masiveFileDto));
		try {
			ProcessIndividualRq envioFileRq = new ProcessIndividualRq();
			envioFileRq.setDataFileId(masiveFileDto.getId().toString());
			envioFileRq.setFileType(fileType);
			String urlValidationFile = Constants.Enviroments.URL_TIMBRADO_INDIVIDUAL;
			HttpEntity<ProcessIndividualRq> request = new HttpEntity<ProcessIndividualRq>(envioFileRq, generateHeaderRequest());
			String responseEntity = restTemplate.postForObject(urlValidationFile, request, String.class);
			LOGGER.info("Call Envio Procces Individual File :: -> response  : {} ", GSON.toJson(responseEntity));
		} catch (Exception e) {
			LOGGER.error("ERROR callTimbradoIndividual : {}" ,  ExceptionUtils.getStackTrace(e) );
		}
	}
}
