package com.miatech.service.service;

import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;

public interface TimbradoMasivoFileService {

	void timbrado(String masiveFileId) throws BusinessException, BaseException;
	
}
