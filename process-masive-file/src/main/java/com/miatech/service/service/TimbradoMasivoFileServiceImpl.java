package com.miatech.service.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.ProcessMasiveFileDao;
import com.miatech.service.dao.ProcessMasiveFileDaoImpl;
import com.miatech.service.dao.RequestFileDao;
import com.miatech.service.dao.RequestFileDaoImpl;
import com.miatech.service.dto.IneDto;
import com.miatech.service.dto.MasiveFileDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.dto.client.IneContabilidadRq;
import com.miatech.service.dto.client.IneEntidadRq;
import com.miatech.service.dto.client.IneRq;
import com.miatech.service.dto.client.TicketRs;
import com.miatech.service.dto.client.TimbradoMasivoRq;
import com.miatech.service.dto.client.TimbradoMasivoRs;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Utils;

public class TimbradoMasivoFileServiceImpl implements TimbradoMasivoFileService {

	private ProcessMasiveFileDao processMasiveFileDao;
	private RequestFileDao requestFileDao;
	private RestTemplate restTemplate;
	private Integer contadorProcesados = 0;
	private Integer contadorCalculados = 0;
	
	private static final Logger LOGGER = LogManager.getLogger(TimbradoMasivoFileServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(Constants.DATEFORMAT7).create();

	private static final String PROCESO_MASIVO_ESTADO_ERROR_DE_PROCESO= "ERROR DE PROCESO";
	private static final String TYPE_TICKET_TKT = "TKT";

	public TimbradoMasivoFileServiceImpl(RestTemplate restTemplate) throws BaseException {
		this.processMasiveFileDao = new ProcessMasiveFileDaoImpl();
		this.requestFileDao = new RequestFileDaoImpl();
		this.restTemplate = restTemplate;
	}

	@Override
	public void timbrado(String masiveFileId) throws BaseException {
		LOGGER.info("timbrado() --> masiveFileId : {} ", masiveFileId);
		RequestFileDto requestFileDto = requestFileDao.getDataMasiveById(masiveFileId);
		List<MasiveFileDto> masiveFilePnrNews = new ArrayList<>();

		requestFileDto.getLstMasiveFileDto().forEach(masiveFileDto -> {
			try {
				List<IneDto> ineDtos = processMasiveFileDao.getDataIneByTicketNumber(masiveFileDto.getRequestFileId(), masiveFileDto.getTicketNumber());
				TimbradoMasivoRs timbradoRs = callByTicketNumber(requestFileDto, masiveFileDto, ineDtos);
				if (timbradoRs != null) {
					contadorProcesados++;
					 if (timbradoRs.getStatus().equals("OK")) {
						 masiveFileDto.setStatus("PROCESADO");
						if (timbradoRs.getData().size() > 1) {
							timbradoRs.getData().forEach(ticketRs -> {
								MasiveFileDto masiveFileDtoNew = createMasiveFileDto(masiveFileDto, ticketRs);
								masiveFilePnrNews.add(masiveFileDtoNew);
								contadorProcesados++;
								contadorCalculados++;
							});
						} else {
							TicketRs ticketRs = timbradoRs.getData().get(0);
							masiveFileDto.setStatus(ticketRs.getEstado());
							masiveFileDto.setUuid(ticketRs.getUuid());
							masiveFileDto.setPathxml(ticketRs.getPathxml());
							masiveFileDto.setPathpdf(ticketRs.getPathpdf());
							masiveFileDto.setSerie(ticketRs.getSerie());
							masiveFileDto.setPnrRef(ticketRs.getPnr());
							masiveFileDto.setTransaction(ticketRs.getTrans());
						}
					} else {
						TicketRs ticketRs = timbradoRs.getData().get(0);
						masiveFileDto.setStatus(ticketRs.getEstado());
					}
					
					processMasiveFileDao.save(masiveFilePnrNews);
					processMasiveFileDao.updateMasiveFileData(masiveFileDto);
					requestFileDao.updateTicketsProscedRequestFile(requestFileDto.getId(), contadorProcesados, contadorCalculados);
				}
			} catch (Exception e) {
				LOGGER.error("ERROR DE timbradoIndividual() --> masiveFileId : {}, DETALLE : {} ", masiveFileId,  ExceptionUtils.getStackTrace(e));
			}
		});
	}
 
	private MasiveFileDto createMasiveFileDto(MasiveFileDto masiveFileDto, TicketRs ticketRs) {
		MasiveFileDto masiveFileNew = new MasiveFileDto();
		masiveFileNew.setRequestFileId(masiveFileDto.getRequestFileId());
		masiveFileNew.setTicketNumber(ticketRs.getTkt());
		masiveFileNew.setSerie(ticketRs.getSerie());
		masiveFileNew.setTicketType(TYPE_TICKET_TKT);
		masiveFileNew.setPnrRef(ticketRs.getPnr());
		masiveFileNew.setRfc(masiveFileDto.getRfc());
		masiveFileNew.setCompanyName(masiveFileDto.getCompanyName());
		masiveFileNew.setZipCode(masiveFileDto.getZipCode());
		masiveFileNew.setFileName(masiveFileDto.getFileName());
		masiveFileNew.setReference(masiveFileDto.getReference());
		masiveFileNew.setEmail(masiveFileDto.getEmail());
		masiveFileNew.setStatus(ticketRs.getEstado());
		masiveFileNew.setUuid(ticketRs.getUuid());
		masiveFileNew.setPathxml(ticketRs.getPathxml());
		masiveFileNew.setPathpdf(ticketRs.getPathpdf());
		masiveFileNew.setTransaction(ticketRs.getTrans());
		return masiveFileNew;
	}
	
	private TimbradoMasivoRs callByTicketNumber(RequestFileDto requestFileDto, MasiveFileDto masiveFileDto, List<IneDto> ineDtos) {
		TimbradoMasivoRs timbradoRs = null;
		TimbradoMasivoRq timbradoRq = new TimbradoMasivoRq();
		timbradoRq.setRequestDateTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(requestFileDto.getRequestDateTime()));
		timbradoRq.setSource("MASIVO");

		String[] tktPnr = { masiveFileDto.getTicketNumber() };
		timbradoRq.setTktPnr(tktPnr);
		timbradoRq.setRfc(masiveFileDto.getRfc());
		timbradoRq.setRfcName(masiveFileDto.getCompanyName());
		timbradoRq.setReceiverEmail("");
		timbradoRq.setRfcFrecuent("0");
		timbradoRq.setUser(requestFileDto.getUser());
		timbradoRq.setComentario(masiveFileDto.getReference() == null ? "" : masiveFileDto.getReference());
		timbradoRq.setJsonIne("");
		String json = ineDtos != null ? GSON.toJson(generateJsonIne(ineDtos)) : "";

		timbradoRs = callTimbrado(timbradoRq, json);
		return timbradoRs;
	}

	private String generateJsonIne(List<IneDto> ineDtos) {
		String response = "";
		if (ineDtos != null) {
			IneRq ineRq = null;
			for (int i = 0; i < ineDtos.size(); i++) {
				IneDto ineDto = ineDtos.get(i);
				if (i == 0) {
					ineRq = new IneRq();
					ineRq.setVersion("1.1");
					ineRq.setTipoProceso(ineDto.getProcessType());
					ineRq.setEntidad(new ArrayList<>());
				}

				IneEntidadRq entidadRq = new IneEntidadRq();
				entidadRq.setClaveEntidad(ineDto.getEntity());
				entidadRq.setAmbito(ineDto.getAmbitoType());

				IneContabilidadRq ineContabilidadRq = new IneContabilidadRq();
				ineContabilidadRq.setIdContabilidad(ineDto.getContabilidadId());
				entidadRq.setContabilidad(ineContabilidadRq);
				ineRq.getEntidad().add(entidadRq);
			}
			response = Utils.parseObjectToJsonString2(ineRq);
		}
		return response;
	}

	private TimbradoMasivoRs callTimbrado(TimbradoMasivoRq timbradoRq, String json) {
		TimbradoMasivoRs timbradoRs = null;
		String timbradoRqString = "";
		try {
			timbradoRqString = Utils.parseObjectToJsonString(timbradoRq);
			if (json != null) {
				timbradoRqString = timbradoRqString.substring(0, timbradoRqString.length() - 5)
						+ json.substring(0, json.length() - 1) + "\\\"}" + "\"";
			}
		} catch (Exception e) {
			LOGGER.error("ERROR DE generateRequest Timbrado () --> {} ",  ExceptionUtils.getStackTrace(e));
		}

		LOGGER.info("Call Timbrado :: -> request  : {}", timbradoRqString);
		HttpEntity<String> request = new HttpEntity<String>(timbradoRqString, Utils.generateHeaderRequest());
		String responseEntity = restTemplate.postForObject(Constants.Enviroments.URL_MIATECH_TIMBRADO_MASIVO, request, String.class);
		LOGGER.info("Call Timbrado :: -> response  : {}",  responseEntity);
		
		try {
			timbradoRs = Utils.parseJsonStringToObject(responseEntity);
		} catch (Exception e) {
			LOGGER.error("ERROR DE PROCESO () --> {} ", ExceptionUtils.getStackTrace(e));
			timbradoRs = new TimbradoMasivoRs();
			List<TicketRs> lstTicketRs = new ArrayList<>();
			TicketRs oTicketRs = new TicketRs();
			oTicketRs.setEstado(PROCESO_MASIVO_ESTADO_ERROR_DE_PROCESO);
			lstTicketRs.add(oTicketRs);
			timbradoRs.setData(lstTicketRs);
			timbradoRs.setStatus("ERROR");
		}

		return timbradoRs;
	}
}
