package com.miatech.service.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.ProcessEspejoFileDao;
import com.miatech.service.dao.ProcessEspejoFileDaoImpl;
import com.miatech.service.dao.RequestFileDao;
import com.miatech.service.dao.RequestFileDaoImpl;
import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.dto.client.TicketRs;
import com.miatech.service.dto.client.TimbradoEspejoRq;
import com.miatech.service.dto.client.TimbradoEspejoRs;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Utils;

public class TimbradoEspejoFileServiceImpl implements TimbradoEspejoFileService {

	private ProcessEspejoFileDao processEspejoFileDao;
	private RequestFileDao requestFileDao;
	private RestTemplate restTemplate;
	private Integer contadorProcesados = 0;
	private Integer contadorCalculados = 0;
	
	private static final Logger LOGGER = LogManager.getLogger(TimbradoEspejoFileServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(Constants.DATEFORMAT7).create();
	private static final String ESTADO_ERROR_DE_PROCESO= "ERROR DE PROCESO";

	public TimbradoEspejoFileServiceImpl(RestTemplate restTemplate) throws BaseException {
		this.processEspejoFileDao = new ProcessEspejoFileDaoImpl();
		this.requestFileDao = new RequestFileDaoImpl();
		this.restTemplate = restTemplate;
	}

	@Override
	public void timbrado(String espejoFileId) throws BaseException {
		LOGGER.info("timbradoMasivo() --> espejoFileId : {} ", espejoFileId);
		RequestFileDto requestFileDto = requestFileDao.getDataEspejoById(espejoFileId);
		requestFileDto.getLstEspejoFileDto().forEach(espejoFileDto -> {
			try {
				TimbradoEspejoRs timbradoRs = callByTicketNumber(requestFileDto, espejoFileDto);
				if (timbradoRs != null) {
					contadorProcesados++;
					 if (timbradoRs.getStatus().equals("OK")) {
						 espejoFileDto.setStatus("PROCESADO");
						if (!timbradoRs.getData().isEmpty()) {
						 	TicketRs ticketRs = timbradoRs.getData().get(0);
							espejoFileDto.setStatus(ticketRs.getEstado());
							espejoFileDto.setUuid(ticketRs.getUuid());
							espejoFileDto.setPathxml(ticketRs.getPathxml());
							espejoFileDto.setPathpdf(ticketRs.getPathpdf());
							espejoFileDto.setSerie(ticketRs.getSerie());
							espejoFileDto.setPnrRef(ticketRs.getPnr());
							espejoFileDto.setTransaction(ticketRs.getTrans());
						}
					} else {
						TicketRs ticketRs = timbradoRs.getData().get(0);
						espejoFileDto.setStatus(ticketRs.getEstado());
					}
					 processEspejoFileDao.updateEspejoFileData(espejoFileDto);
					requestFileDao.updateTicketsProscedRequestFile(requestFileDto.getId(), contadorProcesados, contadorCalculados);
				}
			} catch (Exception e) {
				LOGGER.error("ERROR DE timbradoIndividual() --> masiveFileId : {}, DETALLE : {} ", espejoFileId,  ExceptionUtils.getStackTrace(e));
			}
		});
	}
 
	private TimbradoEspejoRs callByTicketNumber(RequestFileDto requestFileDto, EspejoFileDto espejoFileDto) {
		TimbradoEspejoRs timbradoRs = null;
		TimbradoEspejoRq timbradoRq = new TimbradoEspejoRq();
		timbradoRq.setTkt(espejoFileDto.getVoucherNumber().split("/")[0]);
		timbradoRq.setDocType(espejoFileDto.getTipDocto());
		timbradoRq.setReceiverRFC(espejoFileDto.getRfcDep());
		timbradoRq.setReceiverName(espejoFileDto.getPax());
		timbradoRq.setIata(espejoFileDto.getIata());
		timbradoRq.setCurrency(espejoFileDto.getCurrencyType());
		timbradoRq.setFop(espejoFileDto.getPaymentType());
		timbradoRq.setOaltNuo(String.valueOf(espejoFileDto.getTarifa0()));
		timbradoRq.setOaltNu2("0");
		timbradoRq.setOaltNu8("0");
		timbradoRq.setOaltNu16(String.valueOf(espejoFileDto.getTarifa16()));
		timbradoRq.setTua(String.valueOf(espejoFileDto.getTua()));
		timbradoRq.setXt(String.valueOf(espejoFileDto.getOtrosImp()));
		timbradoRq.setCurrency(espejoFileDto.getCurrencyType());
		timbradoRq.setUser(requestFileDto.getUser());
		timbradoRq.setReceiverEmail(espejoFileDto.getEmail());
		timbradoRq.setComentario(espejoFileDto.getVoucherNumber());
		timbradoRs = callTimbrado(timbradoRq);
		return timbradoRs;
	}

	private TimbradoEspejoRs callTimbrado(TimbradoEspejoRq timbradoEspejoRq) {
		TimbradoEspejoRs timbradoRs = null;
		LOGGER.info("Call Timbrado :: -> request  : {}", GSON.toJson(timbradoEspejoRq));
		HttpEntity<String> request = new HttpEntity<String>(GSON.toJson(timbradoEspejoRq), Utils.generateHeaderRequest());
		String responseEntity = restTemplate.postForObject(Constants.Enviroments.URL_MIATECH_TIMBRADO_ESPEJO, request, String.class);
		LOGGER.info("Call Timbrado :: -> response  : {}",  responseEntity);
		
		try {
			timbradoRs = Utils.parseJsonStringToObject2(responseEntity);
		} catch (Exception e) {
			LOGGER.error("ERROR DE PROCESO () --> {} ", ExceptionUtils.getStackTrace(e));
			timbradoRs = new TimbradoEspejoRs();
			List<TicketRs> lstTicketRs = new ArrayList<>();
			TicketRs oTicketRs = new TicketRs();
			oTicketRs.setEstado(ESTADO_ERROR_DE_PROCESO);
			lstTicketRs.add(oTicketRs);
			timbradoRs.setData(lstTicketRs);
			timbradoRs.setStatus("ERROR");
		}

		return timbradoRs;
	}

}
