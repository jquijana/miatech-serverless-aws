package com.miatech.service.service;

import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;

public interface TimbradoEspejoFileService {

	void timbrado(String espejoFileId) throws BusinessException, BaseException;
	
}
