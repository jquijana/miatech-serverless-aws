package com.miatech.service.service;

import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;

public interface ProcessFileService {

	void processMasive() throws BusinessException, BaseException;

}
