package com.miatech.service.service;

import com.miatech.service.dto.client.ValidationFileRq;
import com.miatech.service.exception.BaseException;
import com.miatech.service.exception.BusinessException;

public interface ValidationEspejoFileService {

	void validation(ValidationFileRq request) throws BusinessException, BaseException;

}
