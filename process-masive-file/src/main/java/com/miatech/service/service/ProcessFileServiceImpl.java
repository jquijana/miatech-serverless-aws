 package com.miatech.service.service;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.ConfigProcessFileDao;
import com.miatech.service.dao.ConfigProcessFileDaoImpl;
import com.miatech.service.dao.RequestFileDao;
import com.miatech.service.dao.RequestFileDaoImpl;
import com.miatech.service.dto.ConfigProcessFileDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.dto.client.GenerateFileRq;
import com.miatech.service.dto.client.ValidationFileRq;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;

public class ProcessFileServiceImpl implements ProcessFileService {

	private RequestFileDao requestFileDao;
	private ConfigProcessFileDao configProcessFileDao;
	private RestTemplate restTemplate;

	private static final Logger LOGGER = LogManager.getLogger(ProcessFileServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(Constants.DATEFORMAT7).create();
	private static final Integer REQUEST_FILE_ESTADO_PENDIENTE = 0;
	private static final Integer REQUEST_FILE_ESTADO_EN_PROCESO = 1;

	public ProcessFileServiceImpl(RestTemplate restTemplate) throws BaseException {
		this.requestFileDao = new RequestFileDaoImpl();
		this.restTemplate = restTemplate;
		this.configProcessFileDao = new ConfigProcessFileDaoImpl();
	}

	@Override
	public void processMasive() throws BaseException {
		LOGGER.info("----- Inicio Proceso Masivo ----- ");
		List<RequestFileDto> requestFileDtos = requestFileDao.getDataRequestFilePendiente();
		ConfigProcessFileDto configProcessMasiveFileDto = configProcessFileDao.getDataConfig(Constants.CODE_FILE_CONFIG_MASIVE);
		ConfigProcessFileDto configProcessEspejoFileDto = configProcessFileDao.getDataConfig(Constants.CODE_FILE_CONFIG_ESPEJO);
		
		if (requestFileDtos != null ) {
			for (RequestFileDto data : requestFileDtos) {
				LOGGER.info(" requestFileDto : {} ", GSON.toJson(data));

				if (data.getStatus() == REQUEST_FILE_ESTADO_PENDIENTE) {
					data.setStatus(REQUEST_FILE_ESTADO_EN_PROCESO);
					requestFileDao.updateRequestFile(data);
				}
				
				if (data.getFileType().equals(Constants.FILE_TYPE_MASIVO)) {
					if (excedeTiempoDeEnvioParcial(configProcessMasiveFileDto, data)) {
						callGenerateFile(data);
					}
				}else if(data.getFileType().equals(Constants.FILE_TYPE_ESPEJO)) {
					if (excedeTiempoDeEnvioParcial(configProcessEspejoFileDto, data)) {
						callGenerateFile(data);
					}
				}
				
				callValidationFile(data);
			}
		}
		LOGGER.info("----- Fin Proceso Masivo ----- ");
	}

	private Boolean excedeTiempoDeEnvioParcial(ConfigProcessFileDto configProcessFileDto, RequestFileDto data) {
		Boolean excedioTiempo = Boolean.FALSE;
		if (data.getLastVerified() != null) {
			long diff = 0L, diffSeconds = 0L, diffMinutes = 0L, diffHours = 0L, totalMinutes = 0L;
			diff = configProcessFileDto.getDateNow().getTime() - data.getLastVerified().getTime();
			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
			diffHours = diff / (60 * 60 * 1000);
			totalMinutes = diffSeconds / 60 + diffMinutes + diffHours * 60;

			// Si el tiempo de tabla configuraciones excede se envia partial
			if (totalMinutes >= configProcessFileDto.getTimeSendMail()
					&& totalMinutes <= configProcessFileDto.getTimeSendMailLimit()) {
				excedioTiempo = Boolean.TRUE;
			}

		}
		return excedioTiempo;
	}
	
	private HttpHeaders generateHeaderRequest() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}

	private void callGenerateFile(RequestFileDto requestFileDto) {
		LOGGER.info("Call Envio Generate File :: -> request  : {} ", GSON.toJson(requestFileDto));
		try {
			GenerateFileRq envioFileRq = new GenerateFileRq();
			envioFileRq.setRequestFileId(requestFileDto.getId().toString());
			envioFileRq.setFileType(requestFileDto.getFileType());
			String urlValidationFile = Constants.Enviroments.URL_ENVIO_FILE;
			HttpEntity<GenerateFileRq> request = new HttpEntity<GenerateFileRq>(envioFileRq, generateHeaderRequest());
			String responseEntity = restTemplate.postForObject(urlValidationFile, request, String.class);
			LOGGER.info("Call Envio Generate File :: -> response  : " + GSON.toJson(responseEntity));
		} catch (Exception e) {
			LOGGER.error("ERROR callGenerateFile : {} ", ExceptionUtils.getStackTrace(e));
		}
	}

	private void callValidationFile(RequestFileDto requestFileDto) {
		LOGGER.info("Call Validation File :: -> request  : {} ", GSON.toJson(requestFileDto));
		try {
			ValidationFileRq validationFileRq = new ValidationFileRq();
			validationFileRq.setRequestFileId(requestFileDto.getId().toString());
			validationFileRq.setFileType(requestFileDto.getFileType());
			String urlValidationFile = Constants.Enviroments.URL_VALIDATION_FILE;
			HttpEntity<ValidationFileRq> request = new HttpEntity<ValidationFileRq>(validationFileRq, generateHeaderRequest());
			String responseEntity = restTemplate.postForObject(urlValidationFile, request, String.class);
			LOGGER.info("Call Validation File :: -> response  : " + GSON.toJson(responseEntity));
		} catch (Exception e) {
			LOGGER.error("ERROR callValidationFile : {} "+ ExceptionUtils.getStackTrace(e));
		}
	}
}
