package com.miatech.service.service;

import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miatech.service.dao.ProcessEspejoFileDao;
import com.miatech.service.dao.ProcessEspejoFileDaoImpl;
import com.miatech.service.dao.RequestFileDao;
import com.miatech.service.dao.RequestFileDaoImpl;
import com.miatech.service.dto.EspejoFileDto;
import com.miatech.service.dto.RequestFileDto;
import com.miatech.service.dto.client.ProcessIndividualRq;
import com.miatech.service.dto.client.ValidationFileRq;
import com.miatech.service.exception.BaseException;
import com.miatech.service.util.Constants;
import com.miatech.service.util.Utils;

public class ValidationEspejoFileServiceImpl implements ValidationEspejoFileService {

	private ProcessEspejoFileDao processEspejoFileDao;
	private RequestFileDao requestFileDao;
	private RestTemplate restTemplate;

	private static final Logger LOGGER = LogManager.getLogger(ValidationEspejoFileServiceImpl.class);
	private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(Constants.DATEFORMAT7).create();
	private static final String PROCESO_MASIVO_ESTADO_ERROR = "ERROR DE VALIDACION";

	public ValidationEspejoFileServiceImpl(RestTemplate restTemplate) throws BaseException {
		this.processEspejoFileDao = new ProcessEspejoFileDaoImpl();
		this.requestFileDao = new RequestFileDaoImpl();
		this.restTemplate = restTemplate;
	}

	@Override
	public void validation(ValidationFileRq request) throws BaseException {
		LOGGER.info("validation() --> request : {}", GSON.toJson(request));
		RequestFileDto requestFileDto = requestFileDao.getDataEspejoFileDetail(request.getRequestFileId());
		if (requestFileDto != null && requestFileDto.getLstEspejoFileDto() != null) {
			for (EspejoFileDto data : requestFileDto.getLstEspejoFileDto()) {
				try {
					if ((validaAirlineCode(data) 
							&& validaMontos(data)
							&& validaCurrencyType(data) 
							&& validaPaymentType(data)
							&& validaRfcDpt(data)
							&& validaEmail(data))
							&& validaPax(data) && validaRuta(data) && validaTipDocto(data)) {
						processEspejoFileDao.updateEspejoFileData(data);
						callTimbradoIndividual(requestFileDto.getFileType(), data);
					} else {
						updateNotValidation(requestFileDto.getId(), data);
					}
				} catch (Exception e) {
					LOGGER.error("ERROR () --> Validacion : {} , DETALLE : {} ", GSON.toJson(data), ExceptionUtils.getStackTrace(e));
				}
			}
		}
	}

	private void updateNotValidation(Long requestFileId,  EspejoFileDto  data) throws BaseException {
		if (data!=null) {
			processEspejoFileDao.updateEspejoFileData(data);
			requestFileDao.updateTicketsProscedRequestFile(requestFileId, 1, 0);
		}
	}
	
	private Boolean validaAirlineCode(final EspejoFileDto data) {
		Boolean esValido = Boolean.TRUE;
		if (data.getAirlineCode() != null && !data.getAirlineCode().isEmpty()) {
			if (data.getAirlineCode().length() != 3) {
				joinObservacion(data, "EL CODIGO IATA DE LA LINEA AEREA.  DEBE SER 3 DIGITOS NUMERICOS");
				esValido = Boolean.FALSE;
			}
		} else {
			joinObservacion(data, "CAMPOS OBLIGATORIOS FALTA CODIGO IATA DE LA LINEA AEREA");
			esValido = Boolean.FALSE;
		}

		return esValido;
	}

	private Boolean validaMontos(final EspejoFileDto data) {
		if (data.getTarifa0() == null) {
			joinObservacion(data, "CAMPOS OBLIGATORIOS FALTA TARIFA 0%");
			return Boolean.FALSE;
		}else {
			if (data.getTarifa0() < 0 ) {
				joinObservacion(data, "TARIFA 0% NO PUEDE SER NEGATIVO ");
				return Boolean.FALSE;
			}
		}
		
		if (data.getIva0() == null) {
			joinObservacion(data, "CAMPOS OBLIGATORIOS FALTA IVA 0% ");
			return Boolean.FALSE;
		}else {
			if (data.getIva0() < 0 ) {
				joinObservacion(data, "IVA 0% NO PUEDE SER NEGATIVO ");
				return Boolean.FALSE;
			}
		}
		
		if (data.getTarifa16() == null) {
			joinObservacion(data, "CAMPOS OBLIGATORIOS FALTA TARIFA 16%");
			return Boolean.FALSE;
		}else {
			if (data.getTarifa16() < 0 ) {
				joinObservacion(data, "TARIFA 16% NO PUEDE SER NEGATIVO ");
				return Boolean.FALSE;
			}
		}
		
		if (data.getIva16() == null) {
			joinObservacion(data, "CAMPOS OBLIGATORIOS FALTA IVA 16% ");
			return Boolean.FALSE;
		}else {
			if (data.getIva16() < 0 ) {
				joinObservacion(data, "IVA 16% NO PUEDE SER NEGATIVO ");
				return Boolean.FALSE;
			}
		}
		
		if (data.getTua() == null) {
			joinObservacion(data, "CAMPOS OBLIGATORIOS FALTA TUA");
			return Boolean.FALSE;
		}else {
			if (data.getTua() < 0 ) {
				joinObservacion(data, "TUA NO PUEDE SER NEGATIVO ");
				return Boolean.FALSE;
			}
		}
		
		if (data.getOtrosImp() == null) {
			joinObservacion(data, "CAMPOS OBLIGATORIOS FALTA OTROS IMPUESTOS ");
			return Boolean.FALSE;
		}else {
			if (data.getOtrosImp() < 0 ) {
				joinObservacion(data, "OTROS IMPUESTOS NO PUEDE SER NEGATIVO ");
				return Boolean.FALSE;
			}
		}
		
		if (data.getTotal() == null) {
			joinObservacion(data, "CAMPOS OBLIGATORIOS FALTA TOTAL ");
			return Boolean.FALSE;
		}else {
			if (data.getTotal() < 0 ) {
				joinObservacion(data, "TOTAL NO PUEDE SER NEGATIVO ");
				return Boolean.FALSE;
			}
		}
		
		double[] values = {data.getTarifa0(), data.getIva0(), data.getTarifa16(), data.getIva16(), data.getTua(), data.getOtrosImp()};
		if (data.getTotal() != (Utils.sumDouble(values, 2))) {
			joinObservacion(data, "LA SUMA DE TODOS LOS CONCEPTOS DEBE SER IGUAL AL TOTAL ");
			return Boolean.FALSE;
		}

		return Boolean.TRUE;
	}

	private Boolean validaEmail(final EspejoFileDto data) {
		Boolean esValido = Boolean.TRUE;
		if (data.getEmail() != null && !data.getEmail().trim().isEmpty()) {
			Pattern pattern = Pattern.compile(
					"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			Matcher mather = pattern.matcher(data.getEmail());
			if (mather.find() == false) {
				joinObservacion(data, "CORREO ELECTRONICO NO PASA VALIDACION");
				esValido = Boolean.FALSE;
			}
		}

		return esValido;
	}
	
	private Boolean validaCurrencyType(final EspejoFileDto data) {
		if (data.getCurrencyType() == null 
				|| data.getCurrencyType().trim().isEmpty() 
				|| !data.getCurrencyType().equals("MXN")) {
				joinObservacion(data, "MONEDA NO PASA VALIDACION (TIENE QUE SER MXN)");
				return Boolean.FALSE;
		}

		return  Boolean.TRUE;
	}
	
	private Boolean validaPaymentType(final EspejoFileDto data) {
		if (data.getPaymentType() == null || data.getPaymentType().trim().isEmpty()) {
				joinObservacion(data, "MPAGO ES OBLIGATORIO ");
				return Boolean.FALSE;
		}
		return  Boolean.TRUE;
	}
	
	private Boolean validaPax(final EspejoFileDto data) {
		if (data.getPax() == null || data.getPax().trim().isEmpty()) {
				joinObservacion(data, "PAX ES OBLIGATORIO ");
				return Boolean.FALSE;
		}
		return  Boolean.TRUE;
	}
	
	private Boolean validaRuta(final EspejoFileDto data) {
		if (data.getRuta() == null || data.getRuta().trim().isEmpty()) {
				joinObservacion(data, "RUTA ES OBLIGATORIO ");
				return Boolean.FALSE;
		}
		return  Boolean.TRUE;
	}
	
	private Boolean validaTipDocto(final EspejoFileDto data) {
		if (data.getTipDocto()  == null || data.getTipDocto().trim().isEmpty()) {
				joinObservacion(data, "TIPODOCTO ES OBLIGATORIO ");
				return Boolean.FALSE;
		}
		return  Boolean.TRUE;
	}

	private Boolean validaRfcDpt(final EspejoFileDto data) {
		Boolean esValido = Boolean.TRUE;
		if (data.getRfcDep() == null || data.getRfcDep().isEmpty()) {
			joinObservacion(data, "CAMPOS OBLIGATORIOS FALTA RFC_DEP");
			esValido = Boolean.FALSE;
		} else {
			if (data.getRfcDep().length() == 12 || data.getRfcDep().length() == 13) {
				Pattern patternMoral = Pattern.compile(
						"([A-ZÑ&]{3,4}) ?(?:- ?)?(\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\d|3[01])) ?(?:- ?)?([A-Z\\d]{2})([A\\d])");
				Matcher mather = patternMoral.matcher(data.getRfcDep());
				if (mather.find() == false) {
					if (data.getRfcDep().length() == 12) {
						joinObservacion(data, "RFC MORAL NO PASA VALIDACION");
						esValido = Boolean.FALSE;
					} else {
						joinObservacion(data, "RFC FISICA NO PASA VALIDACION");
						esValido = Boolean.FALSE;
					}
				}
			} else {
				joinObservacion(data,
						"RFC NO PASA VALIDACION.  RFC TIENE QUE TENER 12 CARACTERES PARA RFC MORAL Y 13 CARACTERES PARA RFC FISICA");
				esValido = Boolean.FALSE;
			}
		}

		return esValido;
	}

	private void joinObservacion(EspejoFileDto masiveFileDto, String message) {
		masiveFileDto.setStatus(PROCESO_MASIVO_ESTADO_ERROR);
		if (masiveFileDto.getObservation() != null) {
			masiveFileDto.setObservation(masiveFileDto.getObservation() + "," + message);
		} else {
			masiveFileDto.setObservation(message);
		}
	}

	private HttpHeaders generateHeaderRequest() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}

	private void callTimbradoIndividual(String fileType, EspejoFileDto data) {
		LOGGER.info("Call Envio Procces Individual File :: -> request  : {}",  GSON.toJson(data));
		try {
			ProcessIndividualRq envioFileRq = new ProcessIndividualRq();
			envioFileRq.setDataFileId(data.getId().toString());
			envioFileRq.setFileType(fileType);
			String urlValidationFile = Constants.Enviroments.URL_TIMBRADO_INDIVIDUAL;
			HttpEntity<ProcessIndividualRq> request = new HttpEntity<ProcessIndividualRq>(envioFileRq, generateHeaderRequest());
			String responseEntity = restTemplate.postForObject(urlValidationFile, request, String.class);
			LOGGER.info("Call Envio Procces Individual File :: -> response  : {} ", GSON.toJson(responseEntity));
		} catch (Exception e) {
			LOGGER.error("ERROR callTimbradoIndividual : {}" ,  ExceptionUtils.getStackTrace(e) );
		}
	}
}
